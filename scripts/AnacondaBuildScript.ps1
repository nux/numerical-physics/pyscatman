$tag=$args[0]

write-host "Building Windows wheels for version $tag"

mkdir build_temp_$tag
cd build_temp_$tag
git clone --branch $tag git@gitlab.ethz.ch:nux/numerical-physics/pyscatman.git
cd pyscatman


$pyver = @('python3.6','python3.7','python3.8')

foreach($py in $pyver) {
    conda activate $py
    $ENV:CI_COMMIT_TAG = $tag
    $ENV:SCATMAN_CUDA = ''
    python setup.py bdist_wheel
    $ENV:SCATMAN_CUDA = 1
    python setup.py bdist_wheel
    $ENV:SCATMAN_CUDA = ''
    conda deactivate
}


$ENV:TWINE_PASSWORD = "Y8CnfrArozEqzWwwkQMS"
$ENV:TWINE_USERNAME = "__token__" 
python -m twine upload --repository-url https://gitlab.com/api/v4/projects/18794850/packages/pypi dist/* --verbose --skip-existing

cd ..
cd ..
Remove-Item build_temp_$tag -Force  -Recurse
