


Setting functions
=========================

The follwing methods aim to set the relevant parameters for the :code:`scatman` module. 


.. _photon_density_description:

Simulation of photons count
---------------------------

When the simulation involves also the photon statistics (i.e. when a detector is used that is not just the MSFT one), the number of scattered photons must be taken into account, to correctly simulate the Poisson statistics of photons that impinge on the detector. 
The number of scattered photons depend both on the photon density of the beam and on the optical properties of the sample. A highly absorbing material scatters much more photons than an almost transparent one.

The Scatman module is able to simulate the photon statistics, and the photon density (counts over area unit) can be set (see the list of setting functions).
When the calculation of the photon count is performed, the following (probably wrong or highly simplifying) assumptions are made:

- All the photons absorbed by the sample are scattered, and
- All the scattered photons arrive on the detector, i.e. no photons fall outside the detector area.

.. warning::
    please keep in mind these two assumptions if you try to get meaningful physical results from the Scatman. I decline any responsibility concerning the meaningfulness of what comes out from these assumptions.
    
    
List of functions
-----------------


.. automodule:: scatman
    :members:
    :exclude-members: Pattern

    
.. raw:: latex

    \newpage

  

  

