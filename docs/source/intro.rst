=========================
Introduction
=========================


Welcome to the PyScatman python module documentation! 

The PyScatman module is a tool to perform a wide-angle coherent diffraction simulation, based on the Multi-Slice Fourier Transform (MSFT) approach.

The PyScatman module is based on the following building blocks:

 - **It is a Python module**
    | Thus, this allows an easy interface with the user and with other Python modules and scripts

 - **It is implemented in C++**
    | PyScatman is a *compiled* module written in C++. However, the user won't have to deal with compilcated C++ stuff, because it regards only its implementation, and not its interface. The C++ implementation allows to achieve the following point
    
 - **It has a high computational effeciency**
    | The MSFT approach is a computationally intensive task, requiring the exection of many Fourier Transforms per simulation. Depending on the hardware on which the module is executed, the C++ code enables to almost fully exploit all the computational resources provided by the PC, from single-core CPUs up to multi-core CPUs with multiple GPUs. 


The following sections will guide you through the module features. Enjoy! ;) 

