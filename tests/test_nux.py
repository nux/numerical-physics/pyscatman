from PIL import Image
import numpy as np

image = Image.open('NUX.png')                                          # load the grayscale image and
nux = np.copy(np.asarray(image)[:,:,0])                                # convert it to a 2D numpy array
nux=nux/np.max(nux)
nux = nux[:,::-1]


import scatman

scatman.set_experiment(wavelength=30, angle=30, resolution=512)        # set the experimental parameters

detector = scatman.Detectors.Ideal(mask_radius=50)                     # set the detector, with a central hole of 50 pixels

shape = scatman.Shapes.RadialMap(a=500, radii=nux,\
            latitude=0, longitude=90, rotation=-20)               # a is a scaling factor (see class documentation),  the nux image is used as radial map 
   
shape2 = scatman.Shapes.RadialMap( radii=nux*500.,\
            latitude=0, longitude=90, rotation=-20)   

patterns=detector.acquire([shape, shape2])

import render_sh                        # you can find it in the "tests" folder. Requires the Vtk python module

render_sh.Scene(patterns, [shape, shape2])     # render the shape and the simulated diffraction pattern together, in a fancy way
    




