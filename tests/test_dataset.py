import scatman
import numpy as np
import time
from PIL import Image
import random

#scatman.use_cpu()

t = [time.time()]

resolution=512
angle=30.
detector="Ideal"
wavelength=50.
t.append(time.time())


ndata=10
#scatman.set_gpus([0])


scatman.set_experiment(wavelength=wavelength, angle=angle, resolution=resolution, photon_density=1e7)
scatman.info()
scatman.init()
scatman.set_verbose(1)
random.seed(111)

mydetector=[]

if detector=="MSFT":
    mydetector = scatman.Detectors.MSFT()
if detector=="Ideal":
    mydetector = scatman.Detectors.Ideal(mask_radius=40)

if detector=="MCP":
    mydetector = scatman.Detectors.MCP(mask_radius=40, 
                                   photons_per_channel=3000., channels_per_pixel = 1, 
                                   gain=1, saturation=1000., linearity=0.9, exponent=0.4,
                                   channel_angle=4., channel_direction=270., offset=0)


mydataset = [] #scatman.Dataset()


for i in range(0, ndata):
    midsize=random.uniform(300,600)
    
    myshape = []
    
    if random.uniform(0,1)<0:
        myshape=scatman.Shapes.Ellipsoid(random.uniform(0.5,1.5)*midsize,random.uniform(0.5,1.5)*midsize,random.uniform(0.5,1.5)*midsize, latitude=random.uniform(-90,90),longitude=random.uniform(0,180),rotation=random.uniform(0,180));
        
    else:
        a=random.uniform(0.3,1)*midsize
        myshape=scatman.Shapes.Dumbbell(midsize,
                                        a,a,a, midsize-a,
                                        latitude=random.uniform(-90,90),longitude=random.uniform(0,180),rotation=random.uniform(0,180));
    mydataset.append(myshape)
    
    
#print(mydataset)    


t.append(time.time())
mypattern = mydetector.acquire_dataset(mydataset)
t.append(time.time())
#millisec=int((t[-1]-t[-2])*1000);

print("Patterns\t", len(mypattern))
print("Time    \t", (t[-1]-t[-2]),"s")

timeperpattern=((t[-1]-t[-2])*1000./len(mypattern))
print("Time per pattern ",timeperpattern , " ms")

import render_sh
render_sh.Scene(mypattern, mydataset)

