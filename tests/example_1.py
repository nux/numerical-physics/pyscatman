import scatman

scatman.info()

scatman.set_verbose(1)

scatman.set_experiment(wavelength=45., angle=30, resolution=512);

mydetector = scatman.Detectors.MSFT()

myshape = scatman.Shapes.Ellipsoid(400,250,250, latitude=60, longitude=40, rotation=0)

mypattern = mydetector.acquire(myshape)

import matplotlib.pyplot as plt
import numpy as np

plt.imshow(np.log(mypattern))
plt.show()

