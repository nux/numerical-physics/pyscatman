import scatman
import numpy as np

resolution=256
angle=30.
wavelength=1.




ndata=10000


scatman.use_gpu()
scatman.set_gpu_ids([0,1,2,3])

scatman.set_experiment(wavelength=wavelength, angle=angle, resolution=resolution, photon_density=1e7, rescale=128)
scatman.info()
scatman.init()
scatman.set_verbose(1)
#random.seed(111)

mydetector = scatman.Detectors.MSFT()

mydataset = [] #scatman.Dataset()

sideres=8

rmin=5
rmax=15

for i in range(0, ndata):
    radii=np.random.rand(6,sideres,sideres)*(rmax-rmin)+ rmin
        
    myshape = scatman.Shapes.CubeMap(radii=radii)
    
    mydataset.append(myshape)
    
mypattern = mydetector.acquire_dataset(mydataset)


import matplotlib.pyplot as plt
import numpy as np

plt.imshow(np.log(mypattern[3]))
plt.show()
