import scatman

scatman.set_experiment(wavelength=[60,30], angle=30, resolution=512, photon_density=[1e6,3e5], phase_shift=[0, 0])        # set the experimental parameters

detector = scatman.Detectors.Ideal(mask_radius=50)                     # set the detector, with a central hole of 50 pixels radius

shapes=[]

beta=[0.01,0.01]
delta=[0.001, 0.001]

harmonics = [[0,0,1]]

shapes.append(scatman.Shapes.SphericalHarmonics(a=400, coefficients=harmonics, delta=delta, beta=beta, latitude=43, longitude=190, rotation=58))


harmonics = [[0,0,1],
             [2,-1,0.25]]

shapes.append(scatman.Shapes.SphericalHarmonics(a=400, coefficients=harmonics, delta=delta, beta=beta, latitude=43, longitude=190, rotation=58))


harmonics = [ [0,0,1],
              [1,1,0.01],
              [2,-1,0.02],
              [7,6,0.01],
              [4,-3,0.01],
              [13,5, -0.01],
              [11,-1, 0.01]]

shapes.append(scatman.Shapes.SphericalHarmonics(a=700, coefficients=harmonics, delta=delta, beta=beta, latitude=-13, longitude=40, rotation=76))
                                                                        
patterns=detector.acquire_dataset(shapes)

import render_sh                      # you can find it in the "tests" folder. Requires the Vtk python module

render_sh.Scene(patterns, shapes)     # render the shape and the simulated diffraction pattern together, in a fancy way
    




