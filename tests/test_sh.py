import scatman
import numpy as np
scatman.set_experiment(wavelength=30, angle=30, resolution=512)        # set the experimental parameters

detector = scatman.Detectors.Ideal(mask_radius=0)                     # set the detector, with a central hole of 50 pixels radius

shapes=[]


harmonics = [[0,0,1]]

shapes.append(scatman.Shapes.SphericalHarmonics(a=400, coefficients=harmonics, latitude=43, longitude=190, rotation=58))

for h in harmonics:
    h[2]*=400.

shapes.append(scatman.Shapes.SphericalHarmonics(coefficients=harmonics, latitude=43, longitude=190, rotation=58))


harmonics = [[0,0,1],
             [1,0,0.1],
             [2,0,0.2],
             [5,-4,0.05],
             [8,1,-0.01]]

shapes.append(scatman.Shapes.SphericalHarmonics(a=400, coefficients=harmonics, latitude=40, longitude=240, rotation=179))

for h in harmonics:
    h[2]*=400.

shapes.append(scatman.Shapes.SphericalHarmonics(coefficients=harmonics, latitude=40, longitude=240, rotation=179))


harmonics = [ [0,0,1],
              [1,1,0.2],
              [2,-1,0.06],
              [7,6,0.08]]

shapes.append(scatman.Shapes.SphericalHarmonics(a=700, coefficients=harmonics, latitude=-13, longitude=40, rotation=76))
                                                                        
patterns=detector.acquire_dataset(shapes)

import render_sh                      # you can find it in the "tests" folder. Requires the Vtk python module

render_sh.Scene(patterns, shapes)     # render the shape and the simulated diffraction pattern together, in a fancy way
    




