import scatman
import numpy as np
import argparse


parser = argparse.ArgumentParser()

parser.add_argument('-d', action='store', dest='detector_type',  type=str, help='Select detector type (MSFT/Ideal/MCP).', default="MSFT")

args = parser.parse_args()






scatman.set_experiment(wavelength=30, angle=30, resolution=512)        # set the experimental parameters

detector = [] 


if args.detector_type=="MSFT":
    detector = scatman.Detectors.MSFT()
if args.detector_type=="Ideal":
    detector = scatman.Detectors.Ideal()
if args.detector_type=="MCP":
    detector = scatman.Detectors.MCP(mask_radius=40, 
                                   gain=1000, saturation=30000., linearity=0, exponent=0.7, offset=0)



scatman.Detectors.Ideal(mask_radius=50)                     # set the detector, with a central hole of 50 pixels radius

shapes=[]

########################d
optical_properties = {"delta":0.001, "beta":0.01}
optical_properties_shell = {"delta_shell":0.003, "beta_shell":0.002}

orientation={"latitude": 60, "longitude": 140, "rotation":80}

size=400.

###################################d


shapes.append(scatman.Shapes.Ellipsoid(size, size*0.7, size*0.5,                                **optical_properties, **orientation))
shapes.append(scatman.Shapes.Dumbbell(size, size*0.7, size*0.7, size*0.7, size*0.25,            **optical_properties,**orientation))
shapes.append(scatman.Shapes.MoonSphere(size, size/8, size/32, **optical_properties_shell,      **optical_properties))
shapes.append(scatman.Shapes.ShellSphere(size, size/8, size/32,**optical_properties_shell,      **optical_properties))

shapes.append(scatman.Shapes.RadialMap(size, np.random.rand(10,10)*0.5 + 0.5,                     **optical_properties,**orientation))


harmonics = [ [0,0,1],
              [1,1,0.2],
              [2,-1,0.06],
              [7,6,0.08]]

shapes.append(scatman.Shapes.SphericalHarmonics(size, coefficients=harmonics,                   **optical_properties,**orientation))

#####################################d
                                                                        
patterns=detector.acquire_dataset(shapes)

import render_sh                      # you can find it in the "tests" folder. Requires the Vtk python module

render_sh.Scene(patterns, shapes)     # render the shape and the simulated diffraction pattern together, in a fancy way
    




