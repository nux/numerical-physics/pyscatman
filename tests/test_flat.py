import scatman
import numpy as np
import time
from PIL import Image

t = [time.time()]

resolution=512
angle=50.
wavelength=80.
t.append(time.time())

mydetector=[]

#scatman.set_threads(4);
#scatman.set_gpus([4,5,6]);
scatman.set_experiment(wavelength=wavelength, angle=angle, resolution=resolution, photon_density=1e6);


mydetector1 = scatman.Detectors.MSFT()
mydetector2 = scatman.Detectors.MSFT(flat=True)

shape=scatman.Shapes.Ellipsoid(400,400,400, latitude=90, longitude=0,rotation=0)
#shapes.append(scatman.Shapes.Dumbbell(300,200,200,200,90, latitude=60, longitude=-140, rotation=20))

scatman.init()

scatman.use_cpu()
scatman.set_verbose(1)
cpu_patterns=[]
cpu_patterns.append(mydetector1.acquire(shape))
cpu_patterns.append(mydetector2.acquire(shape))


#######
from matplotlib import pyplot as plt
import inspect 

plt.rcParams['image.cmap'] = 'inferno'

figcpu, axcpu = plt.subplots(nrows=1, ncols=2)

axcpu[0].imshow(np.log(cpu_patterns[0]))
axcpu[0].set_title("Curved detector")
axcpu[0].axis('off')    
axcpu[1].imshow(np.log(cpu_patterns[1]))
axcpu[1].set_title("Flat detector")    
axcpu[1].axis('off')

plt.savefig("flat.png", dpi=300, bbox_inches='tight', transparent=True)

plt.show()


