import scatman
import random
import numpy as np
from  scipy.special import sph_harm

from PIL import Image
# Open the image form working directory
image = Image.open('NUX_lowres.png')
# summarize some details about the image
print(image.format)
print(image.size)
print(image.mode)
# show the image

nux = np.copy(np.asarray(image)[:,:,0])

nux=nux/np.max(nux)

print(nux.shape)



def real_sph_harm(m, l, phi, theta):

    #return sph_harm(m, l, dphi*i, dtheta*j)
    if m<0:
        return 2**0.5 * np.imag(sph_harm(np.abs(m), l, dphi*i, dtheta*j))
    elif m==0:
        return np.real(sph_harm(np.abs(m), l, dphi*i, dtheta*j))
    else:
        return 2**0.5 * np.real(sph_harm(np.abs(m), l, dphi*i, dtheta*j))

scatman.set_verbose(1)
nshapes=1
#scatman.set_gpus([0])
#scatman.use_cpu()
random.seed(17)


parameters = []
shapes = []
shape_type= []

    
radii = -nux+4.


#radii=np.ones([100,200])

L = 10


coefficients=[]

dtheta=np.pi/(radii.shape[0])
dphi=2.*np.pi/(radii.shape[1])

#thetastart=-90.

#for l in range(L+1):
    #for m in range(-l,l+1):
        
        #val=0
        #for j in range(radii.shape[0]):
            #for i in range(radii.shape[1]):
                #val += radii[j,i]*real_sph_harm(m, l, dphi*i, dtheta*j)*np.sin(dtheta*j)

                
        #val = val/ (radii.shape[0]*radii.shape[1])
        
        #print(l, m, val)
        #coefficients.append([l,m,val])
        
for l in range(L+1):
    for m in range(-l,l+1):
        
        val=0
        for j in range(radii.shape[0]):
            for i in range(radii.shape[1]):
                val += radii[j,i]*real_sph_harm(m, l, dphi*i, dtheta*j)*np.sin(dtheta*j)

                
        val = val/ (radii.shape[0]*radii.shape[1])
        
        print("coeff: ",l, m, val)
        coefficients.append(val)

#newradii=np.zeros(radii.shape)


#for c in range(0,len(coefficients),3):
        
    #val=0
    #for j in range(radii.shape[0]):
        #for i in range(radii.shape[1]):
            #newradii[j,i]+=coefficients[c+2]*real_sph_harm(coefficients[c+1], coefficients[c], dphi*i, dtheta*j)
            

            
    ##val = val/ (radii.shape[0]*radii.shape[1])
    
    #print(coefficients[c:c+3])
    ##coefficients.extend([l,m,val])


for i in range(2):
            
    parameters.append({"a":700,"coefficients" : coefficients, "latitude" : 0, "longitude" : 0, "rotation": 58})
    shape = scatman.Shapes.SphericalHarmonics(**parameters[-1])
    shapes.append(shape)
    shape_type.append(shape.get_name())


#parameters.append({"a":700,"radii" : radii, "latitude" : 0, "longitude" : 0, "rotation": 58})
#shape = scatman.Shapes.RadialMap(**parameters[-1])
#shapes.append(shape)
#shape_type.append(shape.get_name())


scatman.set_resolution(512)

detector = scatman.Detectors.Ideal(mask_radius=50)

scatman.info()
scatman.init()

patterns=detector.acquire_dataset(shapes)




import render_sh

s = render_sh.Scene( patterns, shapes, wavelength_in=60.)  


