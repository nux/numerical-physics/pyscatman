from PIL import Image
import numpy as np

image = Image.open('etopo10.png')                                          # load the grayscale image and
etopo = np.copy(np.asarray(image)[::-1,:])                                # convert it to a 2D numpy array
etopo=etopo/np.max(etopo)
#etopo = etopo[:,::-1]

etopo+=5


import scatman
scatman.use_cpu()

scatman.set_experiment(wavelength=30, angle=30, resolution=512)        # set the experimental parameters

detector = scatman.Detectors.Ideal()                     # set the detector, with a central hole of 50 pixels

shape = scatman.Shapes.RadialMap(a=330, radii=etopo,\
            latitude=40, longitude=-110, rotation=20)               # a is a scaling factor (see class documentation),  the etopo image is used as radial map 
#shape = scatman.Shapes.RadialMap(a=500, radii=etopo,\
            #latitude=90, longitude=0, rotation=0)               # a is a scaling factor (see class documentation),  the etopo image is used as radial map 

#import matplotlib.pyplot as plt
#plt.imshow(etopo, origin="lower")
#plt.show()

patterns=detector.acquire([shape])

import render_sh                        # you can find it in the "tests" folder. Requires the Vtk python module

render_sh.Scene(patterns, [shape], res=280)     # render the shape and the simulated diffraction pattern together, in a fancy way    


