import scatman
import random
import numpy as np

scatman.set_verbose(0)

scatman.set_experiment(wavelength=40, angle=30, resolution=512)

nshapes=1
#scatman.set_gpus([0])
#scatman.use_cpu()
random.seed(17)


parameters = []
shapes = []
sidesize=4
radii=np.ones([6, sidesize, sidesize])*400



#radii[:,::2,::2]*=1.5
radii[1,2,2]=300
#radii[:,1::2,1::2]=200

#radii[:,4,4]=1
#radii[5,1,:]=1.5
#radii[1,:,1]=1.5
#radii[0,-1,-1]*=1.5

#for i in range(6):
    #radii[i,:,:]+=0.2*i

#radii=np.random.rand(6,sidesize,sidesize)*0.5+4.
#radii=radii*100
print(radii.shape)
#radii=radii*400

shape=scatman.Shapes.CubeMesh(radii=radii, delta=0.02, beta=0.01)

radii=shape.get_radii()
import matplotlib.pyplot as plt

fig, axs = plt.subplots(ncols=4, nrows=3, figsize=(12,9))

vmin=0
vmax=np.amax(radii)

for ax in axs.flatten():
    ax.axis('off')

titledist=0.15
    
im = axs[1,0].imshow(radii[0,:,:], vmin=vmin, vmax=vmax)
axs[1,0].text(-1*titledist, 0.5, '0', horizontalalignment='center', verticalalignment='center', transform=axs[1,0].transAxes, fontsize=20)

axs[1,1].imshow(radii[1,:,:], vmin=vmin, vmax=vmax)
axs[1,1].text(0.5, 1.+titledist, '1', horizontalalignment='center', verticalalignment='center', transform=axs[1,1].transAxes, fontsize=20)

axs[1,2].imshow(radii[2,:,:], vmin=vmin, vmax=vmax)
axs[1,2].text(0.5, 1.+titledist, '2', horizontalalignment='center', verticalalignment='center', transform=axs[1,2].transAxes, fontsize=20)

axs[1,3].imshow(radii[3,:,:], vmin=vmin, vmax=vmax)
axs[1,3].text(0.5, 1.+titledist, '3', horizontalalignment='center', verticalalignment='center', transform=axs[1,3].transAxes, fontsize=20)

axs[0,0].imshow(radii[4,:,:], vmin=vmin, vmax=vmax)
axs[0,0].text(-titledist, 0.5, '4', horizontalalignment='center', verticalalignment='center', transform=axs[0,0].transAxes, fontsize=20)

axs[2,0].imshow(radii[5,:,:], vmin=vmin, vmax=vmax)
axs[2,0].text(-titledist, 0.5, '5', horizontalalignment='center', verticalalignment='center', transform=axs[2,0].transAxes, fontsize=20)


#cax = plt.axes([0., 0., 0.1, 0.1])

gs = axs[0, 0].get_gridspec()
gscb = gs[2, 1:].subgridspec(5, 9)
axcb = fig.add_subplot(gscb[2, 1:-1])



cbar=fig.colorbar(im, cax=axcb, ax=axs[0,0], orientation='horizontal')
cbar.set_label('Radius', fontsize=18, labelpad=12)
cbar.outline.set_visible(False)
#axcb.xaxis.set_label_position('top')
axcb.xaxis.set_ticks_position('top')
axcb.tick_params(labelsize=14)

#axcb.spines["top"].set_visible(False)
#axcb.spines["right"].set_visible(False)
#axcb.spines["bottom"].set_visible(False)
#axcb.spines["left"].set_visible(False)

plt.subplots_adjust(wspace=0.05, hspace=0.05)

plt.show()

#exit(0)
##############d

detector = scatman.Detectors.MSFT()

shapes.append(shape)

scatman.set_verbose(0)

scatman.init()

patterns=detector.acquire_dataset(shapes)

import render_sh

s = render_sh.Scene( patterns, shapes, wavelength_in=60.)  


