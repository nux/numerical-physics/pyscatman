import scatman
import numpy as np
import random

resolution=1024
angle=30.
detector="MSFT"
wavelength=50.




ndata=10000


scatman.use_gpu()
scatman.set_gpu_ids([0,1,2,3])

scatman.set_experiment(wavelength=wavelength, angle=angle, resolution=resolution, photon_density=1e7)
scatman.info()
scatman.init()
scatman.set_verbose(1)
random.seed(111)

mydetector=[]

if detector=="MSFT":
    mydetector = scatman.Detectors.MSFT()
if detector=="Ideal":
    mydetector = scatman.Detectors.Ideal(mask_radius=40)

if detector=="MCP":
    mydetector = scatman.Detectors.MCP(mask_radius=40, 
                                   photons_per_channel=3000., channels_per_pixel = 1, 
                                   gain=1, saturation=1000., linearity=0.9, exponent=0.4,
                                   channel_angle=4., channel_direction=270., offset=0)


mydataset = [] #scatman.Dataset()

lmax=10

for i in range(0, ndata):
    midsize=random.uniform(300,1000)
    
    coeffs=[[0,0,1]]
    for l in range(1,lmax+1):
        for m in range(-l, l+1):
            coeffs.append([l,m, np.random.rand()*np.exp(-0.5*l)])
    
    myshape = scatman.Shapes.SphericalHarmonics(a=midsize, coefficients=coeffs)
    
    mydataset.append(myshape)
    
mypattern = mydetector.acquire_dataset(mydataset)



import matplotlib.pyplot as plt
import numpy as np

plt.imshow(np.log(mypattern[3]))
plt.show()
