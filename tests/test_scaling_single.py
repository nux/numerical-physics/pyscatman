import scatman
import numpy as np
import time

t = [time.time()]

resolution=256
angle=30.
wavelength=30.
t.append(time.time())


ntries=10
#scatman.set_gpus([0])


scatman.set_experiment(wavelength=wavelength, angle=angle, resolution=resolution, photon_density=1e7)
scatman.info()
scatman.init()
scatman.set_verbose(0)

mydetector = scatman.Detectors.MSFT()

myshape=scatman.Shapes.Ellipsoid(700, 300, 400, latitude=30, longitude=70, rotation=40);

threads=[]
timing=[]


scatman.use_gpu()
scatman.set_threads(1)
scatman.init()
t.append(time.time())
for i in range(ntries):
    mypattern = mydetector.acquire(myshape)
t.append(time.time())
threads.append(0)
timing.append((t[-1]-t[-2])/ntries)
print("GPU \t")
print("Time    \t", timing[-1],"s") 
print()

scatman.use_cpu()
scatman.init()
for ithreads in range(1,17):
        scatman.set_threads(ithreads)
        scatman.init()

        t.append(time.time())
        for i in range(ntries):
            mypattern = mydetector.acquire(myshape)
        t.append(time.time())
        
        threads.append(ithreads)
        timing.append((t[-1]-t[-2])/ntries)
        print("Threads \t", threads[-1])
        print("Time    \t", timing[-1],"s")
        print()



import h5py

f = h5py.File("scaling_single.h5", "w")


f.create_dataset("time", data=np.array(timing))
f.create_dataset("threads", data=np.array(threads))

f.close()

for i in range(0,len(timing)):
    print(threads[i], timing[i])
    

#import render_sh
#render_sh.Scene(mypattern, mydataset)
