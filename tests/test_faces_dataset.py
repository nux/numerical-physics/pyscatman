import scatman
import random
import numpy as np

scatman.set_verbose(1)

scatman.use_gpu()
scatman.set_gpu_ids([0])

scatman.set_experiment(wavelength=1, angle=30, resolution=512)

nshapes=50
#scatman.set_gpus([0])
#scatman.use_cpu()
random.seed(17)


parameters = []
shapes = []

npoints=40
maxradius=-1
r=10
#points=[[1,0,0],
        #[-1,0,0],
        #[0,1,0],
        #[0.2,-1,0],
        #[0,0,1],
        #[0,0,-1]]

#points = np.random.rand(10,3)-0.5

for s in range(nshapes):
    points=[]
    for i in range(npoints):
        p = np.random.normal(size=3);
        norm = r/(np.sum(p**2))**0.5
        p*=norm
        points.append(p)
        
    #points=np.array(points)

    shapes.append(scatman.Shapes.Faces(points=np.array(points), delta=0.02, beta=0.01, max_radius=maxradius))


detector = scatman.Detectors.MSFT()


scatman.init()

patterns=detector.acquire_dataset(shapes)


import render_sh

s = render_sh.Scene( patterns, shapes, wavelength_in=1.)  


