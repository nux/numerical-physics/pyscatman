import scatman

coords=["projection", "momentum", "angle", "angle-flat", "detector"]
units=[r"[nm$^{-1}$]", r"[nm$^{-1}$]", r"[degrees]",r"[degrees]", ""]
wavelength = 50
angle = 30.

scatman.set_verbose(1)

scatman.set_experiment(wavelength=wavelength, angle=angle, resolution=512);

scatman.info()

scatman.init()

myshape = scatman.Shapes.Ellipsoid(400,250,250, latitude=60, longitude=40, rotation=0)

patterns = []

for coord_type in coords:
    detector=scatman.Detectors.MSFT(coord_type)    
    patterns.append(detector.acquire(myshape))



import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors


fig, axs = plt.subplots(nrows=1, ncols=len(coords), figsize=(16,4))

fig.suptitle(r"$\lambda =$" + str(wavelength) +"nm, " + r"$\theta_{\max}=$" + str(angle) + "°")

vmin=np.amin(patterns[0])
vmax=np.amax(patterns[0])

for i in range(len(coords)):
    axs[i].imshow(patterns[i], extent=(-patterns[i].pixel_size*patterns[i].resolution/2, patterns[i].pixel_size*patterns[i].resolution/2, -patterns[i].pixel_size*patterns[i].resolution/2, patterns[i].pixel_size*patterns[i].resolution/2), norm=colors.LogNorm(vmin=vmin, vmax=vmax))
    axs[i].set_title(patterns[i].coordinates)
    axs[i].get_yaxis().set_visible(False)
    axs[i].set_xlabel(units[i])
    
plt.show()

