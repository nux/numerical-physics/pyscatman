import scatman
import random
import numpy as np

scatman.set_verbose(1)
nshapes=1
#scatman.set_gpus([0])
#scatman.use_cpu()
random.seed(17)


parameters = []
shapes = []
shape_type= []

for n in range(nshapes):
    radii = np.ones([100,100])
    
    for i in range(radii.shape[0]):
        for j in range(radii.shape[1]):
            radii[i,j]+=1.*i/radii.shape[1];
    
    #parameters.append({"a":random.uniform(200,600),"radii" : radii, "latitude" : random.uniform(-90,90), "longitude" : random.uniform(0,180)})
    parameters.append({"a":random.uniform(200,600),"radii" : radii, "latitude" : 90, "longitude" : 0})

    shape = scatman.Shapes.RadialMap(**parameters[-1])
    shapes.append(shape)
    shape_type.append(shape.get_name())


detector = scatman.Detectors.Ideal(mask_radius=50)

scatman.info()
scatman.init()

patterns=detector.acquire_dataset(shapes)




import render_sh

s = render_sh.Scene( patterns, shapes, wavelength_in=60.)  


