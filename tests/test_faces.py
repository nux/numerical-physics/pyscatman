import scatman
import random
import numpy as np

scatman.set_verbose(1)

scatman.set_experiment(wavelength=1, angle=30, resolution=512)

nshapes=1
#scatman.set_gpus([0])
#scatman.use_cpu()
random.seed(17)


parameters = []
shapes = []

npoints=18
maxradius=18



for r in [10,12,6,9]:
    r=10
    points=[]
    #points=[[1,0,0],
            #[-1,0,0],
            #[0,1,0],
            #[0.2,-1,0],
            #[0,0,1],
            #[0,0,-1]]

    #points = np.random.rand(10,3)-0.5

    for i in range(npoints):
        p = np.random.normal(size=3);
        norm = r/(np.sum(p**2))**0.5
        p*=norm
        points.append(p)
        
    points=np.array(points)

    shapes.append(scatman.Shapes.Faces(points=points, delta=0.02, beta=0.01, max_radius=maxradius))
    shapes.append(scatman.Shapes.Faces(points=points, delta=0.02, beta=0.01, max_radius=maxradius,symmetry=1))


points=[[10,0,0],
        [-10, 0, 0],
        [0,10,0],
        [0,-10,0],
        [0,0,10],
        [0,0,-10]]



shapes.append(scatman.Shapes.Faces(points=np.array(points), delta=0.02, beta=0.01, max_radius=maxradius, latitude=70, longitude=30, rotation=45))
#shapes.append(scatman.Shapes.Faces(points=np.array(points), delta=0.02, beta=0.01, max_radius=maxradius, symmetry=1, latitude=70, longitude=30, rotation=45))









detector = scatman.Detectors.MSFT()


scatman.init()

patterns=detector.acquire_dataset(shapes)


import render_sh

s = render_sh.Scene( patterns, shapes, wavelength_in=1.)  


