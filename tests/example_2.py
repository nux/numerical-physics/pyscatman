import scatman

scatman.set_experiment(wavelength=40, angle=30, resolution=512, photon_density=1.e2);
#scatman.set_experiment(wavelength=10, angle=30, resolution=512, photon_density=4.e5);

scatman.set_fourier_oversampling(2.)

mydetector = scatman.Detectors.Ideal()

myshape = scatman.Shapes.Ellipsoid(400,400,400, delta=0.001, beta=0.01)
#myshape = scatman.Shapes.Ellipsoid(400,400,400, delta=-0.05, beta=0.05)

#myshape = scatman.Shapes.Ellipsoid(a=1200, b=400,c=400, beta=1e-1, delta = 1e-1, latitude=60, longitude=30, rotation=0)
mypattern = mydetector.acquire(myshape)

import matplotlib.pyplot as plt
import numpy as np

plt.imshow(np.log(mypattern), cmap="inferno")
plt.show()

