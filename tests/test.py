import scatman
import numpy as np
import time
from PIL import Image

t = [time.time()]

resolution=512
angle=30.
detector="Ideal"
wavelength=30.
t.append(time.time())

mydetector=[]

scatman.set_threads(4);
#scatman.set_gpus([4,5,6]);
scatman.set_experiment(wavelength=wavelength, angle=angle, resolution=resolution, photon_density=1e6);


if detector=="MSFT":
    mydetector = scatman.Detectors.MSFT()
if detector=="Ideal":
    mydetector = scatman.Detectors.Ideal(flat=True)
if detector=="MCP":
    mydetector = scatman.Detectors.MCP(mask_radius=40, 
                                   gain=1000, saturation=30000., linearity=0, exponent=0.7, offset=0)
    
cpu_patterns=[]
gpu_patterns=[]
shapes=[]

shapes.append(scatman.Shapes.Ellipsoid(400,400,400, latitude=90, longitude=0,rotation=0, beam_position=1))
shapes.append(scatman.Shapes.Dumbbell(300,200,200,200,90, latitude=60, longitude=-140, rotation=20))

scatman.init()

scatman.use_cpu()
scatman.set_verbose(1)
print("Testing on CPU...")

for shape in shapes:
    t.append(time.time())
    cpu_patterns.append(mydetector.acquire(shape))
    t.append(time.time())
    print("Detector.acquire("+shape.get_name()+")", int((t[-1]-t[-2])*1000), " ms")

val=scatman.use_gpu()
print(val)
if val==1:
    print("Testing on GPU")
    
    for shape in shapes:
        t.append(time.time())
        gpu_patterns.append(mydetector.acquire(shape))
        t.append(time.time())
        print("Detector.acquire("+shape.get_name()+")", int((t[-1]-t[-2])*1000), " ms")



print()
print("Total execution time: ", int((t[-1]-t[0])*1000), " ms")



angmin=3.5
angmax=30.
nangs=180
dang=(angmax-angmin)/float(nangs)
angles=np.zeros([nangs])
for i in range(0,nangs):
    angles[i]=float(i)*dang+angmin


#######
from matplotlib import pyplot as plt
import inspect 

plt.rcParams['image.cmap'] = 'inferno'

figcpu, axcpu = plt.subplots(nrows=1, ncols=len(cpu_patterns))
figcpu.suptitle('CPU')
for ipatt in range(len(cpu_patterns)):
    if detector!="MCP":
        axcpu[ipatt].imshow(np.log(cpu_patterns[ipatt]))
    else:
        axcpu[ipatt].imshow(cpu_patterns[ipatt])        
    axcpu[ipatt].set_title(shapes[ipatt].get_name())

figgpu=None
axgpu=None

if len(gpu_patterns)>0:
    figgpu, axgpu = plt.subplots(nrows=1, ncols=len(gpu_patterns))
    figcpu.suptitle('GPU')
    for ipatt in range(len(cpu_patterns)):
        if detector!="MCP":
            axgpu[ipatt].imshow(np.log(gpu_patterns[ipatt]))
        else:
            axgpu[ipatt].imshow(gpu_patterns[ipatt]) 
        axgpu[ipatt].set_title(shapes[ipatt].get_name())


plt.show()

