#!/usr/bin/python3
import vtk
from vtk.util import numpy_support

import scatman
import numpy as np
import time
import scipy.ndimage.filters as filters


isovalue = 0.5


class Scene:
    wavelength=60.
    reduction=16.

    def show_density(self):

        print(str(self.index) + ": " + self.shapes[self.index].get_name())
        
        density, dx=self.shapes[self.index].get(self.render_res)
        density=filters.gaussian_filter(density,0.8)
        size=self.shapes[self.index].get_size()
        
        density = density[::-1,:,:]/np.amax(density)
        
        VTK_data = numpy_support.numpy_to_vtk(num_array=density.ravel(), deep=True, array_type=vtk.VTK_DOUBLE)
    
        spacing = size/self.wavelength/density.shape[0]/self.reduction
        origin=-size/self.wavelength/2./self.reduction
        self.imdata.SetDimensions(density.shape)
        self.imdata.SetSpacing([spacing,spacing,spacing])
        self.imdata.SetOrigin([origin,origin,origin])
        self.imdata.GetPointData().SetScalars(VTK_data)
        self.imdata.Modified()
        
    def show_pattern(self):
        pattern=self.patterns[self.index]
        maxval=np.amax(pattern)
        
        pattern/=maxval
        
        pattern= np.log(np.array(pattern))
        
        cutlevel=-15
        minval=np.amin(pattern[pattern>-20])
        
        if(minval>cutlevel):
            pattern-=minval
            pattern[pattern<0]=0
        else:
            pattern-=cutlevel
            pattern[pattern<0]=0
        
        pattern[pattern<0]=0
        
        
        pattern/=np.amax(pattern)
        
        VTK_pattern = numpy_support.numpy_to_vtk(num_array=pattern[::-1, ::-1].transpose().flatten(), deep=True, array_type=vtk.VTK_DOUBLE)        
        
        self.impattern.SetDimensions((pattern.shape[0], pattern.shape[1], 1))
        self.impattern.SetSpacing([3./pattern.shape[0],3./pattern.shape[1],1])
        self.impattern.GetPointData().SetScalars(VTK_pattern)
        self.impattern.Modified()
        
    def keypress_callback(self, obj, ev):
        key = obj.GetKeySym()
        print(key, 'was pressed')
        if key=="Right" or key=="Left":
            if key=="Right":
                self.index=self.index+1
                if self.index >=len(self.patterns):
                    self.index=0
            if key=="Left":
                self.index=self.index-1
                if self.index<0:
                    self.index=len(self.patterns)-1
            print(self.index)
            self.show_density()
            self.show_pattern()
            self.window.Render()
        if key == "Escape":
            self.interactor.GetRenderWindow().Finalize()
            self.interactor.TerminateApp()
            
    
    def __init__(self, in_patterns, in_shapes, wavelength_in=60., res=80):
        self.render_res=res
        self.wavelength=wavelength_in
        self.patterns=in_patterns
        self.shapes=in_shapes

            
        p0=[0.0, 0.0, 5.0]
        p1=[0.0, 0.0, -3.0]
        lineSource=vtk.vtkLineSource()
        lineSource.SetPoint1(p0);
        lineSource.SetPoint2(p1);
        lineSource.Update();
        
        tubeFilter = vtk.vtkTubeFilter();
        tubeFilter.SetInputConnection(lineSource.GetOutputPort());
        tubeFilter.SetRadius(.025); 
        tubeFilter.SetNumberOfSides(50);
        tubeFilter.Update();
        
        
        self.index=0;

        self.imdata = vtk.vtkImageData()

        #self.imdata.SetOrigin([-1,-1,-1])
        

        self.impattern = vtk.vtkImageData()
        # fill the vtk image data object

        self.impattern.SetOrigin([-1.5,-1.5, 5])


        self.ltpattern=vtk.vtkLookupTable()
        self.ltpattern.SetScaleToLinear()
        self.ltpattern.SetTableRange(0, 1)

        ncolors=256
        self.ltpattern.SetNumberOfColors(ncolors)

        for i in range(0,ncolors):
            val = float(i+1)/float(ncolors)
            red = min(3.*val,1)
            green = min(0 if val<1./3. else 3.*(val-1./3.), 1)
            blue = 0 if val< 2./3. else 3.*(val-2./3.)
            self.ltpattern.SetTableValue(i, red, green, blue)


        self.mcpattern = vtk.vtkImageMapToColors()
        self.mcpattern.SetLookupTable(self.ltpattern)
        self.mcpattern.SetInputData(self.impattern)


        self.surface = vtk.vtkMarchingCubes()
        self.surface.SetInputData(self.imdata);
        self.surface.ComputeNormalsOn();
        self.surface.SetValue(0, isovalue);


        ###############################################
        self.mapper = vtk.vtkDataSetMapper()
        self.mapper.SetInputConnection(self.surface.GetOutputPort())

        self.linemapper = vtk.vtkPolyDataMapper()  
        self.linemapper.SetInputConnection(tubeFilter.GetOutputPort())
        
        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)
        self.actor.GetMapper().ScalarVisibilityOff()
        self.actor.GetProperty().SetAmbient(0);
        self.actor.GetProperty().SetDiffuse(1);
        #self.actor.GetProperty().SetColor(0.8, 0.8, 1);
        self.actor.GetProperty().SetColor(174/245., 209/256., 207/256.);
        #self.actor.GetProperty().SetAmbientColor(1,0,0)
        
        self.actor2 = vtk.vtkImageActor()
        self.actor2.GetMapper().SetInputConnection(self.mcpattern.GetOutputPort())
        self.actor2.GetProperty().SetAmbient(1);
        self.actor2.GetProperty().SetDiffuse(0);
        #self.actor.GetProperty().SetColor(174/245., 209/256., 207/256.);

        self.lineactor=vtk.vtkActor()
        self.lineactor.SetMapper(self.linemapper);
        self.lineactor.GetProperty().SetLineWidth(20);        
        self.lineactor.GetProperty().SetColor(1,0,0); 
        self.lineactor.GetProperty().SetOpacity(0.15)
        
        self.window = vtk.vtkRenderWindow()
        self.window.SetSize(1200, 800)

        self.interactor = vtk.vtkRenderWindowInteractor()
        self.interactor.SetRenderWindow(self.window)
        self.interactor.AddObserver('KeyPressEvent', self.keypress_callback, 1.0)

        self.camera = vtk.vtkCamera()
        self.camera.SetPosition(-3.5, 1., -6);
        self.camera.SetFocalPoint(0, 0, 0);
        self.camera.SetParallelProjection(0)

        self.renderer = vtk.vtkRenderer()

        #lk = vtk.vtkLightKit()
        #lk.RemoveLightsFromRenderer(renderer)

        self.renderer.SetActiveCamera(self.camera);

        self.window.AddRenderer(self.renderer)

        self.light = vtk.vtkLight()
        self.light.SetLightTypeToSceneLight()
        self.light.SetPosition(0,0,-8)
        #light.SetPositional(True)
        #self.light.SetConeAngle(30)
        #self.light.SetFocalPoint(0, 0, 0)
        #self.light.SetDiffuseColor(1,0.8,0.8)
        #self.light.SetAmbientColor(1,0.8,0.8)
        #self.light.SetSpecularColor(0.,0.,0.)
        
        self.light.SetConeAngle(180)
        self.light.SetFocalPoint(0, 0, 0)
        self.light.SetDiffuseColor(1,1,1)
        self.light.SetAmbientColor(1,1,1)
        self.light.SetSpecularColor(1,1,1)
        
   
        self.light2 = vtk.vtkLight()
        self.light2.SetLightTypeToSceneLight()
        self.light2.SetPosition(0,0,8)
        #light.SetPositional(True)
        self.light2.SetConeAngle(30)
        self.light2.SetFocalPoint(0, 0, 0)
        self.light2.SetDiffuseColor(1,0.,0.)
        self.light2.SetAmbientColor(1,0.,0.)
        self.light2.SetSpecularColor(0.,0.,0.)


        self.renderer.AddActor(self.actor)
        self.renderer.AddActor(self.actor2)
        self.renderer.AddActor(self.lineactor)

        self.renderer.AddLight(self.light)
        self.renderer.AddLight(self.light2)
        # Setting the background to blue.
        #self.renderer.SetBackground(0.25, 0.25, 0.25)
        self.renderer.SetBackground(1, 1, 1)


        self.show_density()
        self.show_pattern()
        self.window.Render()
        
        

        self.interactor.Start()

    
    



