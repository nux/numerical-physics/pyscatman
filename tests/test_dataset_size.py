import scatman
import numpy as np
import time

resolution=256
angle=30.
wavelength=1.




ndata=1024

sizes=[1,2,4,8,16,32,64, 128, 256, 512, 1024]

scatman.use_gpu()
scatman.set_gpu_ids([0,1,2,3])
#scatman.set_gpu_ids([0])

scatman.set_experiment(wavelength=wavelength, angle=angle, resolution=resolution, photon_density=1e7, rescale=128)
scatman.info()
scatman.init()
scatman.set_verbose(0)
#random.seed(111)

mydetector = scatman.Detectors.MSFT(coordinates="detector")

mydataset = [] #scatman.Dataset()

sideres=5

rmin=5
rmax=20

np.random.seed(10)

for i in range(0, ndata):
    #radii=np.random.rand(6,sideres,sideres)*(rmax-rmin)+ rmin
        
    #myshape = scatman.Shapes.CubeMap(radii=radii)

    radii=np.random.rand(3*sideres,2*sideres)*(rmax-rmin)+ rmin
        
    myshape = scatman.Shapes.RadialMap(radii=radii)

    #radii=np.random.rand(6,sideres,sideres)*(rmax-rmin)+ rmin
        
    #myshape = scatman.Shapes.Ellipsoid(a=20)
    
    mydataset.append(myshape)
    
t = [time.time()]

for size in sizes:

    t.append(time.time())

    patterns=[]
    
    for i in range(0,ndata,size):
        patterns.extend( mydetector.acquire(mydataset[i:i+size]))
    t.append(time.time())
    
    timeperpattern=((t[-1]-t[-2])*1000./len(patterns))
    print("Dataset size: ", size, "\tTime per pattern ",timeperpattern , " ms")
    

#import matplotlib.pyplot as plt
#import numpy as np

#plt.imshow(np.log(mypattern[3]))
#plt.show()
