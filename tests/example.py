import scatman
import numpy as np




scatman.set_threads(4);
scatman.set_gpus([4,5,6]);
scatman.set_experiment(wavelength=30., angle=30, resolution=512);




mydetector = scatman.Detectors.Ideal()

myellipsoid = scatman.Shapes.Ellipsoid(400,400,400, latitude=90, longitude=0, rotation=0)
mydumbbell = scatman.Shapes.Dumbbell(300,200,200,200,90, latitude=60, longitude=-140, rotation=20)


myellipsoid_pattern = mydetector.acquire(myellipsoid)
mydumbbell_pattern = mydetector.acquire(mydumbbell)



import matplotlib.pyplot as plt

fig, ax = plt.subplots(nrows=1, ncols=2)

ax[0].imshow(np.log(myellipsoid_pattern))
ax[1].imshow(np.log(mydumbbell_pattern))

plt.show()

