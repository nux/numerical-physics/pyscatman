import scatman
import random


scatman.set_verbose(1)
nshapes=10
scatman.set_gpus([0])
random.seed(17)

parameters = []
shapes = []
shape_type= []

for n in range(nshapes):
    l = random.randrange(1,16)
    m = random.randrange(-l,l)
    coeff = random.uniform(-0.3,0.3)
    parameters.append({"a":random.uniform(200,600),"coefficients" : [[0,0,1],[l,m,coeff]], "latitude" : random.uniform(-90,90), "longitude" : random.uniform(0,180)})
    shape = scatman.Shapes.SphericalHarmonics(**parameters[-1])
    shapes.append(shape)
    shape_type.append(shape.get_name())
    
    
'''
for n in range(nshapes):
    l = random.randrange(1,16)
    m = random.randrange(-l,l)
    coeff = random.uniform(-0.3,0.3)
    size=random.uniform(200,600)
    parameters.append({"a": size, "b": size, "c": size,  "latitude" : 90, "longitude" : 0})
    shape = scatman.Shapes.Ellipsoid(**parameters[-1])
    shapes.append(shape)
    shape_type.append(shape.get_name())
'''
#scatman.use_cpu()


detector = scatman.Detectors.Ideal(mask_radius=50)

scatman.info()
scatman.init()

patterns=detector.acquire_dataset(shapes)



##myshape = scatman.Shapes.SphericalHarmonics(10, [0,0,1, 2,0,0.3])
#myshape = scatman.Shapes.SphericalHarmonics(10, [0,0,1,2,0,-0.3], latitude=30)

##myshape = scatman.Shapes.Ellipsoid(30, 40,20)





import render_sh


#data=myshape.get(120)

s = render_sh.Scene( patterns, shapes, wavelength_in=60.)  






#from mpl_toolkits import mplot3d
#from skimage import measure
#import matplotlib
#import matplotlib.pyplot as plt
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
#from matplotlib.figure import Figure
#from mpl_toolkits.mplot3d import Axes3D
#import matplotlib.image as mpimg


#data=myshape.get(40, 3,1)


#fig = plt.figure()
##ax = fig.add_subplot(111, projection='3d')
#ax = fig.gca(projection='3d')
##ax.set_aspect('equal')
##set_aspect_equal_3d(ax)


#verts, faces, aa, bb = measure.marching_cubes(data, 0.33, spacing=(1, 1, 1))
#ax.plot_trisurf(verts[:,0], verts[:,1], faces, verts[:,2], color=(0.9,0.6,0.5,0.4), lw=0)
#plt.show()
