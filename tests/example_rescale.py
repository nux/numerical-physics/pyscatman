import scatman

scatman.info()

scatman.set_verbose(1)

size = 1024
newsize = 128

mydetector = scatman.Detectors.MSFT()

myshape = scatman.Shapes.Ellipsoid(400,250,250, latitude=60, longitude=40, rotation=0)


scatman.set_experiment(wavelength=45., angle=30, resolution=size);

mypattern = mydetector.acquire(myshape)


scatman.set_experiment(wavelength=45., angle=30, resolution=size, rescale=newsize);

mypattern_scaled = mydetector.acquire(myshape)

import matplotlib.pyplot as plt
import numpy as np
import cv2
mypattern=np.asarray(mypattern)
mypattern_scipy =   cv2.resize(mypattern, (newsize, newsize), interpolation = cv2.INTER_AREA)    

fig, ax = plt.subplots(ncols=3, nrows=1)

vmin=np.min(np.log(mypattern_scipy))
vmax = np.max(np.log(mypattern_scipy))

ax[0].imshow(np.log(mypattern), vmin=vmin, vmax=vmax)
ax[1].imshow(np.log(mypattern_scaled), vmin=vmin, vmax=vmax)
ax[2].imshow(np.log(mypattern_scipy), vmin=vmin, vmax=vmax)

plt.show()

