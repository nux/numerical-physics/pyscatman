import scatman
import numpy as np

wavelength = 50
angle = 30.
maxshift=50.
nshift=4

shifts=[]

for i in range(nshift):
    shifts.append(np.random.uniform(-maxshift, maxshift, size=2))


scatman.set_verbose(1)

scatman.set_experiment(wavelength=wavelength, angle=angle, resolution=512);

scatman.info()

scatman.init()


detector=scatman.Detectors.MSFT(coordinates="detector")    

shapes = []

for shift in shifts:
    print(shift)
    shapes.append(scatman.Shapes.Ellipsoid(400,250,250, latitude=60, longitude=40, rotation=0, xshift=shift[0], yshift=shift[1]))

patterns = detector.acquire(shapes)



import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors


fig, axs = plt.subplots(nrows=1, ncols=len(shifts), figsize=(16,4))

fig.suptitle(r"$\lambda =$" + str(wavelength) +"nm, " + r"$\theta_{\max}=$" + str(angle) + "°")

vmin=np.amin(patterns[0])
vmax=np.amax(patterns[0])

for i in range(len(shifts)):
    axs[i].imshow(patterns[i], extent=(-patterns[i].pixel_size*patterns[i].resolution/2, patterns[i].pixel_size*patterns[i].resolution/2, -patterns[i].pixel_size*patterns[i].resolution/2, patterns[i].pixel_size*patterns[i].resolution/2), norm=colors.LogNorm(vmin=vmin, vmax=vmax))
    axs[i].set_title("x="+str(shifts[i][0])+" , y="+str(shifts[i][1]))
    axs[i].get_yaxis().set_visible(False)
    
plt.show()

