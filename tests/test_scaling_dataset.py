import scatman
import numpy as np
import time

t = [time.time()]

resolution=1024
angle=30.
wavelength=30.
t.append(time.time())


ntries=10
datasize=[1,10,20,50,100]
#scatman.set_gpus([0])


scatman.set_experiment(wavelength=wavelength, angle=angle, resolution=resolution, photon_density=1e7)
scatman.info()
scatman.init()
scatman.set_verbose(0)

mydetector = scatman.Detectors.MSFT()

myshape=[]
for i in range(0, datasize[-1]):
    myshape.append(scatman.Shapes.Ellipsoid(700, 300, 400, latitude=30, longitude=70, rotation=40))

#timing_cpu=[]
timing_gpu=[]

#scatman.use_cpu()
#scatman.init()
#for size in datasize:
        #scatman.init()

        #t.append(time.time())
        #mypattern = mydetector.acquire_dataset(myshape[:size])
        #t.append(time.time())
        
        #timing_cpu.append((t[-1]-t[-2])/size)
        #print("Dataset \t", size)
        #print("Time CPU\t", timing_cpu[-1],"s")
        #print()



scatman.use_gpu()
scatman.init()
for size in datasize:
        scatman.init()

        t.append(time.time())
        for i in range(ntries):
            mypattern = mydetector.acquire_dataset(myshape[:size])
        t.append(time.time())
        
        timing_gpu.append((t[-1]-t[-2])/size/ntries)
        print("Dataset \t", size)
        print("Time GPU\t", timing_gpu[-1],"s")
        print()

import h5py

f = h5py.File("scaling_dataset.h5", "w")


#f.create_dataset("time_cpu", data=np.array(timing_cpu))
f.create_dataset("time_gpu", data=np.array(timing_gpu))
f.create_dataset("size", data=np.array(datasize))

f.close()

for i in range(0,len(datasize)):
    print(datasize[i], timing_cpu[i], timing_gpu[i])
    

#import render_sh
#render_sh.Scene(mypattern, mydataset)
