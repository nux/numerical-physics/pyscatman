import scatman

scatman.use_gpu()

scatman.set_verbose(1)

scatman.set_experiment(wavelength=40, angle=30, resolution=512);

mydetector = scatman.Detectors.MSFT()

myshape = scatman.Shapes.Ellipsoid(200,400,300, latitude=60, longitude=40, rotation=0)

scatman.init()

mypattern = mydetector.acquire(myshape, field=True)


import matplotlib.pyplot as plt
import numpy as np

fig, axs = plt.subplots(nrows=2, ncols=2, figsize=(10,10))

field=mypattern.get_field()

axs[0,0].set_title("Pattern (logscale)")
axs[0,0].imshow(np.log(mypattern))

axs[0,1].set_title("Phases")
axs[0,1].imshow(np.angle(field), cmap="twilight")

axs[1,0].set_title("Real")
axs[1,0].imshow(np.real(field), cmap="Reds_r")

axs[1,1].set_title("Imaginary")
axs[1,1].imshow(np.imag(field), cmap="Blues_r")

plt.show()

