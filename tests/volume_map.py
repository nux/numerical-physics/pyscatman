import scatman
import numpy as np



def add_sphere(data, center, radius, dx):
    xdim = data.shape[2]
    ydim = data.shape[1]
    zdim = data.shape[0]
    for z in range(zdim):
        for y in range(ydim):
            for x in range(xdim):
                rad = ((x-center[2])**2+ (y-center[1])**2+ (z-center[0])**2)*dx**2
                if rad <(radius)**2:
                    data[z,y,x]=True

scatman.info()

scatman.set_verbose(1)

scatman.set_experiment(wavelength=45., angle=30, resolution=512);

mydetector = scatman.Detectors.MSFT()

#scatman.use_cpu()

size = 500.
dim = 100
dx = size/float(dim)
xdim = dim
ydim= dim
zdim = dim

    
data=np.zeros([zdim,ydim,xdim], dtype=bool)

add_sphere(data, [zdim/2, ydim/2, xdim/2+xdim/6], size/3, dx)


add_sphere(data, [zdim/2, ydim/2, xdim/2-xdim/5], size/8, dx)

print(data.shape)

myshape = scatman.Shapes.VolumeMap(dx, data, delta=0.01, beta=0.01 ,latitude=50, longitude=130, rotation=0)

#rendering, dx = myshape.get(80)

#print(dx)
#rendering
#print(rendering.shape)

scatman.init()

mypattern = mydetector.acquire(myshape)

import render_sh
render_sh.Scene([mypattern], [myshape])


#import matplotlib.pyplot as plt
#import numpy as np



#plt.imshow(np.log(mypattern))
#plt.show()

