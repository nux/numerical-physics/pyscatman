#pragma once

#ifdef SINGLE_PRECISION
//     #pragma message("Compiling in single precision.. ")
    #define FLOAT_TYPE          float
    #define FFTW_COMPLEX        fftwf_complex
    #define FFTW_PLAN           fftwf_plan
    #define FFTW_PLAN_DFT_2D    fftwf_plan_dft_2d
    #define FFTW_DESTROY_PLAN   fftwf_destroy_plan
    #define FFTW_MALLOC         fftwf_malloc
    #define FFTW_EXECUTE        fftwf_execute
    #define FFTW_FREE           fftwf_free
    #define CUDA_COMPLEX        cufftComplex
    #define M_PIF 3.14159265f
//     #define IMAG                1if
#else
//     #pragma message("Compiling in double precision.. ")
    #define FLOAT_TYPE          double
    #define FFTW_COMPLEX        fftw_complex
    #define FFTW_PLAN           fftw_plan
    #define FFTW_PLAN_DFT_2D    fftw_plan_dft_2d
    #define FFTW_DESTROY_PLAN   fftw_destroy_plan
    #define FFTW_MALLOC         fftw_malloc
    #define FFTW_EXECUTE        fftw_execute
    #define FFTW_FREE           fftw_free
    #define CUDA_COMPLEX        cufftDoubleComplex
    #define M_PIF 3.14159265358979323846
//     #define IMAG                1i

#endif

typedef FLOAT_TYPE COMPLEX_TYPE[2];

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif


// #define _HAS_TR1_NAMESPACE 1

enum shape_type{Ellipsoid, Dumbbell, RadialMap, SphericalHarmonics, ShellSphere, MoonSphere, VolumeMap, VolumeDistribution, CubeMap,CubeMesh, CubeFaces, Faces};
  
// namespace Devices{
//     int nthreads
//     
//     
// }
