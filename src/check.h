#pragma once
#include <stdexcept>
#include <string>

class LeftOpen 
{
public:
  template <class T>
  static bool Compare (const T value, const T range) { return value > range; }
  template <class T> static std::string Bracket(const T value){return std::string("( ")+ std::to_string(value);}
};

class LeftClosed
{
public:
  template <class T>
  static bool Compare (const T value, const T range) { return value >= range; }
  template <class T> static std::string Bracket(const T value){return std::string("[ ")+ std::to_string(value);}
};

class RightOpen 
{
public:
  template <class T>
  static bool Compare (const T value, const T range) { return value < range; }
  template <class T> static std::string Bracket(const T value){return  std::to_string(value) + std::string(" )");}
};

class RightClosed
{
public:
  template <class T>
  static bool Compare (const T value, const T range) { return value <= range; }
  template <class T> static std::string Bracket(const T value){return  std::to_string(value) + std::string(" ]");}
};


class RightNone
{
public:
  template <class T>
  static bool Compare (const T value, const T range) { return 1; }
  template <class T> static std::string Bracket(const T value){return std::string("inf )");}
};

class LeftNone
{
public:
  template <class T>
  static bool Compare (const T value, const T range) { return 1; }
  template <class T> static std::string Bracket(const T value){return std::string("( -inf");}
};

template <class L, class R, class T>
void CheckRange (T value, T min, T max, std::string argname = "Arg 1") { 
    
    bool check = L::Compare(value, min) && R::Compare(value, max); 
    
    if(!check){
         throw std::invalid_argument(argname + 
                          " must be in the range " + L::Bracket(min) + 
                          " , "+ R::Bracket(max));
        }
    }

template <class T>
void CheckOptions (T value, std::vector<T> options, std::string argname = "Arg 1") { 
    bool found = 0;
    
    for(int i=0; i<options.size(); i++) if(value==options[i]) found=1;
    
    if(!found){
         std::string options_list = "[ ";
         for(int i=0; i<options.size(); i++){
             options_list += options[i];
             if(i<options.size()-1)
                 options_list+=" , ";
         }
         options_list+=" ]";
         
         throw std::invalid_argument(argname + 
                          " must be one of " + options_list);
        }
    }    

    
    
    
template <class T>
void CheckEqual (T value, T correct, std::string argname = "Arg 1") { 
    
    bool check = (value==correct) ; 
    
    if(!check){
         throw std::invalid_argument(argname + 
                          " must be equal to " + std::to_string(correct));
        }
    }
    
// void CheckThrow (std::string message) { 

//     throw std::invalid_argument(message);
//     }
    

