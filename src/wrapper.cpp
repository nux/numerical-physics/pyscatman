#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include "check.h"
#include "datatypes.h"
#include "context.h"
#include "shapes/shape.h"
#include "shapes/dumbbell.h"
#include "shapes/ellipsoid.h"
#include "shapes/shell_sphere.h"
#include "shapes/moon_sphere.h"
#include "shapes/radial_map.h"
#include "shapes/spherical_harmonics.h"
#include "shapes/volume_map.h"
#include "shapes/volume_distribution.h"
#include "shapes/cube_map.h"
#include "shapes/cube_mesh.h"

#include "shapes/cube_faces.h"
#include "shapes/faces.h"


#include "detectors/detector.h"
#include "detectors/ideal.h"
#include "detectors/msft.h"
#include "detectors/mcp.h"
#include "pattern.h"


#include <variant>
#include <string>
#include <tuple>
#include <omp.h>
namespace py = pybind11;


Context context;

// // // // // // // // // // // // // // // // // // // // // // // // 
template <class DetectorBase = Detectors::Detector> class PyDetector : public DetectorBase {
public:
    using DetectorBase::DetectorBase; // Inherit constructors
    int apply_features(Pattern& pattern) override { 
        PYBIND11_OVERLOAD_PURE(int , DetectorBase, apply_features, pattern); }
};


template <class MSFTBase = Detectors::MSFT> class PyDetectorMSFT : public PyDetector<MSFTBase> {
public:
    using PyDetector<MSFTBase>::PyDetector;
    int apply_features(Pattern& pattern) override { 
        PYBIND11_OVERLOAD_PURE(int , MSFTBase, apply_features, pattern); }
};

template <class IdealBase = Detectors::Ideal> class PyDetectorIdeal : public PyDetector<IdealBase> {
public:
    using PyDetector<IdealBase>::PyDetector;
    int apply_features(Pattern& pattern) override { 
        PYBIND11_OVERLOAD_PURE(int , IdealBase, apply_features, pattern); }
};

template <class MCPBase = Detectors::MCP> class PyDetectorMCP : public PyDetector<MCPBase> {
public:
    using PyDetector<MCPBase>::PyDetector;
    int apply_features(Pattern& pattern) override { 
        PYBIND11_OVERLOAD_PURE(int , MCPBase, apply_features, pattern); }
};


// // // // // // // // // // // // // // // // // // // // // // // // 

template <class ShapeBase = Shapes::Shape> class PyShape : public ShapeBase {
public:
    using ShapeBase::ShapeBase; // Inherit constructors
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//         PYBIND11_OVERLOAD_PURE(int , ShapeBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, ShapeBase, get_name); }
};
template <class EllipsoidBase = Shapes::Ellipsoid> class PyEllipsoid : public PyShape<EllipsoidBase> {
public:
    using PyShape<EllipsoidBase>::PyShape;
//    __host__ __device__  FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , EllipsoidBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, EllipsoidBase, get_name); }    
};
template <class DumbbellBase = Shapes::Dumbbell> class PyDumbbell : public PyShape<DumbbellBase> {
public:
    using PyShape<DumbbellBase>::PyShape;
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , DumbbellBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, DumbbellBase, get_name); }
};

template <class ShellSphereBase = Shapes::ShellSphere> class PyShellSphere : public PyShape<ShellSphereBase> {
public:
    using PyShape<ShellSphereBase>::PyShape;
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , ShellSphereBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, ShellSphereBase, get_name); }    
};

template <class MoonSphereBase = Shapes::MoonSphere> class PyMoonSphere : public PyShape<MoonSphereBase> {
public:
    using PyShape<MoonSphereBase>::PyShape;
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , MoonSphereBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, MoonSphereBase, get_name); }
};


template <class RadialMapBase = Shapes::RadialMap> class PyRadialMap : public PyShape<RadialMapBase> {
public:
    using PyShape<RadialMapBase>::PyShape;
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , RadialMapBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, RadialMapBase, get_name); }
};

template <class SphericalHarmonicsBase = Shapes::SphericalHarmonics> class PySphericalHarmonics : public PyShape<SphericalHarmonicsBase> {
public:
    using PyShape<SphericalHarmonicsBase>::PyShape;
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, SphericalHarmonicsBase, get_name); }
};

template <class VolumeMapBase = Shapes::VolumeMap> class PyVolumeMap : public PyShape<VolumeMapBase> {
public:
    using PyShape<VolumeMapBase>::PyShape;
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , VolumeMapBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, VolumeMapBase, get_name); }
};

template <class VolumeDistributionBase = Shapes::VolumeDistribution> class PyVolumeDistribution : public PyShape<VolumeDistributionBase> {
public:
    using PyShape<VolumeDistributionBase>::PyShape;
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , VolumeDistributionBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, VolumeDistributionBase, get_name); }
};

template <class CubeMapBase = Shapes::CubeMap> class PyCubeMap : public PyShape<CubeMapBase> {
public:
    using PyShape<CubeMapBase>::PyShape;
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , CubeMapBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, CubeMapBase, get_name); }
};

template <class CubeMeshBase = Shapes::CubeMesh> class PyCubeMesh : public PyShape<CubeMeshBase> {
public:
    using PyShape<CubeMeshBase>::PyShape;
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , CubeMeshBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, CubeMeshBase, get_name); }
};


template <class CubeFacesBase = Shapes::CubeFaces> class PyCubeFaces : public PyShape<CubeFacesBase> {
public:
    using PyShape<CubeFacesBase>::PyShape;
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , CubeFacesBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, CubeFacesBase, get_name); }
};
template <class FacesBase = Shapes::Faces> class PyFaces : public PyShape<FacesBase> {
public:
    using PyShape<FacesBase>::PyShape;
//     __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z) override { 
//             PYBIND11_OVERLOAD_PURE(FLOAT_TYPE , FacesBase, x, y, z); }
    std::string get_name() override { PYBIND11_OVERLOAD_PURE(std::string, FacesBase, get_name); }
};


PYBIND11_MODULE(scatman, sm)
{    
    sm.def("set_cpu_threads", [](int nthreads){
                        if(nthreads<0) nthreads = omp_get_max_threads();
                        CheckRange<LeftClosed, RightClosed,int>(nthreads, 1., omp_get_max_threads());
                        context.set_threads(nthreads);
                        }, R"pbdoc(
                        Set the number of computing CPU threads.
                        
                        The function set the number of CPU threads. A higher number of threads corresponds to a faster computation. However, this holds only up to a number of threads equal to the number of CPU cores.
       
                        Args:
                            threads (int): Number of threads
                            
                        )pbdoc", py::arg("threads"));
    
    sm.def("get_cpu_threads", [](){
                    return context.get_threads();
                    },R"pbdoc(
                        Get the maximum number of available CPU cores and the currently selected one.
    
                        Returns:
                            maximum number of available CPU cores and the amount of threads that the module is currently using.
                        
                        )pbdoc");  
    
    
    
    sm.def("set_gpu_ids", [](std::variant<int ,std::vector<int>> gpuid){
                        
                        auto* gpuid_ptr  = std::get_if<int>(&gpuid);
                        if(gpuid_ptr!=nullptr){
                            CheckRange<LeftClosed, RightNone,int>(*gpuid_ptr, 0, 1);
                            context.set_gpu(*gpuid_ptr);
                        }
                        else context.set_gpu(std::get<std::vector<int>>(gpuid));

                        }, R"pbdoc(
                            Set the GPUs to use for the computation. More than one GPU can be selected, by providing a list of IDs.
                            
                            Note:
                                GPU IDs are integers that start from 0.
       
                            Args:
                                id: GPU ID(s).
                            
                            )pbdoc", py::arg("id"));    
    
    sm.def("get_gpu_ids", [](){
                        return context.get_gpu();
                        },R"pbdoc(
                            Get the IDs of available and selected GPUs.
                            
                            Note:
                                GPU IDs are integers that start from 0.
       
                            Returns:
                                A list of available GPU IDs and a list of selected GPU IDs.
                            
                            )pbdoc");      
    
    
    sm.def("use_gpu", [](){ return context.use_gpu();},
                        R"pbdoc(
                        Move the computation to the GPU. 
                        
                        Note:
                            In case that no GPUs are present on your system, are not detected or the module wasn't compiled with GPU support, a warning meassge is diplayed and the function has no effect (the computation is performed on the CPU).
                            
                        )pbdoc"
                        );  
    sm.def("use_cpu", [](){ return context.use_cpu();},
                        R"pbdoc(
                        Move the computation to the CPU. 
                        
                        Note:
                            It is the default module behavior when the module is imported. 
                            
                        )pbdoc");  
    sm.def("set_photon_density", [](FLOAT_TYPE photon_density){ 
                            CheckRange<LeftOpen, RightNone,FLOAT_TYPE>(photon_density, 0, 1, "photon_density");
                            context.set_photon_density(photon_density);}, 
                        R"pbdoc(
                        Set the peak photon density. For further information, see :ref:`here<photon_density_description>`.
                               
                        Args:
                            photon_density (float): photon density.
                            
//                         )pbdoc", py::arg("photon_density"));     
    sm.def("set_wavelength", [](FLOAT_TYPE lambda){ 
                                 CheckRange<LeftOpen, RightNone,FLOAT_TYPE>(lambda, 0, 1, "wavelength");    
                                context.set_lambda(lambda);}, 
                        R"pbdoc(
                        Set the wavelength of the experiment
                               
                        Args:
                            wavelength (float): radiation wavelength.
                            
                        )pbdoc", py::arg("wavelength"));  

    sm.def("set_angle", [](FLOAT_TYPE theta){ 
                                 CheckRange<LeftOpen, RightClosed,FLOAT_TYPE>(theta, 0, 60, "angle");    
                                context.set_angle(theta);}, 
                        R"pbdoc(
                        Set the maximum scattering angle :math:`\theta_\text{max}`.
                               
                        Args:
                            angle (float): Maximum scattering angle, in degrees.
                            
                        )pbdoc", py::arg("angle"));   
    
    sm.def("set_resolution", [](int resolution){ 
                            CheckRange<LeftOpen, RightClosed,int>(resolution, 0, 4096, "resolution"); 
                            context.set_resolution(resolution);}, 
                        R"pbdoc(
                        Set the resolution of the diffraction pattern, in pixel.
                        
                        Note:
                            For the current implementation, only square patterns are available. This means that the parameter provided as input defines both the size in the two dimensions.
                               
                        Args:
                            resolution (int): linear resolution of the detector, in pixels. 
                            
                        )pbdoc", py::arg("resolution")); 
    
    sm.def("set_oversampling", [](int os){ 
                        CheckRange<LeftClosed, RightNone,int>(os, 1., 10., "os");
                        context.set_oversampling(os);}, 
                        R"pbdoc(
                        Set the rendering oversapling.
                        
                        When a shape is rendered on a 3D grid in the real space, before performing the MSFT, the value for each voxel is assigned by sampling the shape in multiple points. For example, setting the value to 3 implies that a total of :math:`3 \times 3 \times 3 = 27` sample values are taken, within the voxel size, to assign its final value. Higher values mitigate the discretization artifacts, but increase the rendering computational cost.
                               
                        Args:
                            os (int): oversampling factor for each axis
                            
                        )pbdoc", py::arg("os"));  

    sm.def("set_fourier_oversampling", [](FLOAT_TYPE os){ 
                            CheckRange<LeftClosed, RightNone,FLOAT_TYPE>(os, 1., 10., "os");
                            context.set_fourier_oversampling(os);}, 
                        R"pbdoc(
                        Set the oversampling factor in Fourier space.
                        
                        For a given pattern resolution :math:`R_\text{pattern}` (for a total number of pixels :math:`R_\text{pattern}^2`), the actual resolution at which the MSFT is computed is defined by :math:`R_\text{MSFT} = \texttt{os} \cdot R_\text{pattern}` (for a total number of pixels :math:`R_\text{MSFT}^2= \texttt{os}^2 \cdot R_\text{pattern}^2`). The final pattern is then cutted at the required resolution.

                        This feature is necessary to mitigate the Discrete Fourier Transform aritfacts at the boundaries. However, it comes at a computational cost. Values between 1.5 and 2 seem to be a good compromise between performance and artifact avoidance. The computational weight of the MSFT scales as :math:`\texttt{os}^2`.
                        
                        Args:
                            os (float): Oversampling factor in Fourier space. 
                        
                        )pbdoc", py::arg("os"));   
    
    sm.def("set_experiment", [](FLOAT_TYPE lambda, FLOAT_TYPE angle, int resolution, FLOAT_TYPE photon_density, int rescale){ 
                CheckRange<LeftOpen, RightNone,FLOAT_TYPE>(lambda, 0, 1, "wavelength");    
                CheckRange<LeftOpen, RightClosed,FLOAT_TYPE>(angle, 0, 60, "angle");    
                CheckRange<LeftOpen, RightClosed,int>(resolution, 0, 4096, "resolution");   
                CheckRange<LeftOpen, RightNone,FLOAT_TYPE>(lambda, 0, 1, "photon_density");


                context.set_photon_density(photon_density);
                context.set_lambda(lambda);
                context.set_angle(angle);
                context.set_resolution(resolution, rescale);
                context.set_values();
                }, 
                        R"pbdoc(
                        Set the parameters relevant for the simulation of the experiment.
                        
                        This function enables to set in just one function call the three relevant parameters that define the experiment
                        
                        Args:
                            wavelength (float): radiation wavelength.
                            angle (float): Maximum scattering angle, in degrees.
                            resolution (int): linear resolution of the detector, in pixels. 
                            photon_density (float): density of photons. For further information, see :ref:`here<photon_density_description>`.
                        
                        )pbdoc",py::arg("wavelength"), py::arg("angle") ,py::arg("resolution"), py::arg("photon_density")=1.e6, py::arg("rescale")=-1);

    sm.def("init", [](){ context.init();
        // run dummy experiment
//         Shapes::Ellipsoid* dummyshape =new Shapes::Ellipsoid(context.lambda*2,context.lambda*2,context.lambda*2,1e-3 , 1.e-3, 0,0,0);
//         Detectors::MSFT dummydetector; 
//         auto dummypattern = dummydetector.acquire(std::vector<Shapes::Shape*>({dummyshape}));
//         delete dummyshape;
    }, 
                        R"pbdoc(
                        Initialize the computational environment.
                        
                        The function allocates memory and makes the hardware ready to compute the MSFT, depending on the current hardware configuration.
                        
                        Note:
                            An explicit call to this function is not mandatory. Each time that the hardware configuration is changed (GPUs, CPU threads, etc.), the function is automatically called before computing the first MSFT, if it wasn't explicitly called before. 
                            
                        Warning:                            
                            If the function is not called explicitly after having changed the hardware configuration, it is thus executed automatically before computing the first MSFT, introducing a slowdown in the first computation. When the code performance is tested, the function must be called explicitly before starting the timer, otherwise inconsistent timing results may happen.
                        
                        )pbdoc");  
    sm.def("info", [](){ context.info();}, 
                        R"pbdoc(
                        Print all the information about the current setup.
                        
                        The provided information concerns both the current hardware configuration and the experimental parameters.                        
                        )pbdoc"); 
    sm.def("set_verbose", [](bool verbosity){ context.set_verbose(verbosity);}, 
                        R"pbdoc(
                        Set the verbosity level
                        
                        This function enables to set verbosity level of the module. If set to 1 (True), information about what is going on during the function calls is displayed. If 0 (False), the functions execute silently.
                        
                        Args:
                            verbosity (bool): Set verbosity.                            
                        
                        )pbdoc"
                        , py::arg("verbosity")=1);
    
    // // // // // // // // // // // // // // // // // // // // // //     
    
    
    
    
    
    py::class_<Pattern>(sm, "Pattern", R"pbdoc(
                        Object that conains a diffraction pattern. 
                        
                        This object doesn't have to be directly defined by the user, and it is only returned by the :code:`acquire` and :code:`acquire_dataset` methods of the detectors.
                        The pattern is bit-compatible with a 2D numpy array, and can be re-interpreted as the latter by an explicit conversion, i.e.:
                        
                        .. code::
                        
                            import numpy as np
                            ...
                            
                            my_numpy_pattern = np.array(my_pattern)
    
                            
                        .. note::
                            The pattern object is actually needed only for data management within the module. It is very likely that in future releases it will be removed, by allowing the :code:`acquire` and :code:`acquire_dataset` methods to directly return a 2D numpy array.
                        
                        )pbdoc", py::buffer_protocol())
    
//             .def(py::init<unsigned int, FLOAT_TYPE>(), py::arg("resolution") = 512 , py::arg("angle") = 60. )
            .def_readonly("resolution", &Pattern::npixel,  R"pbdoc(
                        Linear resolution, in pixel, of the diffraction pattern.
                        )pbdoc")      
            .def_readonly("coordinates", &Pattern::coordinates,  R"pbdoc(
                        Coordinates representation type of the diffraction pattern ( :code:`'projection'`, :code:`'momentum'` , :code:`'angle'` or :code:`'detector'` ).
                        )pbdoc")   
            .def_readonly("pixel_size", &Pattern::pixel_size,  R"pbdoc(
                        Pixel size of the diffraction pattern. Depening on the coordinates type, it can assume different units:
                        
                        - If :code:`coordinates = 'projection'` or :code:`coordinates = 'momentum'`, the unit is the reciprocal of a length.
                        - If :code:`coordinates = 'angle'`, the unit is degrees.
                        - If :code:`coordinates = 'detector'`, the unit is the ratio between the detector's physical pixel size and the detector distance
                        
                        )pbdoc")  
            .def("get_field", [](Pattern &pp){
                
                        auto v = pp.field;
                        auto nparray = py::array(v.size(), v.data());
                        nparray.resize({pp.npixel, pp.npixel});
                        return nparray;
                        }, 
                        R"pbdoc(
                        Get the complex scattered field.
                        
                        .. note::
                            If the option "field" of the detector's acquire method is set to false (default), this function will return just an zero size array.
                        
                        Returns:
                            A 2D numpy complex array containing the complex scattere field. 
                        
                        )pbdoc") 
            .def_buffer([](Pattern &pp) -> py::buffer_info {
                    return py::buffer_info(
                        &pp.data[0],                               /* Pointer to buffer */
                        sizeof(FLOAT_TYPE),                          /* Size of one scalar */
                        py::format_descriptor<FLOAT_TYPE>::format(), /* Python struct-style format descriptor */
                        2,                                      /* Number of dimensions */
                        { pp.npixel, pp.npixel },                 /* Buffer dimensions */
                        { sizeof(FLOAT_TYPE) * pp.npixel ,             /* Strides (in bytes) for each index */
                        sizeof(FLOAT_TYPE)}
                        );
                    })
            .def("get_error", [](Pattern &pp, py::array_t<FLOAT_TYPE> error_pattern, py::array_t<int> error_map,  FLOAT_TYPE error_norm, FLOAT_TYPE experiment_sum, FLOAT_TYPE scaling, FLOAT_TYPE offset, FLOAT_TYPE exponent){
                        auto expbuf = error_pattern.request();
                        CheckEqual<int>(expbuf.shape.size(), 2, "The number of dimensions of the experimental pattern");
                        CheckEqual<int>(expbuf.shape[0],expbuf.shape[1], "First dimension and second dimension of experimental pattern must be equal. The first dimension");
                        CheckEqual<int>(expbuf.shape[0],pp.npixel, "Number of pixels in the experimental pattern");                        
                
                        auto errmapbuf = error_map.request();
                        CheckEqual<int>(errmapbuf.shape.size(), 2, "The number of dimensions of the experimental pattern");
                        CheckEqual<int>(errmapbuf.shape[0],errmapbuf.shape[1], "First dimension and second dimension of experimental pattern must be equal. The first dimension");
                        CheckEqual<int>(errmapbuf.shape[0],pp.npixel, "Number of pixels in the experimental pattern");                        
                        
                        return pp.get_error((FLOAT_TYPE*) expbuf.ptr, (int*) errmapbuf.ptr,error_norm,  experiment_sum,  scaling, offset, exponent);
                        
                        }, py::arg("error_pattern"), py::arg("error_map"), py::arg("error_norm"), py::arg("experiment_sum"), py::arg("scaling"), py::arg("offset"), py::arg("exponent")) 

            
    ;    
    /////////////////////////////
 ///////////////////////////////

    py::module sh = sm.def_submodule("Shapes");
    
    py::class_<Shapes::Shape, PyShape<>>(sh, "Shape")
//             .def("get", &Shapes::Shape::get, py::arg("resolution")=80., py::arg("oversampling")=1.);   
            .def("get", [](Shapes::Shape &shape, unsigned int resolution, int os){
                
                        CheckRange<LeftOpen, RightNone,int>(resolution, 0, 1, "resolution");
                        CheckRange<LeftOpen, RightNone,int>(os, 1, 2, "os");

                        FLOAT_TYPE dx=0;
                         auto v = shape.get(resolution, dx, os);
                        auto nparray = py::array(v.size(), v.data());
                        nparray.resize({resolution, resolution, resolution});
                        return std::make_tuple(nparray, dx);
                        }, 
                        R"pbdoc(
                        Get a voxel rendering of the shape.
                        
                        Get a 3D rendering of the shape. 
                                                
                        Args:
                            resolution: number of voxels in one dimension. The current implementation draws the shape inside a 3D cube with a total number of voxels equal to :math:`\text{resolution}^3`.
                            oversampling:   oversampling factor for each axis.                             
                                            When a shape is rendered on the 3D grid, the value for each voxel is assigned by sampling the shape in multiple points. For example, setting the value to 3 implies that a total of :math:`3 \times 3 \times 3 = 27` sample values are taken, within the voxel size, to assign its final value. Higher values mitigate the discretization artifacts, but increase the rendering computational cost.
                            
                        Returns:
                            A tuple containing a 3D numpy array containing the rendered shape, and float that provides the linear dimension of the voxels.
                        
                        )pbdoc", py::return_value_policy::move,  py::arg("resolution")=80, py::arg("os")=3) 
            .def("get_name", [](Shapes::Shape &shape){
                                return shape.get_name();})
            .def("get_size", &Shapes::Shape::get_size)
            .def("set_fitting_properties", &Shapes::Shape::set_fitting_properties,  py::arg("scaling"),  py::arg("offset")); 

    py::class_<Shapes::Ellipsoid, Shapes::Shape, PyEllipsoid<>>(sh, "Ellipsoid")
            .def(py::init<FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE,  FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE >(), 
                        R"pbdoc(
                        Define an Ellipsoid via the values of its three axes.
                                                
                        Args:
                            a: Main axis of the ellipsoid.
                            b,c: the other two axes. If negative, they are set equal to a, giving a spherical shape
                            delta, beta: see :ref:`here<optical_description>`
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("a"),py::arg("b")=-1,py::arg("c")=-1,
                    py::arg("delta") = 0, py::arg("beta") = 1.e-3,
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0,py::arg("xshift") = 0,py::arg("yshift") = 0);
    
    py::class_<Shapes::Dumbbell, Shapes::Shape, PyDumbbell<>>(sh, "Dumbbell")
            .def(py::init<FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE,  FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE >(),
                        R"pbdoc(
                        Define a dumbbell shape via the parameters described in the picture.
                        
                        .. image:: figures/dumbbell.png
                            :width: 50% 
                            :align: center
                            
                        Args:
                            l: Main axis of the dumbbell
                            a,b,c,z: the other relevant lengths
                            delta, beta: see :ref:`here<optical_description>`
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.                
            )pbdoc",
                    py::arg("l"), py::arg("a"),py::arg("b"),py::arg("c"), py::arg("z"),
                    py::arg("delta") = 0, py::arg("beta") = 1.e-3,
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0,py::arg("xshift") = 0,py::arg("yshift") = 0);   

    py::class_<Shapes::ShellSphere, Shapes::Shape, PyShellSphere<>>(sh, "ShellSphere")
            .def(py::init<FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE>(),
                        R"pbdoc(
                        Define a sphere with optical properties distributed in a core-shell manner. Due to the simmetry of the sphere, the orientation parameters have no effect and are thus not available for this shape.
                        
                        .. image:: figures/shell_sphere.png
                            :width: 50% 
                            :align: center
                            
                        Args:
                            r: sphere radius
                            s: thickness of the shell
                            t: width of the transition between the shell and the core
                            delta, beta, delta_shell, beta_shell: see :ref:`here<optical_description>`
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("r"), py::arg("s"), py::arg("t")=0,
                    py::arg("delta")=0,py::arg("beta")=1.e-2, py::arg("delta_shell")=0.01, py::arg("beta_shell")=1.e-3,py::arg("beam_position") = 0,py::arg("xshift") = 0,py::arg("yshift") = 0);   
                        
    py::class_<Shapes::MoonSphere, Shapes::Shape, PyMoonSphere<>>(sh, "MoonSphere")
            .def(py::init<FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE>(),
                        R"pbdoc(
                        Define a sphere with optical properties distributed in a core-shell manner, where the shell has a *falling moon* shape oriented in the direction of the incoming beam. Due to the simmetry of the sphere, the orientation parameters have no effect and are thus not available for this shape.
                        
                        .. image:: figures/moon_sphere.png
                            :width: 50% 
                            :align: center              
                            
                        Args:
                            r: sphere radius
                            s: thickness of the shell
                            t: width of the transition between the shell and the core
                            delta, beta, delta_shell, beta_shell: see :ref:`here<optical_description>`
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("r"), py::arg("s"), py::arg("t")=0,
                    py::arg("delta")=0,py::arg("beta")=1.e-2, py::arg("delta_shell")=0.01, py::arg("beta_shell")=1.e-3,py::arg("beam_position") = 0,py::arg("xshift") = 0,py::arg("yshift") = 0);    
    
        
    
    py::class_<Shapes::RadialMap, Shapes::Shape, PyRadialMap<>>(sh, "RadialMap")
            .def(py::init([]( py::array_t<FLOAT_TYPE> radii_, FLOAT_TYPE a_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_,
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_, FLOAT_TYPE xshift_, FLOAT_TYPE yshift_) {
                auto buf = radii_.request();
                return new Shapes::RadialMap(a_, (FLOAT_TYPE*) buf.ptr, buf.shape[0], buf.shape[1], delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_); }), 
                        R"pbdoc(
                        Define a RadialMap. 
                                                
                        Args:
                            radii: 2D array providing the radii as function of theta and phi.
                            a: scaling factor: if a>0 the shape will be scaled such that the maximum radius has a value equal to a.
                            delta, beta: see :ref:`here<optical_description>`
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("radii"), py::arg("a")=-1,
                    py::arg("delta") = 0, py::arg("beta") = 1.e-3,
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0,py::arg("xshift") = 0,py::arg("yshift") = 0);
            
    py::class_<Shapes::SphericalHarmonics, Shapes::RadialMap, PySphericalHarmonics<>>(sh, "SphericalHarmonics")
        .def(py::init([]( py::array_t<FLOAT_TYPE> coefficients_list_, FLOAT_TYPE a_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_,
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_, FLOAT_TYPE xshift_, FLOAT_TYPE yshift_) {
            
                FLOAT_TYPE* coefficients;
                int size;
                auto buf = coefficients_list_.request();
                
                if(buf.shape.size()==2){
                    FLOAT_TYPE* coefficients_list_ptr = (FLOAT_TYPE*) buf.ptr;
                    int nmax=0;
                    for(int icoeff=0; icoeff+2<buf.shape[0]*buf.shape[1]; icoeff+=3){
                        int l = coefficients_list_ptr[icoeff];
                        int m = coefficients_list_ptr[icoeff+1];
                        int n = m +  l*l + l;
                        if(n>nmax) nmax=n;
                    }
                    size= nmax+1;
                    
                    coefficients = new FLOAT_TYPE[size];
                    for(int i=0; i<size;i++) coefficients[i]=0;

                    for(int icoeff=0; icoeff+2<buf.shape[0]*buf.shape[1]; icoeff+=3){
                        int l = coefficients_list_ptr[icoeff];
                        int m = coefficients_list_ptr[icoeff+1];
                        int n = m +  l*l + l;
                        FLOAT_TYPE c = coefficients_list_ptr[icoeff+2];
                        coefficients[n]+=c;
                    }
                }
                else if(buf.shape.size()==1){
                    coefficients = (FLOAT_TYPE*) buf.ptr;
                    size = buf.shape[0];
                }
                
                return new Shapes::SphericalHarmonics(a_, coefficients, size, delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_); }), 
                        R"pbdoc(
                        Define a shape basing on the spherical harmonics coefficients. See the documentation for a deeper description. 
                        
                                                
                        Args:
                            coefficients: coefficients of the spherical harmonics, organized in a list of triplets [l,m,val]
                            a: scaling factor: if a>0 the shape will be scaled such that the maximum radius has a value equal to a.
                            delta, beta: see :ref:`here<optical_description>`
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("coefficients"),py::arg("a")=-1,
                    py::arg("delta") = 0, py::arg("beta") = 1.e-3,
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0,py::arg("xshift") = 0,py::arg("yshift") = 0);
            
        
            
    
    py::class_<Shapes::VolumeMap, Shapes::Shape, PyVolumeMap<>>(sh, "VolumeMap")            
            .def(py::init([](FLOAT_TYPE dx_, py::array_t<bool> data_,
                             FLOAT_TYPE delta_, FLOAT_TYPE beta_,
                            FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_, FLOAT_TYPE xshift_, FLOAT_TYPE yshift_) {
                auto buf = data_.request();
                CheckEqual<int>(buf.shape.size(), 3, "The number of dimensions of array data");
                CheckRange<LeftOpen, RightClosed,std::size_t>( buf.shape[2]*buf.shape[1]*buf.shape[0], 0, 256*256*256, "The product of (x, y, z) dimensions of array data, now " + std::to_string(buf.shape[2])+"*"+ std::to_string(buf.shape[1])+"*"+ std::to_string(buf.shape[0])+"="+ std::to_string(buf.shape[2]*buf.shape[1]*buf.shape[0])+",");    
                FLOAT_TYPE dz=dx_;
//                 if(dz_<=0) dz = dx_;

//                 printf("xdim %d, ydim %d, zdim %d\n", buf.shape[2], buf.shape[1], buf.shape[0]);
                return new Shapes::VolumeMap(dx_,dz,  (bool*) buf.ptr, buf.shape[2], buf.shape[1], buf.shape[0], delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_); }), 
                        R"pbdoc(
                        Define a custom sample, by directly providing its 3D rendering. Here, the rendering is provided as a 3D boolean array, where True indicates the presence of the material, while False is vacuum. The 3D array provided as input can be of any size, and it is automatically rescaled to match the experimental conditions via interpolation.
                                                
                        .. note:
                            The use of this object implies a relatively large performance penalty, which is directly proportional to the size of the data provided as input, and becomes a bottleneck when running on the GPU.
                            
                        Args:
                            dx: linear size of the voxels
                            data: 3D bool array that contains the 3D distribution of the sample's optical properties.
                            delta, beta: see :ref:`here<optical_description>`
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("dx"),py::arg("data"), 
                    py::arg("delta") = 0, py::arg("beta") = 1.e-3,                 
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0,py::arg("xshift") = 0,py::arg("yshift") = 0);
            
            
        
    py::class_<Shapes::VolumeDistribution, Shapes::Shape, PyVolumeDistribution<>>(sh, "VolumeDistribution")            
            .def(py::init([](FLOAT_TYPE dx_, py::array_t<std::complex<FLOAT_TYPE>> data_, 
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_, FLOAT_TYPE xshift_, FLOAT_TYPE yshift_) {
                auto buf = data_.request();
                CheckEqual<int>(buf.shape.size(), 3, "The number of dimensions of array data");
                CheckRange<LeftOpen, RightClosed,std::size_t>( buf.shape[2]*buf.shape[1]*buf.shape[0], 0, 256*256*256, "The product of (x, y, z) dimensions of array data, now " + std::to_string(buf.shape[2])+"*"+ std::to_string(buf.shape[1])+"*"+ std::to_string(buf.shape[0])+"="+ std::to_string(buf.shape[2]*buf.shape[1]*buf.shape[0])+",");    

//                 printf("xdim %d, ydim %d, zdim %d\n", buf.shape[2], buf.shape[1], buf.shape[0]);
                return new Shapes::VolumeDistribution(dx_, (COMPLEX_TYPE*) buf.ptr, buf.shape[2], buf.shape[1], buf.shape[0], latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_); }), 
                        R"pbdoc(
                        Define a custom sample, by directly providing its 3D complex refractive index distribution by means of a 3D complex-valued array. The 3D array provided as input can be of any size, and it is automatically rescaled to match the experimental conditions via interpolation. Values equal to 1 correspond to the optical properties of vacuum.
                                                
                        .. note:
                            The use of this object implies a relatively large performance penalty, which is directly proportional to the size of the data provided as input, and becomes a bottleneck when running on the GPU.
                            
                        Args:
                            dx: linear size of the voxels
                            data: 3D complex array that contains the 3D distribution of the sample's optical properties.
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("dx"),py::arg("data"),
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0,py::arg("xshift") = 0,py::arg("yshift") = 0);
            
            
        
    py::class_<Shapes::CubeMap, Shapes::Shape, PyCubeMap<>>(sh, "CubeMap")
            .def(py::init([]( py::array_t<FLOAT_TYPE> radii_, FLOAT_TYPE a_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_, 
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_, bool cubic_interpolation_, bool tan_adjust_, FLOAT_TYPE xshift_, FLOAT_TYPE yshift_) {
                auto buf = radii_.request();
                CheckEqual<int>(buf.shape.size(), 3, "The number of dimensions of array radii");
                CheckEqual<int>(buf.shape[0], 6, "First dimension of 3D array");
                
                if(buf.shape[1]!=buf.shape[2])
                        throw std::invalid_argument("Second and third dimensions of the radii array must be equal");

                return new Shapes::CubeMap(a_, (FLOAT_TYPE*) buf.ptr, buf.shape[2],delta_, beta_, 1, latitude_, longitude_, orientation_, beam_position_, cubic_interpolation_,tan_adjust_, xshift_, yshift_); }), 
                        R"pbdoc(
                        Define a sample's shape using a Cube Mapping of its radius. 
                                                
                        Args:
                            radii: 3D array providing the radii. The first dimension of the array must be equal to 6, and represents the 6 faces of the cube. Second and third dimesions should be equal, which means that the faces of the cube must be square matrices. 
                            a: scaling factor: if a>0 the shape will be scaled such that the maximum radius has a value equal to a.
                            delta, beta: see :ref:`here<optical_description>`
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("radii"), py::arg("a")=-1,
                    py::arg("delta") = 0, py::arg("beta") = 1.e-3,
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0,py::arg("cubic_interpolation") = 1,py::arg("tangent_correction") = 0,py::arg("xshift") = 0,py::arg("yshift") = 0)
            
            .def("get_radii", [](Shapes::CubeMap &cm){
                        cm.init();
                        std::vector<FLOAT_TYPE> v(6*cm.sidesize*cm.sidesize);
                        for(int i=0; i<v.size(); i++) v[i]= cm.radii[i];
                 
                        auto nparray = py::array(v.size(), v.data());
                        nparray.resize({6, cm.sidesize, cm.sidesize});
                        return nparray;
                        });
            
    py::class_<Shapes::CubeMesh, Shapes::Shape, PyCubeMesh<>>(sh, "CubeMesh")
            .def(py::init([]( py::array_t<FLOAT_TYPE> radii_, FLOAT_TYPE a_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_, 
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_, FLOAT_TYPE xshift_, FLOAT_TYPE yshift_) {
                auto buf = radii_.request();
                CheckEqual<int>(buf.shape.size(), 3, "The number of dimensions of array radii");
                CheckEqual<int>(buf.shape[0], 6, "First dimension of 3D array");
                
                if(buf.shape[1]!=buf.shape[2])
                        throw std::invalid_argument("Second and third dimensions of the radii array must be equal");

                return new Shapes::CubeMesh(a_, (FLOAT_TYPE*) buf.ptr, buf.shape[2],delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_); }), 
                        R"pbdoc(
                        Define a sample's shape using a Cube Mapping of its radius. 
                                                
                        Args:
                            radii: 3D array providing the radii. The first dimension of the array must be equal to 6, and represents the 6 faces of the cube. Second and third dimesions should be equal, which means that the faces of the cube must be square matrices. 
                            a: scaling factor: if a>0 the shape will be scaled such that the maximum radius has a value equal to a.
                            delta, beta: see :ref:`here<optical_description>`
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("radii"), py::arg("a")=-1,
                    py::arg("delta") = 0, py::arg("beta") = 1.e-3,
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0, py::arg("xshift") = 0,py::arg("yshift") = 0)
            
            .def("get_radii", [](Shapes::CubeMesh &cm){
                        cm.init();
                        std::vector<FLOAT_TYPE> v(6*cm.sidesize*cm.sidesize);
                        for(int i=0; i<v.size(); i++) v[i]= cm.radii[i];
                 
                        auto nparray = py::array(v.size(), v.data());
                        nparray.resize({6, cm.sidesize, cm.sidesize});
                        return nparray;
                        });            
     
        
    py::class_<Shapes::CubeFaces, Shapes::Shape, PyCubeFaces<>>(sh, "CubeFaces")
            .def(py::init([]( py::array_t<FLOAT_TYPE> radii_, FLOAT_TYPE a_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_, 
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_, FLOAT_TYPE map_resolution_, FLOAT_TYPE xshift_, FLOAT_TYPE yshift_) {
                auto buf = radii_.request();
                CheckEqual<int>(buf.shape.size(), 3, "The number of dimensions of array radii");
                CheckEqual<int>(buf.shape[0], 6, "First dimension of 3D array");
                
                if(buf.shape[1]!=buf.shape[2])
                        throw std::invalid_argument("Second and third dimensions of the radii array must be equal");

                return new Shapes::CubeFaces(a_, (FLOAT_TYPE*) buf.ptr, buf.shape[2],delta_, beta_, latitude_, longitude_, orientation_, beam_position_, map_resolution_, xshift_, yshift_); }), 
                        R"pbdoc(
                        Define a sample's shape using a Cube Mapping of its radius. 
                                                
                        Args:
                            radii: 3D array providing the radii. The first dimension of the array must be equal to 6, and represents the 6 faces of the cube. Second and third dimesions should be equal, which means that the faces of the cube must be square matrices. 
                            a: scaling factor: if a>0 the shape will be scaled such that the maximum radius has a value equal to a.
                            delta, beta: see :ref:`here<optical_description>`
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("radii"), py::arg("a")=-1,
                    py::arg("delta") = 0, py::arg("beta") = 1.e-3,
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0,py::arg("map_resolution") = 30,py::arg("xshift") = 0,py::arg("yshift") = 0)
            
            .def("get_radii", [](Shapes::CubeFaces &cm){
                        cm.init();
                        std::vector<FLOAT_TYPE> v(6*cm.sidesize*cm.sidesize);
                        for(int i=0; i<v.size(); i++) v[i]= cm.radii[i];
                 
                        auto nparray = py::array(v.size(), v.data());
                        nparray.resize({6, cm.sidesize, cm.sidesize});
                        return nparray;
                        });
            
        
    py::class_<Shapes::Faces, Shapes::Shape, PyFaces<>>(sh, "Faces")
            .def(py::init([]( py::array_t<FLOAT_TYPE> points_, 
              FLOAT_TYPE delta_, FLOAT_TYPE beta_, 
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_, FLOAT_TYPE max_radius_, FLOAT_TYPE central_symmetry_, FLOAT_TYPE xshift_, FLOAT_TYPE yshift_) {
                auto buf = points_.request();
                CheckEqual<int>(buf.shape.size(), 2, "The number of dimensions of array points");
                CheckEqual<int>(buf.shape[1], 3, "First dimension of 3D array");
/*                
                if(buf.shape[1]!=buf.shape[2])
                        throw std::invalid_argument("Second and third dimensions of the points array must be equal");*/

                return new Shapes::Faces((FLOAT_TYPE*) buf.ptr, buf.shape[0],delta_, beta_, latitude_, longitude_, orientation_, beam_position_, max_radius_, (int) central_symmetry_, xshift_, yshift_); }), 
                        R"pbdoc(
                        Define a sample's shape using a Cube Mapping of its radius. 
                                                
                        Args:
                            points: to do
                            a: scaling factor: if a>0 the shape will be scaled such that the maximum radius has a value equal to a.
                            delta, beta: see :ref:`here<optical_description>`
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("points"), 
                    py::arg("delta") = 0, py::arg("beta") = 1.e-3,
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0,py::arg("max_radius") = -1,py::arg("symmetry") = 0, py::arg("xshift") = 0,py::arg("yshift") = 0)
            
            .def(py::init([]( py::array_t<FLOAT_TYPE> points_, py::array_t<int> active_, 
              FLOAT_TYPE delta_, FLOAT_TYPE beta_, 
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_, FLOAT_TYPE max_radius_, FLOAT_TYPE xshift_, FLOAT_TYPE yshift_) {
                auto buf = points_.request();
                CheckEqual<int>(buf.shape.size(), 2, "The number of dimensions of array points");
                CheckEqual<int>(buf.shape[1], 3, "First dimension of 3D array");
                auto buf_active = active_.request();
                CheckEqual<int>(buf.shape[0], buf_active.shape[0], "Points array first dimension");
                
                FLOAT_TYPE* points = (FLOAT_TYPE*) buf.ptr;
                int* active = (int*) buf_active.ptr;
                                
                int nactive=0;
                for(int i=0; i<buf_active.shape[0]; i++)
                    if(active[i]>0)
                        nactive++;
                    
                std::vector<FLOAT_TYPE> filtered_points(nactive*3);
                
                int counter=0;
                for(int i=0; i<buf_active.shape[0]; i++){
                    if(active[i]>0){
                        filtered_points[3*counter]= points[3*i];                
                        filtered_points[3*counter+1]= points[3*i+1];                
                        filtered_points[3*counter+2]= points[3*i+2];                
                        counter++;
                    }
                }

                return new Shapes::Faces(&filtered_points[0], nactive,delta_, beta_, latitude_, longitude_, orientation_, beam_position_, max_radius_, xshift_, yshift_); }), 
                        R"pbdoc(
                        Define a sample's shape using a Cube Mapping of its radius. 
                                                
                        Args:
                            points: to do
                            active: to do
                            a: scaling factor: if a>0 the shape will be scaled such that the maximum radius has a value equal to a.
                            delta, beta: see :ref:`here<optical_description>`
                            latitude, longitude, rotation: see :ref:`here<orientation_description>`.
                            beam_position: see :ref:`here<beam_position_description>`.
                        )pbdoc",
                    py::arg("points"), py::arg("active"), 
                    py::arg("delta") = 0, py::arg("beta") = 1.e-3,
                    py::arg("latitude") = 90, py::arg("longitude") = 0,py::arg("rotation") = 0,py::arg("beam_position") = 0,py::arg("max_radius") = -1,py::arg("xshift") = 0,py::arg("yshift") = 0)
            
            ;
            /////////////////////////////     
            
    /////////////////////////////
    /////////////////////////////
    /////////////////////////////
    /////////////////////////////
    /////////////////////////////

    py::module de = sm.def_submodule("Detectors", R"pbdoc(
                        Submodule that provides different types of detectors.                         
                        
                        )pbdoc" );
    py::class_<Detectors::Detector, PyDetector<>>(de, "Detector", 
                        R"pbdoc(
                        Base class for the detector                           
                        
                        )pbdoc"
                        )
            
            .def("acquire", [](Detectors::Detector& detector, std::variant<Shapes::Shape*,std::vector<Shapes::Shape*>> shape, bool save_field)->std::variant<Pattern,std::vector<Pattern>>{

                auto* shape_ptr  = std::get_if<Shapes::Shape*>(&shape);
                if(shape_ptr!=nullptr) return std::move(detector.acquire(*shape_ptr, save_field));
                else return std::move(detector.acquire(std::get<std::vector<Shapes::Shape*>>(shape), save_field));
//             return std::visit([&detector](auto shape){return detector.acquire(shape);}, shape);
//             return detector.acquire(shape);

                
            }, 
                        R"pbdoc(
                        Simulate the experiment on a given shape or list of shapes. 
                        
                        The function performs the MSFT on the shapes, applying the properties of the detector.
                        
                        Args:
                            shape: the input sample on which the MSFT is performed. Can be a single shape, or a list of shapes.
                            field: If true, the complex scattered wavefield is also saved, and can be accessed by calling the :code:`get_field()` method of the returned Pattern objects.
                            
                        Returns:
                            The diffraction pattern, or a list of diffraction patterns, corresponding to the simulated wide-angle scattering image for each shape provided as input.
                        
                        )pbdoc",
             py::arg("shape"), py::arg("field")=false) 
            
            .def("acquire_dataset", [](Detectors::Detector& detector, std::vector<Shapes::Shape*> shape_list){
                return detector.acquire(shape_list, 0);
            }, py::arg("shapes"));
/*             
            .def("apply_features", [](Detectors::Detector &det, py::array_t<FLOAT_TYPE> pattern_in, FLOAT_TYPE scattered_photons){
                        auto acc = pattern_in.mutable_unchecked<2>();
                        
                        Pattern pattern(acc.shape(0), 60., scattered_photons);
                        for(int i=0; i<acc.shape(0); i++)
                            for(int j=0; j<acc.shape(1); j++)
                                pattern.set(acc(i,j), i, j);
                        
                        det.apply_features(pattern);
                        return pattern;
                        }, R"pbdoc(
                        Apply the detector features to an already existing diffraction pattern, provided as input
                                                
                        Args:
                            patten: the input diffraction pattern
                            
                        Returns:
                            the diffraction pattern affected by the detector features
                        
                        )pbdoc", py::arg("pattern"), py::arg("scattered_photons")=-1);*/
//             .def("tune", &Detectors::Detector::tune);


    py::class_<Detectors::MSFT, Detectors::Detector, PyDetectorMSFT<>>(de, "MSFT")
            .def(py::init<std::string>(), R"pbdoc(
                        The MSFT detector is a sort of "virtual" detector, in the sense that it directly provides the MSFT numerical result, without adding any kind of additional feature, poisson noise included. 
                        
                        Args:
                            coordinates: ( `'projection'`, `'momentum'`, `'angle'` or `'detector'`) set the coordinates of the pattern.
                        )pbdoc", py::arg("coordinates")="projection")
            
//             
             .def("compare", [](Detectors::MSFT& detector, std::vector<Shapes::Shape*> shapes, py::array_t<FLOAT_TYPE> error_pattern, py::array_t<int> error_map, FLOAT_TYPE error_norm, FLOAT_TYPE experiment_sum, FLOAT_TYPE exponent, FLOAT_TYPE metric)->std::vector<FLOAT_TYPE>{
                auto expbuf = error_pattern.request();
                CheckEqual<int>(expbuf.shape.size(), 2, "The number of dimensions of the experimental pattern");
                CheckEqual<int>(expbuf.shape[0],expbuf.shape[1], "First dimension and second dimension of experimental pattern must be equal. The first dimension");
                CheckEqual<int>(expbuf.shape[0],context.scale_npixel, "Number of pixels in the experimental pattern");                        
        
                auto errmapbuf = error_map.request();
                CheckEqual<int>(errmapbuf.shape.size(), 2, "The number of dimensions of the experimental pattern");
                CheckEqual<int>(errmapbuf.shape[0],errmapbuf.shape[1], "First dimension and second dimension of experimental pattern must be equal. The first dimension");
                CheckEqual<int>(errmapbuf.shape[0],context.scale_npixel, "Number of pixels in the experimental pattern");  
               return std::move(detector.compare(shapes, (FLOAT_TYPE*) expbuf.ptr, (int*) errmapbuf.ptr,  error_norm,experiment_sum,  exponent, metric));

            }, 
             py::arg("shapes"), py::arg("error_pattern"), py::arg("error_map"), py::arg("error_norm"),py::arg("experiment_sum"), py::arg("exponent"), py::arg("metric")=1.) 
            ;
            
            
            
            
            
            
    py::class_<Detectors::Ideal, Detectors::Detector, PyDetectorIdeal<>>(de, "Ideal")
            .def(py::init<unsigned int, std::string>(), R"pbdoc(
                        The Ideal detector is a model for a *perfect* detector. It is a detector with no artifacts, a perfect linear response and a pixel color depth equivalent to the precision of the :code:`float` type (32 or 64 bits).
                                                
                        Args:
                            mask_radius: radius (in pixels) of the beam stopper/hole at the center of the detector
                            coordinates: ( `'projection'`, `'momentum'`, `'angle'` or `'detector'`) set the coordinates of the pattern.
                        
                        )pbdoc", py::arg("mask_radius")=0,py::arg("coordinates")="projection");

    py::class_<Detectors::MCP, Detectors::Detector, PyDetectorMCP<>>(de, "MCP")
            .def(py::init<unsigned int, FLOAT_TYPE,  FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, FLOAT_TYPE, std::string>(),
                 R"pbdoc(
                        Model for a *Micro Channel Plate* detector. This detector provides as output a diffraction pattern affected by the MCP artifacts, like background noise, signal offset, non-linear response and saturation. The relevant parameters for the features simulation are provided as arguments for its construction. The MCP features are applied to the diffraction pattern after having performed the MSFT, by applying function :math:`R` to the *ideal* pattern :math:`I_\text{ideal}`, that is :math:`I_\text{MCP}= R(I_\text{ideal})`.
                        
                        For more info see :ref:`here<MCP_response_description>`.
                                    
                        Warning:
                            This class is still experimental. The following description of the arguments was done months after its implementation, and it is very likely that there are some mistakes. Please, use it carefully and don't pretend meaningful results from it. Work on this class is still ongoing.
                        
                        
                        
                        Args:
                            mask_radius: radius (in pixels) of the beam stopper/hole at the center of the detector
                            gain: gain of the MCP.
                            saturation: saturation value of the MCP.
                            linearity: fraction of the saturation value up to which the MCP response is sub-linear. 
                            exponent: exponent to simulate the non-linear behavior.
                            offset: offset value of the MCP output.
                            coordinates: ( `'projection'`, `'momentum'`, `'angle'` or `'detector'`) set the coordinates of the pattern.
                            
                        )pbdoc", 
                 py::arg("mask_radius")=0,
                 py::arg("gain")=1., 
                 py::arg("saturation")=30000, 
                 py::arg("linearity")=0.7,
                 py::arg("exponent")=0.4,
                 py::arg("offset")=0,
                 py::arg("coordinates")="projection");
//             .def_readwrite("photons_per_channel", &Detectors::MCP::photons_per_channel);
    

    
}
