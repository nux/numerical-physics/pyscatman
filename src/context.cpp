#include "context.h"
#include <omp.h>
#include <stdio.h>
#include <string>
#include "util/tuning.h"

#ifdef WITH_CUDA
    #include "gpu/msft_context.h"
#endif


#define MAX_THREADS_PER_GPU 16


Context::~Context(){
#ifdef WITH_CUDA
        for(MSFT_gpu_context* gpucont : gpu_context_list) delete gpucont;
#endif
        
        
    };

bool Context::use_gpu(){
#ifdef WITH_CUDA
    if(gpu_list.size()>0){
        on_gpu=1;  
        is_init_gpu=0;
        if(verbose) printf("Switching computation on GPU.\n");
        return 1;
    }
    else{
        on_gpu=0;
        printf("WARNING: Cannot switch to GPU. GPU list is empty. Please set the GPU list before.\n");
        return 0;
    }
#else
    printf("WARNING: GPUs not enabled. Operation has no effect\n");
    return 0;
#endif
}

bool Context::use_cpu(){
    on_gpu=0;
    is_init_gpu=0;
    if(verbose) printf("Switching computation on CPU.\n");
    return 1;
}

void Context::set_threads(int nthreads_=0){
        if(nthreads_<=0){
            nthreads=omp_get_max_threads();
        }
        else{
            nthreads=nthreads_;
            if(nthreads>omp_get_max_threads()){
                printf("WARNING: The number of requried threads (%d) exceeds the number of cores (%d). This usually implies performance decay\n", nthreads, omp_get_max_threads());                
            }
        }
        if(verbose) printf("Number of threads set to %d\n", nthreads);           
        is_init_cpu=is_init_gpu=0;    
}
    


void Context::set_gpu(int gpu_id){
        std::vector<int> gpu_selection={gpu_id};
        set_gpu(gpu_selection);
}


void Context::set_gpu(std::vector<int> gpu_selection){
    #ifdef WITH_CUDA
        std::vector<std::string> dev_name;
        gpu_all.clear();
        gpu_list.clear();
        if(verbose) printf("Selecting GPUs...\n");
        Util::get_gpu_list(dev_name, gpu_all);
        
        for(auto des_id : gpu_selection){
            bool found=0;
            for(int i=0; i<gpu_all.size(); i++){
                if(gpu_all[i]==des_id){
                    gpu_list.push_back(des_id);
                    if(verbose) printf("\t%d: %s\n",des_id, dev_name[i].c_str());
                    found=1;
                }
                
            }
            if(!found){
                printf("\tWARNING: GPU with id %d not found. Excluded from list.\n", des_id);
            }                  
        }
        
        if(gpu_list.size()==0){
            printf("ERROR: No GPU found.\n");
            use_cpu();
        }
        else{
            if(verbose) printf("%d GPUs found (id:", int(gpu_list.size()));
            if(verbose) for(auto id : gpu_list) printf(" %d", id);
            if(verbose) printf(")\n");
            use_gpu();
        }
    
    #else
        printf("WARNING: GPUs not enabled. Operation has no effect\n");
    #endif
        
        is_init_gpu=0;
        
    }

std::tuple<int,int> Context::get_threads(){
        return std::tuple<int,int>{omp_get_max_threads(), nthreads};
    }
    
std::tuple<std::vector<int>,std::vector<int>> Context::get_gpu(){
    #ifdef WITH_CUDA
        return std::tuple<std::vector<int>,std::vector<int>>{this->gpu_all, this->gpu_list};
    #else
        printf("WARNING: GPUs not enabled.\n");
        return std::tuple<std::vector<int>,std::vector<int>>{std::vector<int>({}), std::vector<int>({})};  
    #endif
    }
    
void Context::init_gpu(){
    #ifdef WITH_CUDA
    if(on_gpu==1){
        std::size_t maxheap=std::size_t(MAX_THREADS_PER_GPU)*std::size_t(256*256*256)*std::size_t(3)*sizeof(FLOAT_TYPE);
        
        for(auto dev_id: gpu_list){
                cudaSetDevice(dev_id);
                cudaDeviceSetLimit(cudaLimitMallocHeapSize, maxheap);
        }
        
        if(!is_init_gpu && gpu_list.size()>0){
                nthreads_gpu=nthreads;
                
                FLOAT_TYPE nthreads_per_gpu=FLOAT_TYPE(nthreads)/FLOAT_TYPE(gpu_list.size());
                if(nthreads_per_gpu>MAX_THREADS_PER_GPU) nthreads_gpu=MAX_THREADS_PER_GPU*gpu_list.size();
                if(nthreads_per_gpu<1) nthreads_gpu=gpu_list.size();
                
                
                for(auto& gpucont:gpu_context_list) delete gpucont;
                gpu_context_list.resize(nthreads_gpu);
                            
                if(verbose) printf("GPU initialization... nthreads_gpu set to %d", nthreads_gpu);
                
        #pragma omp parallel num_threads(nthreads_gpu)
                {
                    int tempid = omp_get_thread_num();
                    gpu_context_list[tempid]=new MSFT_gpu_context(1, gpu_list[tempid%gpu_list.size()], 128, 1);

                }
            is_init_gpu=1;
            if(verbose) printf("Done. \n");
        }
    }
    else{
        for(auto& gpucont:gpu_context_list) delete gpucont;  
        gpu_context_list.resize(0);
        is_init_gpu=1;
    }
    #endif        
        
    }
    
    void Context::init_cpu(){
    if(!is_init_cpu){
        if(verbose) printf("CPU initialization... ");
        is_init_cpu=1;
        if(verbose) printf("Done. \n");        
        }
    }
    
void Context::init(){
    init_gpu();
    init_cpu();
}

void Context::info(){ 
        printf("*Experiment:\n");
        printf("    - wavelength:              %.1f\n", lambda);        
        printf("    - angle:                   %.1f°\n", output_theta);
        printf("    - resolution:              %dx%d\n", output_npixel, output_npixel);   
        printf("    - photon density:          %.2e\n", photon_density);   
        printf("*MSFT:\n");
        printf("    - rendering oversampling:  %d\n", os );        
        printf("    - fourier oversampling:    %.1f\n", os_ft );  
        printf("*Hardware:\n");
        printf("    - Available threads:       %d\n", int(omp_get_max_threads()));        
        printf("    - Selected threads:        %d\n", nthreads);        
#ifdef WITH_CUDA        
        printf("    - Available GPUs:          ");
        for(auto igpu : gpu_all) printf("GPU-%d ", igpu);
        printf("\n");
        printf("    - Selected GPUs:           ");
        for(auto igpu : gpu_list) printf("GPU-%d ", igpu);
        printf("\n");
#else
        printf("    - GPU computing not enabled\n");
#endif
        
    }
 
