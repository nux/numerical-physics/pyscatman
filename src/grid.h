#pragma once
#include "datatypes.h"
#include <vector>
#include <cmath>


#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif

class Grid{
public:
    
    Grid(std::vector<int> dim_, std::vector<FLOAT_TYPE> min_, std::vector<FLOAT_TYPE> step_, FLOAT_TYPE latitude_ = 0, FLOAT_TYPE longitude_ = 0, FLOAT_TYPE rotation_ = 0): 
                        min_x(min_[0]),min_y(min_[1]),min_z(min_[2]),
                        dim_x(dim_[0]),dim_y(dim_[1]),dim_z(dim_[2]),
                        step_x(step_[0]),step_y(step_[1]),step_z(step_[2]){
        
        size=dim_x*dim_y*dim_z;

        
        
        rotation  = -rotation_ * std::atan(1.0)*4./180.;
        latitude  = (90.+latitude_)*std::atan(1.0)*4./180.;
        longitude = -longitude_*std::atan(1.0)*4./180.;
        
    } 
    
    __host__ __device__  
    void get_coord(int x, int y, int z, FLOAT_TYPE* coords){
        
        coords[0]=min_x + step_x/FLOAT_TYPE(2) + step_x*FLOAT_TYPE(x);
        coords[1]=min_y + step_y/FLOAT_TYPE(2) + step_y*FLOAT_TYPE(y);
        coords[2]=min_z + step_z/FLOAT_TYPE(2) + step_z*FLOAT_TYPE(z);
        
        
//         FLOAT_TYPE rotaxis[3] = {0,0,1};
//         if(rotation!= 0)
//             rotate(&rotaxis[0], rotation, &coords[0]);
//         
//         rotaxis[0] = FLOAT_TYPE(std::cos(rotation-longitude));
//         rotaxis[1] = FLOAT_TYPE(std::sin(rotation-longitude));
//         rotaxis[2] = 0;
// 
//         rotate(&rotaxis[0], latitude, &coords[0]);
        
        
        FLOAT_TYPE rotaxis[3] = {0,0,1};
        FLOAT_TYPE rotaxis2[3] = {0,1,0};
        FLOAT_TYPE rotaxis3[3] = {0,0,1};

        if(rotation!= 0)
            rotate(&rotaxis[0], rotation, &coords[0]);
            rotate(&rotaxis[0], rotation, &rotaxis2[0]);
            rotate(&rotaxis[0], rotation, &rotaxis3[0]);


        rotate(&rotaxis2[0], latitude, &coords[0]);
        rotate(&rotaxis2[0], latitude, &rotaxis3[0]);
        
        rotate(&rotaxis3[0], longitude, &coords[0]);
                
         
    }
    
    FLOAT_TYPE get_cell_volume(){
        return step_x*step_y*step_z;
    }
    
    int get_size(){return size;}
    
    void print_grid(){
        for(int z=0; z<dim_z; z++)
            for(int y=0; y<dim_y; y++)
                    for(int x=0; x<dim_x; x++){
                        std::vector<FLOAT_TYPE> temp(3);
                        get_coord(x,y,z,&temp[0]);
                        printf("%f\t%f\t%f\n", temp[0], temp[1], temp[2]);
                    }
        return;
    }
    
    __host__ __device__  
    void rotate(FLOAT_TYPE* axis, FLOAT_TYPE angle, FLOAT_TYPE* val){        
        FLOAT_TYPE sum=0;
        for(int i=0; i<3; i++) sum+=axis[i]*axis[i];
        
        sum = std::sqrt(sum);
//         for(auto& ax : axis) ax/=sum;
        FLOAT_TYPE ux = axis[0]/sum;
        FLOAT_TYPE uy = axis[1]/sum;
        FLOAT_TYPE uz = axis[2]/sum;
        
        
        FLOAT_TYPE cos_a = FLOAT_TYPE(std::cos(angle));
        FLOAT_TYPE sin_a = FLOAT_TYPE(std::sin(angle));
        

        FLOAT_TYPE temp[3];


        temp[0] =       (cos_a + ux*ux*(1.-cos_a))  *val[0] + 
                        (ux*uy*(1.-cos_a)-uz*sin_a) *val[1] + 
                        (ux*uz*(1.-cos_a)+uy*sin_a) *val[2];
        temp[1] =       (uy*ux*(1.-cos_a)+uz*sin_a) *val[0] + 
                        (cos_a + uy*uy*(1-cos_a))   *val[1] +
                        (uy*uz*(1.-cos_a)-ux*sin_a) *val[2];
        temp[2] =       (uz*ux*(1.-cos_a)-uy*sin_a) *val[0] +
                        (uz*uy*(1.-cos_a)+ux*sin_a) *val[1] + 
                        (cos_a + uz*uz*(1.-cos_a))  *val[2];

        val[0] = temp[0];
        val[1] = temp[1];
        val[2] = temp[2];
                  
        
    }
    
    

    int size;
    double rotation;
    double latitude;
    double longitude;
    FLOAT_TYPE min_x, min_y, min_z;
    FLOAT_TYPE dim_x, dim_y, dim_z;
    FLOAT_TYPE step_x, step_y, step_z;
};


#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif
