#pragma once
#include <omp.h>
#include <stdio.h>
#include <vector>
#include "datatypes.h"
#include <cmath>
#include <string>
#include <tuple>

#ifdef WITH_CUDA
class MSFT_gpu_context;
#endif


class Context{
public:
    Context(){
    verbose=0;
    omp_set_nested(1);
    omp_set_dynamic(0);
    set_threads(0);
    set_photon_density(1.e6);
      
#ifdef WITH_CUDA
    set_gpu(0);
    if(!use_gpu())  use_cpu();
#else
    use_cpu();
#endif
    lambda=60.;
    output_theta=30;
    output_npixel=1024;
    scale_npixel=output_npixel;
    os_ft=2;
    os=3;
    os_z=0.5;

    set_values(); 
    is_init_cpu=is_init_gpu=0;
    };
    
    ~Context();
    
    // // // // // // // // // // // 
    void set_photon_density(FLOAT_TYPE photon_density_){
        photon_density=photon_density_;
    }
    
    void set_lambda(FLOAT_TYPE lambda_){
        lambda=lambda_;
        set_values();
        is_init_cpu=is_init_gpu=0;
    }
    void set_angle(FLOAT_TYPE output_theta_){
        output_theta=output_theta_;
        set_values();
        is_init_cpu=is_init_gpu=0;
    }
    void set_resolution(int output_npixel_, int scale_npixel_=-1){
        output_npixel=output_npixel_;
        if(scale_npixel_<=0 || scale_npixel_>output_npixel_) scale_npixel=output_npixel;
        else scale_npixel=scale_npixel_;
        set_values();
        is_init_cpu=is_init_gpu=0;
    }
    void set_oversampling(FLOAT_TYPE os_){
        os=os_;
        set_values();
        is_init_cpu=is_init_gpu=0;
    }
    void set_fourier_oversampling(FLOAT_TYPE os_ft_){
        os_ft=os_ft_;
        set_values();
        is_init_cpu=is_init_gpu=0;
    }
    
    void set_values(){
        K = 2.*M_PI/lambda; 
        output_thetarad = M_PI*output_theta/180.;
//         qmax = K*2.*std::sin(output_thetarad/2.);
        qmax = K*std::sin(output_thetarad);

        dq = 2.*qmax/FLOAT_TYPE(output_npixel);
        
        npixel = os_ft*output_npixel;
        
        dx = M_PI/qmax/os_ft;
        dz = dx/FLOAT_TYPE(os_z);
        
        
//         printf("K: %f, dq: %f, dx: %f, lambda: %f, thetarad: %f\n", K,dq,dx, lambda, thetarad);
    }
    
    void set_verbose(bool verbosity=1){
        verbose=verbosity;
    }
    
    void set_threads(int nthreads);
    

    std::tuple<int,int> get_threads();
    
    std::tuple<std::vector<int>,std::vector<int>> get_gpu();
    
    
// // // // // // // // // // // //     
    
    void set_gpu(std::vector<int> gpu_selection);
    void set_gpu(int gpu_id);
    bool use_gpu();
    bool use_cpu();
    void init();
    void init_cpu();
    void init_gpu();
    void info();
//     void set_gpu_context();
    
        
    int nthreads;

#ifdef WITH_CUDA
    int nthreads_gpu;

    std::vector<int> gpu_list;
    std::vector<int> gpu_all;
    std::vector<MSFT_gpu_context*> gpu_context_list;
#endif
    
    bool on_gpu;
    bool is_init_cpu;
    bool is_init_gpu;
    bool verbose;
// // // // // // // // // // 
    FLOAT_TYPE lambda;
    FLOAT_TYPE photon_density;
    FLOAT_TYPE output_theta;
    FLOAT_TYPE output_thetarad;

    int output_npixel;
    int scale_npixel;
    FLOAT_TYPE os_ft;
    int os;
    FLOAT_TYPE os_z;
    

    FLOAT_TYPE dx;
    FLOAT_TYPE dz;
    FLOAT_TYPE dq;
    FLOAT_TYPE qmax;
    FLOAT_TYPE K;     
    int npixel;
};


// template<class T> void CheckBounds(T val, T min, T max){
//     if(val<min || val>max){
//         throw std::range_error("Value "+ std::to_string(val) + " must be in the range ["+std::to_string(min)+","+std::to_string(max)+ "]");
//     }
//     
// }
