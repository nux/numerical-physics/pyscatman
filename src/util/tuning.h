#include <thread>
#include <string>
#include <vector>

namespace Util{

#ifdef WITH_CUDA
void get_gpu_list(std::vector<std::string>& dev_name, std::vector<int>& dev_id);
#endif
    
void tuning(std::vector<std::string>& dev_name, std::vector<int>& dev_count, std::vector<std::vector<int>>& dev_ids);
};
