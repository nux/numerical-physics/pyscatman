#include "util/tuning.h"
#include <string>



namespace Util{

#ifdef WITH_CUDA
void get_gpu_list(std::vector<std::string>& dev_name, std::vector<int>& dev_id){
    
    int n_gpus;
    cudaGetDeviceCount(&n_gpus);
    
    dev_name.clear();
    dev_id.clear();
    
    for (int i = 0; i < n_gpus; i++) {
            cudaDeviceProp prop;
            cudaGetDeviceProperties(&prop, i);
            dev_id.push_back(i);
            dev_name.push_back(std::string(prop.name));
        }
}
#endif

void tuning(std::vector<std::string>& dev_name, std::vector<int>& dev_count, std::vector<std::vector<int>>& dev_ids){
  
    
        int n_cpus = std::thread::hardware_concurrency();
//         printf("%d concurrent threads are supported.\n", n);
    
        if(n_cpus>0){
            dev_name.push_back("CPUs");
            dev_count.push_back(n_cpus);
            std::vector<int> temp_ids;
            for(int i=0; i<n_cpus; i++) temp_ids.push_back(i);
            dev_ids.push_back(temp_ids);            
        }
#ifdef WITH_CUDA
        int n_gpus;

        cudaGetDeviceCount(&n_gpus);
        if(n_gpus>0){
            dev_name.push_back("GPUs");
            dev_count.push_back(n_gpus);
            std::vector<int> temp_ids;
            for(int i=0; i<n_gpus; i++) temp_ids.push_back(i);
            dev_ids.push_back(temp_ids);   
        }
        
        for (int i = 0; i < n_gpus; i++) {
            cudaDeviceProp prop;
            cudaGetDeviceProperties(&prop, i);
            printf("Device Number: %d\n", i);
            printf("  Device name: %s\n", prop.name);
            printf("  Memory Clock Rate (KHz): %d\n",
                prop.memoryClockRate);
            printf("  Memory Bus Width (bits): %d\n",
                prop.memoryBusWidth);
            printf("  Max threads per block: %d\n",
                prop.maxThreadsPerBlock);
            printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
                2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
        }    
#endif 
            
        
        
    }
};
