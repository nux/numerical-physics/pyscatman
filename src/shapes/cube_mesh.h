#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>

#include "grid.h"
#include "shapes/shape.h"


#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif


#ifdef SINGLE_PRECISION
    #define ATAN2 atan2f
    #define COS   cosf
    #define SIN     sinf
    #define SQRT  sqrtf
    #define ABS fabsf
    #define FLOOR floorf
#else
    #define ATAN2 atan2
    #define COS   cos
    #define SIN     sin
    #define SQRT  sqrt
    #define ABS fabs
    #define FLOOR floor

#endif

// int factorial(int n)
// {
//   return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
// }

namespace Shapes{

class CubeMesh: public Shape{
public:
    __host__ __device__ CubeMesh(): Shape(){}
    __host__ CubeMesh(FLOAT_TYPE a_, FLOAT_TYPE* radii_, int sidesize_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_,
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_=0, FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0 ): 
              Shape(delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_),
                a(a_), sidesize(sidesize_){

            radii = new FLOAT_TYPE[sidesize*sidesize*6];
            
            buffer = (void*) radii;
            buffer_size = sidesize*sidesize*6*sizeof(FLOAT_TYPE);
            
            maxDim=0;
            
            FLOAT_TYPE maxval=0;
            for(int i=0; i<sidesize*sidesize*6;i++){
                radii[i]=radii_[i];
                if(radii[i]>maxval) maxval=radii[i];
            }
            

            if(a<=0) maxDim=2.*maxval;
            
            else{
                FLOAT_TYPE norm=a/maxval;
            
                for(int i=0; i<sidesize*sidesize*6; i++){
                    radii[i]*=norm;
                }
                maxDim = 2.*a;
            }
      }
    
            
        
    __host__ __device__ CubeMesh(const CubeMesh& in): Shapes::Shape(in){
        a=in.a;
        maxDim=in.maxDim;
        sidesize=in.sidesize;
        radii= new FLOAT_TYPE[sidesize*sidesize*6];
        buffer = (void*) radii;
        buffer_size = in.buffer_size;

    }

    __host__ __device__ ~CubeMesh(){
//         printf("SH destructor\n");        
        delete [] radii; 
    }

    
    void init(){
        
        FLOAT_TYPE* faces[6];
        
        for(int iface=0; iface<6; iface++){
            faces[iface]=&radii[sidesize*sidesize*iface];
        }
    
        for(int y=0; y< sidesize; y++){
                faces[1][sidesize*y]=faces[0][sidesize*y+sidesize-1];
                faces[2][sidesize*y]=faces[1][sidesize*y+sidesize-1];
                faces[3][sidesize*y]=faces[2][sidesize*y+sidesize-1];
                faces[0][sidesize*y]=faces[3][sidesize*y+sidesize-1];
        }

//         for(int y=0; y< sidesize; y++){
        for(int y=0; y< sidesize; y++)       faces[4][sidesize*y]=faces[3][y];
        for(int y=0; y< sidesize; y++)        faces[4][sidesize*y+sidesize-1]=faces[1][sidesize-1-y];

        for(int y=0; y< sidesize; y++)        faces[5][sidesize*y]=faces[3][sidesize*sidesize-1-y];
        for(int y=0; y< sidesize; y++)        faces[5][sidesize*y+sidesize-1]=faces[1][sidesize*(sidesize-1)+y];

//         }
        

//         for(int x=0; x< sidesize; x++){
        for(int x=0; x< sidesize; x++)        faces[4][x]=faces[2][sidesize-1-x];
        for(int x=0; x< sidesize; x++)        faces[4][sidesize*(sidesize-1)+x]=faces[0][x];

        for(int x=0; x< sidesize; x++)        faces[5][x]=faces[0][sidesize*(sidesize-1)+x];
        for(int x=0; x< sidesize; x++)        faces[5][sidesize*(sidesize-1)+x]=faces[2][sidesize*sidesize-1-x];

//         }
    }
    
// // // // // // // // // // // // // // // // // // // // // // 

__host__ __device__  void convert_xyz_to_cube_uv(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z, int &index, FLOAT_TYPE &u, FLOAT_TYPE &v){
  FLOAT_TYPE absX = fabs(x);
  FLOAT_TYPE absY = fabs(y);
  FLOAT_TYPE absZ = fabs(z);
  
  int isXPositive = x > 0 ? 1 : 0;
  int isYPositive = y > 0 ? 1 : 0;
  int isZPositive = z > 0 ? 1 : 0;
  
  FLOAT_TYPE maxAxis, uc, vc;
  
  // POSITIVE X
  if (isXPositive && absX >= absY && absX >= absZ) {

    maxAxis = absX;
    uc = y;
    vc = z;
    index = 0;
  }
  // NEGATIVE X
  if (!isXPositive && absX >= absY && absX >= absZ) {

    maxAxis = absX;
    uc = -y;
    vc = z;
    index = 2;
  }
  // POSITIVE Y
  if (isYPositive && absY >= absX && absY >= absZ) {

    maxAxis = absY;
    uc = -x;
    vc = z;
    index = 1;
  }
  // NEGATIVE Y
  if (!isYPositive && absY >= absX && absY >= absZ) {

    maxAxis = absY;
    uc = x;
    vc = z;
    index = 3;
  }
  // POSITIVE Z
  if (isZPositive && absZ >= absX && absZ >= absY) {

    maxAxis = absZ;
    uc = y;
    vc = -x;
    index = 5;
  }
  // NEGATIVE Z
  if (!isZPositive && absZ >= absX && absZ >= absY) {

    maxAxis = absZ;
    uc = y;
    vc = x;
    index = 4;
  }

//   if(tan_adjust){
    // Do tangent correction
    u = 0.5f * (std::atan(uc / maxAxis)*4.f/M_PIF+ 1.0f);
    v = 0.5f * (std::atan(vc / maxAxis)*4.f/M_PIF+ 1.0f);
//   }
//   else{
//     // Convert range from -1 to 1 to 0 to 1
//     u = 0.5f * (uc / maxAxis + 1.0f);
//     v = 0.5f * (vc / maxAxis + 1.0f);
// }
}


__host__ __device__  void convert_cube_index_to_xyz(int iface, FLOAT_TYPE u, FLOAT_TYPE v, FLOAT_TYPE& x, FLOAT_TYPE& y, FLOAT_TYPE& z, FLOAT_TYPE step){
  FLOAT_TYPE coords[3];
  
  coords[0]=std::tan((FLOAT_TYPE(u)*step*2.f-1.f)*M_PIF/4.f);
  coords[1]=std::tan((FLOAT_TYPE(v)*step*2.f-1.f)*M_PIF/4.f);

//   coords[0]=start+step/2.+step*FLOAT_TYPE(u);
//   coords[1]=start+step/2.+step*FLOAT_TYPE(v);
//   coords[0]=start+step/2.+step*FLOAT_TYPE(u);
//   coords[1]=start+step/2.+step*FLOAT_TYPE(v);
  coords[2]=1.;
  
  if(iface==0){
      x=coords[2];
      y=coords[0];
      z=-coords[1];
  }
  else if(iface==1){
      x=-coords[0];
      y=coords[2];
      z=-coords[1];
  }
  else if(iface==2){
      x=-coords[2];
      y=-coords[0];
      z=-coords[1];
  }  
  else if(iface==3){
      x=coords[0];
      y=-coords[2];
      z=-coords[1];
  }   
  else if(iface==4){
      x=coords[1];
      y=coords[0];
      z=coords[2];
  }       
  else if(iface==5){
      x=-coords[1];
      y=coords[0];
      z=-coords[2];
  }    

}


// // // // // // // // // // // // // // // // // // // 


__host__ __device__ FLOAT_TYPE linearInterpolate (FLOAT_TYPE p[2], FLOAT_TYPE x) {
	return (1.f-x)*p[0] + x*p[1];
}

__host__ __device__ FLOAT_TYPE bilinearInterpolate (FLOAT_TYPE p[2][2], FLOAT_TYPE x, FLOAT_TYPE y) {
	FLOAT_TYPE arr[2];
	arr[0] = linearInterpolate(p[0], x);
	arr[1] = linearInterpolate(p[1], x);
	return linearInterpolate(arr, y);
}

__host__ __device__ FLOAT_TYPE linearInterpolate2 (FLOAT_TYPE p[2], FLOAT_TYPE x, FLOAT_TYPE theta) {
// 	return (1.f-x)*p[0] + x*p[1];
	return p[0]*p[1]*std::sin(theta)/(p[0]*std::sin(x*theta)+p[1]*std::sin(theta*(1.f-x)));

    
}

__host__ __device__ FLOAT_TYPE bilinearInterpolate2 (FLOAT_TYPE p[2][2], FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE theta[3]) {
	FLOAT_TYPE arr[2];
	arr[0] = linearInterpolate2(p[0], x, theta[0]);
	arr[1] = linearInterpolate2(p[1], x, theta[1]);
	return linearInterpolate2(arr, y, theta[2]);
}

__host__ __device__ FLOAT_TYPE get_angle_from_uv(FLOAT_TYPE p[2][2], FLOAT_TYPE sidesize) {
    
    FLOAT_TYPE anglep[2][2];
    
    for(int i=0; i<2;i++)
        for(int j=0; j<2;j++)
            anglep[i][j]=(p[i][j]/(sidesize-1)*2.f-1.f)*M_PIF/4.;
        
    FLOAT_TYPE norm[2]={0,0};
    FLOAT_TYPE proj=0;
    
    for(int i=0; i<2;i++){
        norm[0]+=std::tan(anglep[0][i])*std::tan(anglep[0][i]);
        norm[1]+=std::tan(anglep[1][i])*std::tan(anglep[1][i]);
        proj+=std::tan(anglep[0][i])*std::tan(anglep[1][i]);
    }
    
    norm[0]=std::sqrt(norm[0]+1.f);
    norm[1]=std::sqrt(norm[1]+1.f);
    proj+=1.;
        
    return std::acos(proj/(norm[0]*norm[1]));
}


   __host__ __device__ FLOAT_TYPE get_radius(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
       
    int iface;
    FLOAT_TYPE u,v;
    
    
    convert_xyz_to_cube_uv(xx,yy,zz, iface,  u, v);
    
    FLOAT_TYPE* face=&radii[sidesize*sidesize*iface];
   
    u=u*FLOAT_TYPE(sidesize-1)*0.999;
    v=v*FLOAT_TYPE(sidesize-1)*0.999;
    int ui=int(std::floor(u));
    int vi=int(std::floor(v));
    
    FLOAT_TYPE val=0;

    FLOAT_TYPE points[2][2];
    
    for(int j=0; j<2; j++)
        for(int i=0; i<2; i++){
            points[j][i]=face[(ui+i)    +sidesize*(vi+j) ];
            
        }

    FLOAT_TYPE udec= u-FLOAT_TYPE(ui);
    FLOAT_TYPE vdec= v-FLOAT_TYPE(vi);
//     val = bilinearInterpolate (points, udec, vdec);        
    FLOAT_TYPE thetaval=M_PIF/2./(sidesize-1);

    FLOAT_TYPE theta[3]={thetaval, thetaval, thetaval};
    FLOAT_TYPE pt[2][2]={{ui,vi},{ui+1, vi}};
    theta[0]=get_angle_from_uv(pt, sidesize);
    FLOAT_TYPE pt1[2][2]={{ui,vi+1},{ui+1, vi+1}};
    theta[1]=get_angle_from_uv(pt1, sidesize);
    FLOAT_TYPE pt2[2][2]={{u,vi},{u, vi+1}};
    theta[2]=get_angle_from_uv(pt2, sidesize); 
    
//     printf("%f, %f, %f, %f\n", theta[0], theta[1], theta[2], thetaval);
    
    return bilinearInterpolate2(points, udec, vdec, theta);  
    //     val = bilinearInterpolate (points, udec, vdec);        

   }


// // // // // // // // // // // // // // // // // // // 
   __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){

    FLOAT_TYPE val = get_radius( xx,  yy,  zz);

        
    FLOAT_TYPE r=SQRT(xx*xx+yy*yy+zz*zz);

    if(r<=ABS(val)) return 1;
    else return 0;
    
//     printf("r=%f, grid=%f\n", r, radii[iphi+nphi*itheta]);

    }

    
    
    
    __host__ __device__ void get_refractive_index(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz, FLOAT_TYPE (&value)[2]){
        FLOAT_TYPE dens_val = get_density(xx, yy, zz);
        if(dens_val>0.5){
            value[0] = 1.-get_delta(xx,yy,zz);
            value[1] = get_beta(xx,yy,zz);
           }
        else{
            value[0] = 1.;
            value[1] = 0.;
           }
        }  
    
    virtual size_t mysize(){return sizeof(CubeMesh);}
    virtual int mytype(){return shape_type::CubeMesh;};
    virtual std::string get_name(){return std::string("CubeMesh");};
    
    FLOAT_TYPE a;
    
    FLOAT_TYPE* radii;

    int sidesize;
    bool is_init;

};

};

#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif

