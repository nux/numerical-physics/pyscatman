#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>

#include "grid.h"
#include "shapes/shape.h"

#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif

namespace Shapes{

class Ellipsoid: public Shape{
public:
    __host__ __device__ Ellipsoid(): Shape(){}
    __host__ __device__ Ellipsoid(FLOAT_TYPE a_, FLOAT_TYPE b_, FLOAT_TYPE c_, 
              FLOAT_TYPE delta_, FLOAT_TYPE beta_,
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_=0, FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0 ): Shape(delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_),
                a(a_), b(b_), c(c_){
                    if(b<0) b=a;
                    if(c<0) c=a;
                    maxDim = a;
                    if(b>maxDim) maxDim=b;
                    if(c>maxDim) maxDim=c;      
                    maxDim*=2.;
                }
    
    __host__ __device__ Ellipsoid(const Ellipsoid& in){
        a=in.a;
        b=in.b;
        c=in.c;
        latitude=in.latitude;
        longitude=in.longitude;
        orientation=in.orientation;
        maxDim=in.maxDim;
        delta=in.delta;
        beta=in.beta;       
    }



   __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
            if(xx*xx/(b*b) + yy*yy/(c*c) + zz*zz/(a*a)<=1)
                return 1.;
            else 
                return 0;
        }
    
    __host__ __device__ void get_refractive_index(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz, FLOAT_TYPE (&value)[2]){
            if(xx*xx/(b*b) + yy*yy/(c*c) + zz*zz/(a*a)<=1){
                value[0] = 1.-delta;
                value[1] = beta;
            }
            else{
                value[0] = 1.;
                value[1] = 0.;
            }
        }
    
    size_t mysize(){return sizeof(Ellipsoid);}
    int mytype(){return shape_type::Ellipsoid;};
    std::string get_name(){return std::string("Ellipsoid");};
    
    FLOAT_TYPE a;
    FLOAT_TYPE b;
    FLOAT_TYPE c;



};

};

#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif

