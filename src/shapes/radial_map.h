#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>

#include "grid.h"
#include "shapes/shape.h"


#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif


#ifdef SINGLE_PRECISION
    #define ATAN2 atan2f
    #define COS   cosf
    #define SIN     sinf
    #define SQRT  sqrtf
    #define ABS fabsf
    #define FLOOR floorf
#else
    #define ATAN2 atan2
    #define COS   cos
    #define SIN     sin
    #define SQRT  sqrt
    #define ABS fabs
    #define FLOOR floor

#endif

// int factorial(int n)
// {
//   return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
// }

namespace Shapes{

class RadialMap: public Shape{
public:
    __host__ __device__ RadialMap(): Shape(){}
    __host__ RadialMap(FLOAT_TYPE a_, FLOAT_TYPE* radii_, int ntheta_, int nphi_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_,
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_=0,  FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0 ): 
              Shape(delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_),
                a(a_), ntheta(ntheta_), nphi(nphi_){

            dtheta_sh = M_PI/FLOAT_TYPE(ntheta);
            dphi_sh = 2.*M_PI/FLOAT_TYPE(nphi);

            radii = new FLOAT_TYPE[ntheta*nphi];
            
            buffer = (void*) radii;
            buffer_size = ntheta*nphi*sizeof(FLOAT_TYPE);
            
            maxDim=0;
            
            if(radii_!=nullptr){
                FLOAT_TYPE maxval=0;
                for(int i=0; i< ntheta*nphi; i++){
                    radii[i]=radii_[i];
                    if(radii[i]>maxval) maxval=radii[i];
                }
                
                if(a<=0) maxDim=2.*maxval;
                
                else{
                    FLOAT_TYPE norm=a/maxval;
                
                    for(int i=0; i<ntheta*nphi; i++){
                        radii[i]*=norm;
                    }
                    maxDim = 2.*a;
                }
            }
            }
    
            
        
    __host__ __device__ RadialMap(const RadialMap& in): Shapes::Shape(in){
        a=in.a;
        maxDim=in.maxDim;
        dtheta_sh=in.dtheta_sh;
        dphi_sh=in.dphi_sh;
        ntheta=in.ntheta;
        nphi=in.nphi;
        radii= new FLOAT_TYPE[ntheta*nphi];
        buffer = (void*) radii;
        buffer_size = in.buffer_size;

    }

    __host__ __device__ ~RadialMap(){
//         printf("SH destructor\n");        
        delete [] radii; 
    }

    
    
   __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
       
    FLOAT_TYPE r=SQRT(xx*xx+yy*yy+zz*zz);
    FLOAT_TYPE phi=ATAN2(yy,xx);
    FLOAT_TYPE theta=ATAN2(SQRT(xx*xx+yy*yy),-zz);
// //     printf("%f, %f, %f\n", xx,yy,zz);


//     printf("%f\n", theta);
    
//     if(theta<0) printf("ERRORRRRRRR!\n");
    if(phi<0) phi+=2.*M_PI;
    
    
    FLOAT_TYPE fphi=phi/dphi_sh;
    FLOAT_TYPE ftheta=theta/dtheta_sh;
    
       
    int iphi = FLOOR(fphi);
    int itheta = FLOOR(ftheta);
    
    FLOAT_TYPE phi_dec=fphi-FLOOR(fphi);
    FLOAT_TYPE theta_dec=ftheta-FLOOR(ftheta);

    
//     if(iphi>=nphi) iphi-=nphi;
//     if(itheta>=ntheta) itheta-=ntheta;
    
    
    FLOAT_TYPE val = radii[(iphi)  %nphi  +nphi*((itheta)  %ntheta) ]*(1.-phi_dec)*(1.-theta_dec) +
                     radii[(iphi+1)%nphi  +nphi*((itheta)  %ntheta) ]*(   phi_dec)*(1.-theta_dec) +
                     radii[(iphi)  %nphi  +nphi*((itheta+1)%ntheta) ]*(1.-phi_dec)*(   theta_dec) +
                     radii[(iphi+1)%nphi  +nphi*((itheta+1)%ntheta) ]*(   phi_dec)*(   theta_dec);
    
    if(r<=ABS(val)) return 1;
    else return 0;
    
//     printf("r=%f, grid=%f\n", r, radii[iphi+nphi*itheta]);

    }
    
    
    __host__ __device__ void get_refractive_index(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz, FLOAT_TYPE (&value)[2]){
        FLOAT_TYPE dens_val = get_density(xx, yy, zz);
        if(dens_val>0.5){
            value[0] = 1.-get_delta(xx,yy,zz);
            value[1] = get_beta(xx,yy,zz);
           }
        else{
            value[0] = 1.;
            value[1] = 0.;
           }
        }  
    
    virtual size_t mysize(){return sizeof(RadialMap);}
    virtual int mytype(){return shape_type::RadialMap;};
    virtual std::string get_name(){return std::string("RadialMap");};
    
    FLOAT_TYPE a;
    FLOAT_TYPE dtheta_sh;
    FLOAT_TYPE dphi_sh;
    
        
    FLOAT_TYPE* radii;
    int nphi;
    int ntheta;
    
    bool is_init;


};

};

#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif

