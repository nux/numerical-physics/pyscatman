#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>
#include <random>

#include "grid.h"


#ifndef WITH_NVCC
#define __host__ 
#define __device__ 

#endif


// #ifdef WITH_NVCC
// class  MSFT_gpu_context;
// #endif

namespace Shapes{

    class Shape{
    public:
        __host__ __device__ Shape(){}
        __host__ __device__ Shape( FLOAT_TYPE delta_, FLOAT_TYPE beta_, FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_=0, FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0, FLOAT_TYPE scaling_ = 1, FLOAT_TYPE offset_=0): 
                        latitude(latitude_), longitude(longitude_), orientation(orientation_), delta(delta_), beta(beta_), beam_position(beam_position_), xshift(xshift_), yshift(yshift_), scaling(scaling_), offset(offset_), is_init(0), buffer(nullptr), buffer_size(0){}
        __host__ __device__ virtual ~Shape(){};
        __host__ __device__ Shape(const Shape& in) = default;
        
//         virtual int draw(std::vector<FLOAT_TYPE>& data, int npixel, FLOAT_TYPE& dx, FLOAT_TYPE& volume) = 0;
        __host__ __device__ virtual FLOAT_TYPE get_density(FLOAT_TYPE,FLOAT_TYPE,FLOAT_TYPE)=0;
        __host__ __device__ virtual FLOAT_TYPE get_delta(FLOAT_TYPE,FLOAT_TYPE,FLOAT_TYPE);
        __host__ __device__ virtual FLOAT_TYPE get_beta(FLOAT_TYPE,FLOAT_TYPE,FLOAT_TYPE);
        __host__ __device__ virtual void get_refractive_index(FLOAT_TYPE,FLOAT_TYPE,FLOAT_TYPE, FLOAT_TYPE (&value)[2])=0;
        
        __host__ __device__ FLOAT_TYPE get_density_os(FLOAT_TYPE* min, FLOAT_TYPE* max, int OS);
        __host__ __device__ FLOAT_TYPE get_delta_os(FLOAT_TYPE* min, FLOAT_TYPE* max, int OS);
        __host__ __device__ FLOAT_TYPE get_beta_os(FLOAT_TYPE* min, FLOAT_TYPE* max, int OS);
        __host__ __device__ void get_refractive_index_os(FLOAT_TYPE* min, FLOAT_TYPE* max, FLOAT_TYPE (&value)[2], int OS);
        
        __host__ virtual void init(){ is_init=1;}
        
// #ifdef WITH_NVCC
//         __host__ virtual void init_gpu(MSFT_gpu_context* context){ printf("Init GPU\n"); this->init(); }
// #endif
        template<typename get_value_function> __host__ __device__  
        FLOAT_TYPE get_value_os(FLOAT_TYPE* min, FLOAT_TYPE* max, int OS, get_value_function get_value);
        
        std::vector<FLOAT_TYPE> get(int npixel, FLOAT_TYPE& dx, int os=1, FLOAT_TYPE os_z=1);
    
        FLOAT_TYPE get_size(){return maxDim;}
        
        void set_fitting_properties(FLOAT_TYPE scaling_, FLOAT_TYPE offset_){scaling=scaling_, offset=offset_;}
        
        virtual std::string get_name(){return std::string("Undefined");};

        
        virtual size_t mysize(){return 0;};
        virtual int mytype(){return 0;};

        FLOAT_TYPE latitude;
        FLOAT_TYPE longitude;
        FLOAT_TYPE orientation;
        FLOAT_TYPE maxDim;
        FLOAT_TYPE delta;
        FLOAT_TYPE beta;
        FLOAT_TYPE beam_position;
        FLOAT_TYPE xshift;
        FLOAT_TYPE yshift;
        FLOAT_TYPE scaling;
        FLOAT_TYPE offset;
        bool is_init;
        
        void* buffer;
        size_t buffer_size;

        
    };
};

#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif
