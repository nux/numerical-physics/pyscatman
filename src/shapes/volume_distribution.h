#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>
#include <memory>
#include "grid.h"
#include "shapes/shape.h"


#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif


#ifdef SINGLE_PRECISION
    #define ATAN2 atan2f
    #define COS   cosf
    #define SIN     sinf
    #define SQRT  sqrtf
    #define ABS fabsf
    #define FLOOR floorf
#else
    #define ATAN2 atan2
    #define COS   cos
    #define SIN     sin
    #define SQRT  sqrt
    #define ABS fabs
    #define FLOOR floor

#endif

// int factorial(int n)
// {
//   return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
// }



namespace Shapes{


class VolumeDistribution: public Shape{
public:
    __host__ __device__ VolumeDistribution(): Shape(){}
    __host__ VolumeDistribution(FLOAT_TYPE dv_, COMPLEX_TYPE* data_, int xdim_, int ydim_, int zdim_,
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_=0, FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0 ): 
              Shape(0, 0, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_),
                dv(dv_), xdim(xdim_), ydim(ydim_), zdim(zdim_){

            /*int max_pdim=xdim;
            if(ydim>max_pdim) max_pdim=ydim;
            if(zdim>max_pdim) max_pdim=zdim;
            
            maxDim = FLOAT_TYPE(max_pdim)*dv;
            */
            maxDim=std::sqrt(std::pow(FLOAT_TYPE(xdim)*dv,2) + std::pow(FLOAT_TYPE(ydim)*dv,2) + std::pow(FLOAT_TYPE(zdim)*dv,2));

            data= new COMPLEX_TYPE[xdim*ydim*zdim];
            
            buffer = (void*) data;
            buffer_size = xdim*ydim*zdim*sizeof(COMPLEX_TYPE);
            
//             std::copy( (FLOAT_TYPE*) data_, (FLOAT_TYPE*) data_ + 2*xdim*ydim*zdim, (FLOAT_TYPE*) data);
            
            for(long int index=0; index<xdim*ydim*zdim; index++) {
                data[index][0]=data_[index][0];
                data[index][1]=data_[index][1];
            }
            

            }
    
__host__ __device__ 
    void add_complex(COMPLEX_TYPE &val1, COMPLEX_TYPE &val2, FLOAT_TYPE factor){
        val1[0]+=val2[0]*factor;
        val1[1]+=val2[1]*factor;
    }
        
    __host__ __device__ VolumeDistribution(const VolumeDistribution& in): Shapes::Shape(in){
        dv=in.dv;
        maxDim=in.maxDim;
        xdim=in.xdim;
        ydim=in.ydim;
        zdim=in.zdim;
        data= new COMPLEX_TYPE[xdim*ydim*zdim];
        buffer = (void*) data;
        buffer_size = in.buffer_size;        

    }

    __host__ __device__ ~VolumeDistribution(){
//         printf("SH destructor\n");        
        delete [] data; 
    }

    
    
__host__ __device__ void get_refractive_index(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz, FLOAT_TYPE (&value)[2]){
    
    FLOAT_TYPE fx = xx/dv + FLOAT_TYPE(xdim/2.);
    FLOAT_TYPE fy = yy/dv + FLOAT_TYPE(ydim/2.);
    FLOAT_TYPE fz = zz/dv + FLOAT_TYPE(zdim/2.);

    int ix = FLOOR(fx);
    int iy = FLOOR(fy);    
    int iz = FLOOR(fz);
    
    FLOAT_TYPE xdec=fx-FLOOR(fx);
    FLOAT_TYPE ydec=fy-FLOOR(fy);
    FLOAT_TYPE zdec=fz-FLOOR(fz);
    
    
    value[0]=0;
    value[1]=0;
    
    COMPLEX_TYPE tempval;
    
    get_data((ix  ), (iy  ), (iz  ), tempval);
    add_complex(value,    tempval,   (1.-xdec)*(1.-ydec)*(1.-zdec));
    
    get_data((ix+1), (iy  ), (iz  ), tempval);
    add_complex(value,    tempval,   (   xdec)*(1.-ydec)*(1.-zdec));
    
    get_data((ix  ), (iy+1), (iz  ), tempval);
    add_complex(value,    tempval,   (1.-xdec)*(   ydec)*(1.-zdec));
    
    get_data((ix+1), (iy+1), (iz  ), tempval);
    add_complex(value,    tempval,   (   xdec)*(   ydec)*(1.-zdec));
    
    get_data((ix  ), (iy  ), (iz+1), tempval);
    add_complex(value,    tempval,   (1.-xdec)*(1.-ydec)*(   zdec));
    
    get_data((ix+1), (iy  ), (iz+1), tempval);
    add_complex(value,    tempval,   (   xdec)*(1.-ydec)*(   zdec));
    
    get_data((ix  ), (iy+1), (iz+1), tempval);
    add_complex(value,    tempval,   (1.-xdec)*(   ydec)*(   zdec));
    
    get_data((ix+1), (iy+1), (iz+1), tempval);
    add_complex(value,    tempval,   (   xdec)*(   ydec)*(   zdec));
    
        
//     printf("r=%f, grid=%f\n", r, radii[iphi+nphi*itheta]);

    }
    
    
   __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
       COMPLEX_TYPE tempval;
       get_refractive_index(xx, yy, zz, tempval);
       return tempval[1];//(tempval[1]>0 ? 1 : 0);      
}

    
    __host__ __device__ void get_data(int x, int y, int z, COMPLEX_TYPE& val){
        if(x<0 || y<0 || z<0 || x>=xdim || y>=ydim || z>= zdim) {val[0] = 1; val[1]=0;}
        else {
            val[0] = data[x + y*xdim + z*xdim*ydim][0];
            val[1] = data[x + y*xdim + z*xdim*ydim][1];
        }
    }
    
    virtual size_t mysize(){return sizeof(VolumeDistribution);}
    virtual int mytype(){return shape_type::VolumeDistribution;};
    virtual std::string get_name(){return std::string("VolumeDistribution");};
    
    FLOAT_TYPE dv;
        
    COMPLEX_TYPE* data;
    int xdim;
    int ydim;
    int zdim;
    
    
    bool is_init;


};

};

#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif

