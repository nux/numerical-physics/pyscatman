#include "shapes/shape.h"

#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif


namespace Shapes{

// // // // // // // // // // // // // // // // // // // // //     
    
    __host__  __device__ FLOAT_TYPE Shape::get_delta(FLOAT_TYPE,FLOAT_TYPE,FLOAT_TYPE){return delta;};
    __host__  __device__ FLOAT_TYPE Shape::get_beta(FLOAT_TYPE,FLOAT_TYPE,FLOAT_TYPE){return beta;};
    
    
    __host__  __device__ FLOAT_TYPE Shape::get_density_os(FLOAT_TYPE* min, FLOAT_TYPE* max, int OS){
            return get_value_os(min, max,OS, [this](FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z){ return this->get_density(x,y,z);});
        }
    __host__  __device__ FLOAT_TYPE Shape::get_delta_os(FLOAT_TYPE* min, FLOAT_TYPE* max, int OS){
            return get_value_os(min, max,OS, [this](FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z){ return this->get_delta(x,y,z);});
        }
    __host__  __device__ FLOAT_TYPE Shape::get_beta_os(FLOAT_TYPE* min, FLOAT_TYPE* max, int OS){
            return get_value_os(min, max,OS, [this](FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z){ return this->get_beta(x,y,z);});
        }
        
    template<typename get_value_function>  __host__ __device__  
        FLOAT_TYPE Shape::get_value_os(FLOAT_TYPE* min, FLOAT_TYPE* max, int OS, get_value_function get_value){
            FLOAT_TYPE start[3];
            FLOAT_TYPE step[3];
            for(int i=0; i<3; i++){
                step[i]=(max[i]-min[i])/FLOAT_TYPE(OS);
                start[i] = min[i]+step[i]/FLOAT_TYPE(2);
            }
            
            FLOAT_TYPE sum=0;
            FLOAT_TYPE weights=0;
            
            for(int z=0; z<OS; z++)
                for(int y=0; y<OS; y++)
                    for(int x=0; x<OS; x++){
                        sum+=get_value(start[0]+step[0]*FLOAT_TYPE(x),
                                       start[1]+step[1]*FLOAT_TYPE(y),
                                       start[2]+step[2]*FLOAT_TYPE(z));
                        weights+=1;
                    }
                
            return sum/weights;
    }      

    
    
        
    __host__ __device__  
        void Shape::get_refractive_index_os(FLOAT_TYPE* min, FLOAT_TYPE* max, FLOAT_TYPE (&value)[2], int OS){
            FLOAT_TYPE start[3];
            FLOAT_TYPE step[3];
            for(int i=0; i<3; i++){
                step[i]=(max[i]-min[i])/FLOAT_TYPE(OS);
                start[i] = min[i]+step[i]/FLOAT_TYPE(2);
            }
            
            value[0]=0;
            value[1]=0;
            
            FLOAT_TYPE weights=0;
            
            for(int z=0; z<OS; z++)
                for(int y=0; y<OS; y++)
                    for(int x=0; x<OS; x++){
                        FLOAT_TYPE temp_value[2];
                        this->get_refractive_index(start[0]+step[0]*FLOAT_TYPE(x),
                                                start[1]+step[1]*FLOAT_TYPE(y),
                                                start[2]+step[2]*FLOAT_TYPE(z),
                                                temp_value);
                        value[0]+=temp_value[0];
                        value[1]+=temp_value[1];
                        weights+=1;
                    }
                
            value[0]/=weights;
            value[1]/=weights;
            
    }  
    
//         template<typename get_value_function>  __host__ __device__  
//         FLOAT_TYPE Shape::get_value_os(FLOAT_TYPE* min, FLOAT_TYPE* max, int OS, get_value_function get_value){
//             FLOAT_TYPE start[3];
//             FLOAT_TYPE step[3];
//             
//             for(int i=0; i<2; i++){
//                 step[i]=(max[i]-min[i])/FLOAT_TYPE(OS);
//                 start[i] = min[i]+step[i]/FLOAT_TYPE(2);
//             }
//             
//             int OSZ = std::ceil((max[2]-min[2])/(max[0]-min[0]))*OS;
//             step[2]=(max[2]-min[2])/FLOAT_TYPE(OSZ);
//             start[2] = min[2]+step[2]/FLOAT_TYPE(2);            
//             
//             FLOAT_TYPE sum=0;
//             FLOAT_TYPE weights=0;
//             
//             for(int z=0; z<OSZ; z++)
//                 for(int y=0; y<OS; y++)
//                     for(int x=0; x<OS; x++){
//                         sum+=get_value(start[0]+step[0]*FLOAT_TYPE(x),
//                                        start[1]+step[1]*FLOAT_TYPE(y),
//                                        start[2]+step[2]*FLOAT_TYPE(z));
//                         weights+=1;
//                     }
//                 
//             return sum/weights;
//     } 
    
    std::vector<FLOAT_TYPE> Shape::get(int npixel, FLOAT_TYPE& dx, int os, FLOAT_TYPE z_os){
        init();
        
        FLOAT_TYPE gridsize = maxDim*1.10; 
        dx = gridsize/FLOAT_TYPE(npixel);
//         printf("dx=%f\n", dx);
        FLOAT_TYPE dz = dx/z_os;
        int nslices = std::ceil(gridsize/dz);
        Grid grid({npixel,npixel,nslices}, 
                    {-gridsize/FLOAT_TYPE(2),-gridsize/FLOAT_TYPE(2),-gridsize/FLOAT_TYPE(2)},
                    { dx, dx, dz}, latitude, longitude, orientation);
        
        std::vector<FLOAT_TYPE> data(npixel*npixel*nslices);
        
        for(int zz=0; zz<grid.dim_z; zz++)
        for(int yy=0; yy<grid.dim_y; yy++)
        for(int xx=0; xx<grid.dim_x; xx++){
            int index=xx+yy*grid.dim_x+zz*grid.dim_x*grid.dim_y;
            std::vector<FLOAT_TYPE> coords(3);
            grid.get_coord(xx, yy, zz, &coords[0]);
            
            if(os <= 1)  
                data[index] = get_density(coords[0], coords[1], coords[2]);
            else{
                FLOAT_TYPE min[3]={coords[0]-dx/FLOAT_TYPE(2), coords[1]-dx/FLOAT_TYPE(2),coords[2]-dz/FLOAT_TYPE(2)};
                FLOAT_TYPE max[3]={coords[0]+dx/FLOAT_TYPE(2), coords[1]+dx/FLOAT_TYPE(2),coords[2]+dz/FLOAT_TYPE(2)};
                data[index] = get_density_os(min, max, os);
            }   
//             printf("index %d, value %f\n", index, data[index] );
        }
        return data;
    }
    
}

#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif
        
        
