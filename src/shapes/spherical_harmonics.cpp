#include "shapes/spherical_harmonics.h"
#ifdef _WIN32
    using std::sph_legendre;
//     using std::tr1::sph_legendre;
#else
    #include <tr1/cmath>
    using std::tr1::sph_legendre;
#endif


    
    
namespace Shapes{
    void SphericalHarmonics::init(){
        
        
        
        for(int i=0; i<ntheta*nphi; i++) radii[i]=0;
        

        for(int icoeff=0; icoeff<ncoefficients; icoeff+=1){
            if(coefficients[icoeff]!=0){
                int l = int(std::sqrt(icoeff));
                int m = icoeff -l*l-l;
                
                for(int itheta=0; itheta<ntheta; itheta++){
                    FLOAT_TYPE theta=FLOAT_TYPE(itheta)*dtheta_sh;
                    FLOAT_TYPE sph_leg_val = coefficients[icoeff]*sph_legendre(l, std::abs(m), theta);
                    if(m!=0) sph_leg_val*=std::sqrt(2);
                    for(int iphi=0; iphi<nphi; iphi++){
                        FLOAT_TYPE phi= FLOAT_TYPE(iphi)*dphi_sh;
                        int index=iphi+nphi*itheta;
                        
                        if(m>=0)
                            radii[index]+=sph_leg_val*std::cos(FLOAT_TYPE(m)*phi);
                        else
                            radii[index]+=sph_leg_val*std::sin(FLOAT_TYPE(-m)*phi);
                
                    
                    }
                }
            }

        }
            
        FLOAT_TYPE max=0;
        
        for(int i=0; i<ntheta*nphi; i++) if(radii[i]>max) max=radii[i];
        
        if(a<=0){
            FLOAT_TYPE norm = std::sqrt(4.*M_PI);
            maxDim = 2.*norm*max;
            for(int i=0; i<ntheta*nphi; i++) radii[i]*=norm;
        }

        else{
            maxDim = 2.*a;
            FLOAT_TYPE norm = a/max;    
                
            for(int i=0; i<ntheta*nphi; i++) radii[i]*=norm;
            
            is_init=1;
        }
        
    }
}


