#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>

#include "grid.h"
#include "shapes/shape.h"


#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif


#ifdef SINGLE_PRECISION
    #define ATAN2 atan2f
    #define COS   cosf
    #define SIN     sinf
    #define SQRT  sqrtf
    #define ABS fabsf
    #define FLOOR floorf
#else
    #define ATAN2 atan2
    #define COS   cos
    #define SIN     sin
    #define SQRT  sqrt
    #define ABS fabs
    #define FLOOR floor

#endif

// int factorial(int n)
// {
//   return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
// }

namespace Shapes{

class CubeFaces: public Shape{
public:
    __host__ __device__ CubeFaces(): Shape(){}
    __host__ CubeFaces(FLOAT_TYPE a_, FLOAT_TYPE* cuts_, int cutsize_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_,
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_=0, int facesize_=20, FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0 ): 
              Shape(delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_),
                a(a_),  cutsize(cutsize_), facesize(facesize_), sidesize(facesize_+4), cubic_int(0){

                    
            cuts = new FLOAT_TYPE[cutsize*cutsize*6];
            radii = new FLOAT_TYPE[sidesize*sidesize*6];

            buffer = (void*) radii;
            buffer_size = sidesize*sidesize*6*sizeof(FLOAT_TYPE);
            
            sidepixsize=2.*std::sqrt(2.)/FLOAT_TYPE(cutsize);
            maxDim=0;
            
            FLOAT_TYPE maxval=0;
            for(int i=0; i<cutsize*cutsize*6;i++){
                cuts[i]=cuts_[i];
                if(cuts[i]>maxval) maxval=cuts[i];
            }
            

            if(a<=0) maxDim=2.*maxval;
            
            else{
                FLOAT_TYPE norm=a/maxval;
            
                for(int i=0; i<cutsize*cutsize*6; i++){
                    cuts[i]*=norm;
                }
                maxDim = 2.*a;
            }
      }
    
            
        
    __host__ __device__ CubeFaces(const CubeFaces& in): Shapes::Shape(in){
        a=in.a;
        maxDim=in.maxDim;
        cutsize=in.cutsize;
        facesize=in.facesize;
        sidesize=in.sidesize;
        sidepixsize=in.sidepixsize;
        cubic_int=in.cubic_int;
        is_init=0;
        cuts= new FLOAT_TYPE[cutsize*cutsize*6];
        radii= new FLOAT_TYPE[sidesize*sidesize*6];
        buffer = (void*) radii;
        buffer_size = in.buffer_size;
    }

    __host__ __device__ ~CubeFaces(){
//         printf("SH destructor\n");        
        delete [] cuts;
        delete [] radii;
    }

    
    
    
// // // // // // // // // // // // // // // // // // // // // // 

__host__ __device__  void convert_xyz_to_cube_uv(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z, int &index, FLOAT_TYPE &u, FLOAT_TYPE &v){
  FLOAT_TYPE absX = fabs(x);
  FLOAT_TYPE absY = fabs(y);
  FLOAT_TYPE absZ = fabs(z);
  
  int isXPositive = x > 0 ? 1 : 0;
  int isYPositive = y > 0 ? 1 : 0;
  int isZPositive = z > 0 ? 1 : 0;
  
  FLOAT_TYPE maxAxis, uc, vc;
  
  // POSITIVE X
  if (isXPositive && absX >= absY && absX >= absZ) {

    maxAxis = absX;
    uc = y;
    vc = z;
    index = 0;
  }
  // NEGATIVE X
  if (!isXPositive && absX >= absY && absX >= absZ) {

    maxAxis = absX;
    uc = -y;
    vc = z;
    index = 2;
  }
  // POSITIVE Y
  if (isYPositive && absY >= absX && absY >= absZ) {

    maxAxis = absY;
    uc = -x;
    vc = z;
    index = 1;
  }
  // NEGATIVE Y
  if (!isYPositive && absY >= absX && absY >= absZ) {

    maxAxis = absY;
    uc = x;
    vc = z;
    index = 3;
  }
  // POSITIVE Z
  if (isZPositive && absZ >= absX && absZ >= absY) {

    maxAxis = absZ;
    uc = y;
    vc = -x;
    index = 5;
  }
  // NEGATIVE Z
  if (!isZPositive && absZ >= absX && absZ >= absY) {

    maxAxis = absZ;
    uc = y;
    vc = x;
    index = 4;
  }

  // Convert range from -1 to 1 to 0 to 1
  u = 0.5f * (uc / maxAxis + 1.0f);
  v = 0.5f * (vc / maxAxis + 1.0f);
}
// // // // // // // // // // // // // // // // // // // 

__host__ __device__  void convert_cube_index_to_xyz(int iface, int u, int v, FLOAT_TYPE& x, FLOAT_TYPE& y, FLOAT_TYPE& z, FLOAT_TYPE start, FLOAT_TYPE step){
  FLOAT_TYPE coords[3];
  coords[0]=start+step/2.+step*FLOAT_TYPE(u);
  coords[1]=start+step/2.+step*FLOAT_TYPE(v);
  coords[2]=1.;
  
  if(iface==0){
      x=coords[2];
      y=coords[0];
      z=-coords[1];
  }
  else if(iface==1){
      x=-coords[0];
      y=coords[2];
      z=-coords[1];
  }
  else if(iface==2){
      x=-coords[2];
      y=-coords[0];
      z=-coords[1];
  }  
  else if(iface==3){
      x=coords[0];
      y=-coords[2];
      z=-coords[1];
  }   
  else if(iface==4){
      x=coords[1];
      y=coords[0];
      z=coords[2];
  }       
  else if(iface==5){
      x=-coords[1];
      y=coords[0];
      z=-coords[2];
  }    

}


   __host__ __device__ void get_cut_value(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz, FLOAT_TYPE& radius){
       
    
    FLOAT_TYPE r=SQRT(xx*xx+yy*yy+zz*zz);
    FLOAT_TYPE rmin=-1;
    FLOAT_TYPE sideextension= 2.;
    FLOAT_TYPE step= sideextension/FLOAT_TYPE(cutsize);
    
    for(int iface=0; iface<6; iface++)
        for(int v=0; v<cutsize; v++)
            for(int u=0; u<cutsize; u++)
                if(cuts[cutsize*cutsize*iface + v*cutsize+u]>0){
                FLOAT_TYPE x,y,z;
                convert_cube_index_to_xyz(iface, u,v,x,y,z, -sideextension/2., step);
                FLOAT_TYPE facer=SQRT(x*x+y*y+z*z);
                FLOAT_TYPE cosang=(x*xx+y*yy+z*zz)/(r*facer);
                if(cosang<=1 && cosang>1.e-4){
                    FLOAT_TYPE rval=cuts[cutsize*cutsize*iface + v*cutsize+u]/cosang;
                    if(rmin>rval or rmin<0) rmin=rval;
                    }
            }
    
       

    radius=rmin;
    
//     printf("r=%f, grid=%f\n", r, cuts[iphi+nphi*itheta]);

    } 


    void init(){
        
    FLOAT_TYPE step= 2./FLOAT_TYPE(facesize);
    FLOAT_TYPE sideextension= step*FLOAT_TYPE(sidesize);

    maxDim=0;
    
    for(int iface=0; iface<6; iface++)
        for(int v=0; v<sidesize; v++)
            for(int u=0; u<sidesize; u++){
                FLOAT_TYPE x,y,z;
                convert_cube_index_to_xyz(iface, u,v,x,y,z, -sideextension/2., step);
                get_cut_value(x,y,z, radii[sidesize*sidesize*iface + v*sidesize+u]);
                if(radii[sidesize*sidesize*iface + v*sidesize+u]>maxDim) 
                    maxDim=radii[sidesize*sidesize*iface + v*sidesize+u];
            }    
       
    maxDim*=2.;
//     printf("%f\n", maxDim);
    is_init=true;
                
    }



// // // // // // // // // // // // // // // // // // // 

__host__ __device__ FLOAT_TYPE cubicInterpolate (FLOAT_TYPE p[4], FLOAT_TYPE x) {
	return p[1] + 0.5 * x*(p[2] - p[0] + x*(2.0*p[0] - 5.0*p[1] + 4.0*p[2] - p[3] + x*(3.0*(p[1] - p[2]) + p[3] - p[0])));
}

__host__ __device__ FLOAT_TYPE bicubicInterpolate (FLOAT_TYPE p[4][4], FLOAT_TYPE x, FLOAT_TYPE y) {
	FLOAT_TYPE arr[4];
	arr[0] = cubicInterpolate(p[0], x);
	arr[1] = cubicInterpolate(p[1], x);
	arr[2] = cubicInterpolate(p[2], x);
	arr[3] = cubicInterpolate(p[3], x);
	return cubicInterpolate(arr, y);
}


__host__ __device__ FLOAT_TYPE linearInterpolate (FLOAT_TYPE p[2], FLOAT_TYPE x) {
	return (1.f-x)*p[0] + x*p[1];
}

__host__ __device__ FLOAT_TYPE bilinearInterpolate (FLOAT_TYPE p[2][2], FLOAT_TYPE x, FLOAT_TYPE y) {
	FLOAT_TYPE arr[2];
	arr[0] = linearInterpolate(p[0], x);
	arr[1] = linearInterpolate(p[1], x);
	return linearInterpolate(arr, y);
}



// // // // // // // // // // // // // // // // // // // 
   __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
       
    int iface;
    FLOAT_TYPE u,v;
    

    
    convert_xyz_to_cube_uv(xx,yy,zz, iface,  u, v);
    
    FLOAT_TYPE* face=&radii[sidesize*sidesize*iface];
   
    u=u*FLOAT_TYPE(facesize)+1.5;
    v=v*FLOAT_TYPE(facesize)+1.5;
    int ui=int(std::floor(u));
    int vi=int(std::floor(v));
    
    FLOAT_TYPE val=0;
    if(cubic_int==1){
        FLOAT_TYPE points[4][4];
        
        for(int j=0; j<4; j++)
            for(int i=0; i<4; i++){
                points[j][i]=face[(ui-1+i)    +sidesize*(vi-1+j) ];
                
            }

        FLOAT_TYPE udec= u-FLOAT_TYPE(ui);
        FLOAT_TYPE vdec= v-FLOAT_TYPE(vi);
        val = bicubicInterpolate (points, udec, vdec);
    }
    else{
        FLOAT_TYPE points[2][2];
        
        for(int j=0; j<2; j++)
            for(int i=0; i<2; i++){
                points[j][i]=face[(ui+i)    +sidesize*(vi+j) ];
                
            }

        FLOAT_TYPE udec= u-FLOAT_TYPE(ui);
        FLOAT_TYPE vdec= v-FLOAT_TYPE(vi);
        val = bilinearInterpolate (points, udec, vdec);        
        
        
        
    }
        
        
        
    FLOAT_TYPE r=SQRT(xx*xx+yy*yy+zz*zz);

    if(r<=ABS(val)) return 1;
    else return 0;
    
//     printf("r=%f, grid=%f\n", r, radii[iphi+nphi*itheta]);

    }
    
    
    __host__ __device__ void get_refractive_index(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz, FLOAT_TYPE (&value)[2]){
        FLOAT_TYPE dens_val = get_density(xx, yy, zz);
        if(dens_val>0.5){
            value[0] = 1.-get_delta(xx,yy,zz);
            value[1] = get_beta(xx,yy,zz);
           }
        else{
            value[0] = 1.;
            value[1] = 0.;
           }
        }  
    
    virtual size_t mysize(){return sizeof(CubeFaces);}
    virtual int mytype(){return shape_type::CubeFaces;};
    virtual std::string get_name(){return std::string("CubeFaces");};
    
    FLOAT_TYPE a;
    
    FLOAT_TYPE* cuts;
    FLOAT_TYPE* radii;

    FLOAT_TYPE sidepixsize;
    int cutsize;
    int facesize;
    int sidesize;
    bool is_init;
    bool cubic_int;


};

};

#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif

