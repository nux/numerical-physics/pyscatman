#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>

#include "grid.h"
#include "shapes/radial_map.h"


#define NTHETA 180
#define NPHI 360

#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif


#ifdef SINGLE_PRECISION
    #define ATAN2 atan2f
    #define COS   cosf
    #define SIN     sinf
    #define SQRT  sqrtf
    #define ABS fabsf
    #define FLOOR floorf
#else
    #define ATAN2 atan2
    #define COS   cos
    #define SIN     sin
    #define SQRT  sqrt
    #define ABS fabs
    #define FLOOR floor

#endif

// int factorial(int n)
// {
//   return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
// }

namespace Shapes{

class SphericalHarmonics: public RadialMap{
public:
    __host__ __device__ SphericalHarmonics(): RadialMap(){}
    __host__ SphericalHarmonics(FLOAT_TYPE a_, FLOAT_TYPE* coefficients_,  int ncoefficients_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_,
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_=0, FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0 ): 
              ncoefficients(ncoefficients_),
              RadialMap(a_, nullptr, NTHETA, NPHI, delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_) {

            coefficients = new FLOAT_TYPE[ncoefficients];
          
            for(int i=0; i<ncoefficients; i++) coefficients[i]=coefficients_[i];

            }
    
            
        
    __host__ __device__ SphericalHarmonics(const SphericalHarmonics& in): Shapes::RadialMap(in){

        ncoefficients=in.ncoefficients;
        coefficients= new FLOAT_TYPE[ncoefficients];

    }

    __host__ __device__ ~SphericalHarmonics(){

        delete [] coefficients;
    }

    __host__ void init();
    
  
    
    size_t mysize(){return sizeof(SphericalHarmonics);}
    int mytype(){return shape_type::SphericalHarmonics;};
    std::string get_name(){return std::string("SphericalHarmonics");};
    

    
    FLOAT_TYPE* coefficients;
    int ncoefficients;



};

};

#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif

