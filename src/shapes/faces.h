#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>

#include "grid.h"
#include "shapes/shape.h"


#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif


#ifdef SINGLE_PRECISION
    #define ATAN2 atan2f
    #define COS   cosf
    #define SIN     sinf
    #define SQRT  sqrtf
    #define ABS fabsf
    #define FLOOR floorf
#else
    #define ATAN2 atan2
    #define COS   cos
    #define SIN     sin
    #define SQRT  sqrt
    #define ABS fabs
    #define FLOOR floor

#endif

// int factorial(int n)
// {
//   return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
// }

namespace Shapes{

class Faces: public Shape{
public:
    __host__ __device__ Faces(): Shape(){}
    __host__ Faces(FLOAT_TYPE* cuts_, int cutsize_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_,
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_=0, FLOAT_TYPE max_radius_=-1, int central_symmetry_=0, FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0 ): 
              Shape(delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_),
                cutsize(cutsize_), max_radius(max_radius_){
      
            if(central_symmetry_==0){
                cuts = new FLOAT_TYPE[cutsize*3];
                
                buffer = (void*) cuts;
                buffer_size = cutsize*3*sizeof(FLOAT_TYPE);
                
                
                for(int i=0; i<cutsize*3;i++){
                    cuts[i]=cuts_[i];
                }
            }
            
            
            else{
                cutsize*=2;
                cuts = new FLOAT_TYPE[cutsize*3];
                
                buffer = (void*) cuts;
                buffer_size = cutsize*3*sizeof(FLOAT_TYPE);
                
                
                for(int i=0; i<(cutsize/2)*3;i++){
                    cuts[i]=cuts_[i];
                }               
                for(int i=0; i<(cutsize/2)*3;i++){
                    cuts[(cutsize/2)*3+i]=-cuts_[i];
                }                    
                
            }  
            
      }
    
            
        
    __host__ __device__ Faces(const Faces& in): Shapes::Shape(in){
        a=in.a;
        maxDim=in.maxDim;
        cutsize=in.cutsize;

        is_init=0;
        cuts= new FLOAT_TYPE[cutsize*cutsize*6];
        buffer = (void*) cuts;
        buffer_size = in.buffer_size;
    }

    __host__ __device__ ~Faces(){
//         printf("SH destructor\n");        
        delete [] cuts;
    }

    
    

   __host__ __device__ FLOAT_TYPE check_cut_value(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
       
    
    FLOAT_TYPE r=SQRT(xx*xx+yy*yy+zz*zz);

    if(r>maxDim/2.)  return 0.;
    if(r<min_radius) return 1.;
    
    
    for(int ivec=0; ivec<cutsize; ivec++){
                FLOAT_TYPE x=cuts[ivec*3];
                FLOAT_TYPE y=cuts[ivec*3+1];
                FLOAT_TYPE z=cuts[ivec*3+2];
                FLOAT_TYPE facer=SQRT(x*x+y*y+z*z);
                FLOAT_TYPE cosang=(x*xx+y*yy+z*zz)/(r*facer);
                if(cosang<=1 && cosang>1.e-4){
                    if(r>facer/cosang) return 0;
                    }
            }
    
       
    return 1.;
       
//     printf("r=%f, grid=%f\n", r, cuts[iphi+nphi*itheta]);

    } 


    void init(){
        
        if(max_radius>0){
            maxDim=2.*max_radius;
        }
        
        else{
            maxDim=0;
            for(int ivec=0; ivec<cutsize; ivec++){
                FLOAT_TYPE x=cuts[ivec*3];
                FLOAT_TYPE y=cuts[ivec*3+1];
                FLOAT_TYPE z=cuts[ivec*3+2];
                FLOAT_TYPE facer=SQRT(x*x+y*y+z*z);
                if(facer>maxDim) maxDim=facer;
        }
        maxDim*=3.;
        }
        
        
        min_radius=maxDim/2.;
        
        for(int ivec=0; ivec<cutsize; ivec++){
                FLOAT_TYPE x=cuts[ivec*3];
                FLOAT_TYPE y=cuts[ivec*3+1];
                FLOAT_TYPE z=cuts[ivec*3+2];
                FLOAT_TYPE facer=SQRT(x*x+y*y+z*z);
                if(facer<min_radius) min_radius=facer;
        }
        
    
//     printf("%f\n", maxDim);
    is_init=true;
                
    }



// // // // // // // // // // // // // // // // // // // 
   __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
       
    return check_cut_value(xx,yy,zz);
        
    }
    
    
    __host__ __device__ void get_refractive_index(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz, FLOAT_TYPE (&value)[2]){
        FLOAT_TYPE dens_val = get_density(xx, yy, zz);
        if(dens_val>0.5){
            value[0] = 1.-get_delta(xx,yy,zz);
            value[1] = get_beta(xx,yy,zz);
           }
        else{
            value[0] = 1.;
            value[1] = 0.;
           }
        }  
    
    virtual size_t mysize(){return sizeof(Faces);}
    virtual int mytype(){return shape_type::Faces;};
    virtual std::string get_name(){return std::string("Faces");};
    
    FLOAT_TYPE a;
    
    FLOAT_TYPE* cuts;

    
    FLOAT_TYPE max_radius;
    FLOAT_TYPE min_radius;

    int cutsize;

    bool is_init;
    bool cubic_int;


};

};

#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif

