#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>

#include "grid.h"
#include "shapes/shape.h"

#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif

namespace Shapes{

class MoonSphere: public Shape{
public:

    __host__ __device__  MoonSphere(FLOAT_TYPE a_, FLOAT_TYPE a_shell_, FLOAT_TYPE transition_width_, FLOAT_TYPE delta_core_, FLOAT_TYPE beta_core_, FLOAT_TYPE delta_shell_, FLOAT_TYPE beta_shell_, FLOAT_TYPE beam_position_=0, FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0 ): Shape(delta_core_, beta_core_, 90, 0, 0, beam_position_, xshift_, yshift_),
                a(a_), a_shell(a_shell_), 
                delta_shell(delta_shell_), beta_shell(beta_shell_), transition_width(transition_width_){
                    maxDim = 2.*a;     
                }


    __host__ __device__  FLOAT_TYPE get_density(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
            if(xx*xx+ yy*yy + zz*zz <=a*a)
                return 1.;
            else 
                return 0;
        }
    
    __host__ __device__  FLOAT_TYPE fermi(FLOAT_TYPE x, FLOAT_TYPE C, FLOAT_TYPE D, FLOAT_TYPE min, FLOAT_TYPE max){
        return (max-min)/(std::exp((x-C)/D*8.)+1.)+min;
    } 
        
    __host__ __device__  FLOAT_TYPE get_delta(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
        FLOAT_TYPE shifted_z=zz-a_shell;
        if(transition_width<=0){
            if(xx*xx+ yy*yy + shifted_z*shifted_z <=a*a)
                return delta;
            else 
                return delta_shell;
           }
        else{
            FLOAT_TYPE radius=std::sqrt(xx*xx+ yy*yy + shifted_z*shifted_z);
            return fermi(radius, a, transition_width, delta_shell, delta);
        }
        }
        
    
    __host__ __device__  FLOAT_TYPE get_beta(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
        FLOAT_TYPE shifted_z=zz-a_shell;
        if(transition_width<=0){
            if(xx*xx+ yy*yy + shifted_z*shifted_z <=a*a)
                return beta;
            else 
                return beta_shell;
           }
        else{
            FLOAT_TYPE radius=std::sqrt(xx*xx+ yy*yy + shifted_z*shifted_z);
            return fermi(radius, a, transition_width, beta_shell, beta);
        }
        }  
        
    __host__ __device__ void get_refractive_index(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz, FLOAT_TYPE (&value)[2]){
        FLOAT_TYPE dens_val = get_density(xx, yy, zz);
        if(dens_val>0.5){
            value[0] = 1.-get_delta(xx,yy,zz);
            value[1] = get_beta(xx,yy,zz);
           }
        else{
            value[0] = 1.;
            value[1] = 0.;
           }
        }  

    size_t mysize(){return sizeof(MoonSphere);}
    int mytype(){return shape_type::MoonSphere;};
    std::string get_name(){return std::string("MoonSphere");};

    FLOAT_TYPE a;
    FLOAT_TYPE a_shell;
    FLOAT_TYPE beta_shell;
    FLOAT_TYPE delta_shell;
    FLOAT_TYPE transition_width;


};

};


