#pragma once

#include <vector>
// #include <cmath>
#include <math.h>
#include <string>
#include <fstream>
#include <stdio.h>

#include "grid.h"
#include "shapes/shape.h"


#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif

#ifdef SINGLE_PRECISION
    #define ATAN2 atan2f
    #define COS   cosf
    #define SIN     sinf
#else
    #define ATAN2 atan2
    #define COS   cos
    #define SIN     sin
#endif
namespace Shapes{

class Dumbbell: public Shape{
public:
    __host__ __device__ Dumbbell(): Shape(){}
    __host__ __device__ Dumbbell(FLOAT_TYPE longAxis_ , FLOAT_TYPE a_, FLOAT_TYPE b_, FLOAT_TYPE c_, FLOAT_TYPE z0_, 
                FLOAT_TYPE delta_, FLOAT_TYPE beta_,
                FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_=0, FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0 ): Shape(delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_),
                longAxis(longAxis_), a(a_), b(b_), c(c_), z0(z0_){
                    
         maxDim = longAxis;
         if(b>maxDim) maxDim=b;
         if(c>maxDim) maxDim=c;   
         
         maxDim*=2.;
         
         z_m1 = longAxis - a;
         z_m2 = -z_m1;
         me_c = -c/(a*a) * (z0-z_m1)/(std::sqrt(1.-(z0-z_m1)*(z0-z_m1)/(a*a)));
         me_b = -b/(a*a) * (z0-z_m1)/(std::sqrt(1.-(z0-z_m1)*(z0-z_m1)/(a*a)));
         zec = c*std::sqrt(1.-(z0-z_m1)*(z0-z_m1)/(a*a));
         zeb = b*std::sqrt(1.-(z0-z_m1)*(z0-z_m1)/(a*a));

         c1_c = me_c/(2.*z0);
         c1_b = me_b/(2.*z0);
         dip_c = zec-c1_c*z0*z0;
         dip_b = zeb-c1_b*z0*z0;
                    
                }
                

    __host__ __device__ Dumbbell(const Dumbbell& in){
        latitude=in.latitude;
        longitude=in.longitude;
        orientation=in.orientation;
        maxDim=in.maxDim;
        delta=in.delta;
        beta=in.beta;       
        
        longAxis=in.longAxis;
        a=in.a;
        b=in.b;
        c=in.c;
        z0=in.z0;

        z_m1=in.z_m1;
        z_m2=in.z_m2;
        me_c=in.me_c;
        me_b=in.me_b;
        zec=in.zec;
        zeb=in.zeb;

        c1_c=in.c1_c;
        c1_b=in.c1_b;
        dip_c=in.dip_c;
        dip_b=in.dip_b;
        
        
        
        
    }
    
//     int draw(std::vector<FLOAT_TYPE>& data, uint npixel, FLOAT_TYPE& dx, FLOAT_TYPE& volume){
//     
// 
//     }
    
    __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
            FLOAT_TYPE cc=zz*zz*c1_c + dip_c;
            FLOAT_TYPE bb=zz*zz*c1_b + dip_b;
            
            FLOAT_TYPE phi=ATAN2(yy,xx);
            
            FLOAT_TYPE xc = cc*COS(phi);
            FLOAT_TYPE yc = bb*SIN(phi);
            

//             printf("%f\t%f\t%f\n", z0-z_m1, a, phi);
            
            FLOAT_TYPE rE1 = xx*xx/(c*c)+yy*yy/(b*b)+(zz-z_m1)*(zz-z_m1)/(a*a);
            FLOAT_TYPE rE2 = xx*xx/(c*c)+yy*yy/(b*b)+(zz-z_m2)*(zz-z_m2)/(a*a);

            FLOAT_TYPE densC1=((zz<z0 && zz>-z0) ? 1.: 0);
            FLOAT_TYPE densC2=((xx*xx+yy*yy<=xc*xc+yc*yc) ? 1.: 0);
//             FLOAT_TYPE densC2=((cc*cc<bb*bb) ? 1.: 0);

            
            FLOAT_TYPE densE1=((zz >= z0) ? 1.: 0);
            FLOAT_TYPE densE2=((zz <= -z0) ? 1.: 0);
            FLOAT_TYPE densE3=((rE1<=1) ? 1.: 0);
            FLOAT_TYPE densE4=((rE2<=1) ? 1.: 0);

            FLOAT_TYPE dens = densC1*densC2 + densE1*densE3 + densE2*densE4;
//             FLOAT_TYPE dens = densE4;// + densE2*densE4;

            return (dens < 1 ? dens : 1.);        
    }
    
    
__host__ __device__ void get_refractive_index(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz, FLOAT_TYPE (&value)[2]){
            FLOAT_TYPE cc=zz*zz*c1_c + dip_c;
            FLOAT_TYPE bb=zz*zz*c1_b + dip_b;
            
            FLOAT_TYPE phi=ATAN2(yy,xx);
            
            FLOAT_TYPE xc = cc*COS(phi);
            FLOAT_TYPE yc = bb*SIN(phi);
            

//             printf("%f\t%f\t%f\n", z0-z_m1, a, phi);
            
            FLOAT_TYPE rE1 = xx*xx/(c*c)+yy*yy/(b*b)+(zz-z_m1)*(zz-z_m1)/(a*a);
            FLOAT_TYPE rE2 = xx*xx/(c*c)+yy*yy/(b*b)+(zz-z_m2)*(zz-z_m2)/(a*a);

            FLOAT_TYPE densC1=((zz<z0 && zz>-z0) ? 1.: 0);
            FLOAT_TYPE densC2=((xx*xx+yy*yy<=xc*xc+yc*yc) ? 1.: 0);
//             FLOAT_TYPE densC2=((cc*cc<bb*bb) ? 1.: 0);

            
            FLOAT_TYPE densE1=((zz >= z0) ? 1.: 0);
            FLOAT_TYPE densE2=((zz <= -z0) ? 1.: 0);
            FLOAT_TYPE densE3=((rE1<=1) ? 1.: 0);
            FLOAT_TYPE densE4=((rE2<=1) ? 1.: 0);

            FLOAT_TYPE dens = densC1*densC2 + densE1*densE3 + densE2*densE4;
//             FLOAT_TYPE dens = densE4;// + densE2*densE4;

            FLOAT_TYPE dens_value = (dens < 1 ? 0 : 1.);
            
            
            if(dens_value>0.5){
                value[0] = 1.-delta;
                value[1] = beta;
            }
            else{
                value[0] = 1.;
                value[1] = 0.;
            }
            
            return ;        
    }
    
    size_t mysize(){return sizeof(Dumbbell);}
    int mytype(){return shape_type::Dumbbell;};
    std::string get_name(){return std::string("Dumbbell");};

    
    FLOAT_TYPE longAxis;
    FLOAT_TYPE a;
    FLOAT_TYPE b;
    FLOAT_TYPE c;
    FLOAT_TYPE z0;

    FLOAT_TYPE z_m1;
    FLOAT_TYPE z_m2;
    FLOAT_TYPE me_c;
    FLOAT_TYPE me_b;
    FLOAT_TYPE zec;
    FLOAT_TYPE zeb;

    FLOAT_TYPE c1_c;
    FLOAT_TYPE c1_b;
    FLOAT_TYPE dip_c;
    FLOAT_TYPE dip_b;
    
    

};

};


#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif
