#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>

#include "grid.h"
#include "shapes/shape.h"


#ifndef WITH_NVCC
#define __host__ 
#define __device__ 
#endif


#ifdef SINGLE_PRECISION
    #define ATAN2 atan2f
    #define COS   cosf
    #define SIN     sinf
    #define SQRT  sqrtf
    #define ABS fabsf
    #define FLOOR floorf
#else
    #define ATAN2 atan2
    #define COS   cos
    #define SIN     sin
    #define SQRT  sqrt
    #define ABS fabs
    #define FLOOR floor

#endif

// int factorial(int n)
// {
//   return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
// }

namespace Shapes{

class CubeMap: public Shape{
public:
    __host__ __device__ CubeMap(): Shape(){}
    __host__ CubeMap(FLOAT_TYPE a_, FLOAT_TYPE* radii_, int inputsize_,
              FLOAT_TYPE delta_, FLOAT_TYPE beta_,FLOAT_TYPE z_scaling_,
              FLOAT_TYPE latitude_, FLOAT_TYPE longitude_, FLOAT_TYPE orientation_, FLOAT_TYPE beam_position_=0, bool cubic_int_=1, bool tan_adjust_=0, FLOAT_TYPE xshift_=0,FLOAT_TYPE yshift_=0 ): 
              Shape(delta_, beta_, latitude_, longitude_, orientation_, beam_position_, xshift_, yshift_),
                a(a_), inputsize(inputsize_), sidesize(inputsize_+4), z_scaling(z_scaling_), cubic_int(cubic_int_), tan_adjust(tan_adjust_){

            radii = new FLOAT_TYPE[sidesize*sidesize*6];
            input_radii = new FLOAT_TYPE[inputsize*inputsize*6];
            
            buffer = (void*) radii;
            buffer_size = sidesize*sidesize*6*sizeof(FLOAT_TYPE);
            
            maxDim=0;
            
            FLOAT_TYPE maxval=0;
            for(int i=0; i<inputsize*inputsize*6;i++){
                input_radii[i]=radii_[i];
                if(input_radii[i]>maxval) maxval=input_radii[i];
            }
            

            if(a<=0) maxDim=2.*maxval;
            
            else{
                FLOAT_TYPE norm=a/maxval;
            
                for(int i=0; i<inputsize*inputsize*6; i++){
                    input_radii[i]*=norm;
                }
                maxDim = 2.*a;
            }
      }
    
            
        
    __host__ __device__ CubeMap(const CubeMap& in): Shapes::Shape(in){
        a=in.a;
        maxDim=in.maxDim;
        inputsize=in.inputsize;
        sidesize=in.sidesize;
        z_scaling=in.z_scaling;
        cubic_int=in.cubic_int;
        radii= new FLOAT_TYPE[sidesize*sidesize*6];
        input_radii= new FLOAT_TYPE[inputsize*inputsize*6];
        buffer = (void*) radii;
        buffer_size = in.buffer_size;

    }

    __host__ __device__ ~CubeMap(){
//         printf("SH destructor\n");        
        delete [] radii; 
        delete [] input_radii;
    }

    
    void init(){
        
        FLOAT_TYPE* input_faces[6];
        FLOAT_TYPE* faces[6];
        FLOAT_TYPE* sides[6][4];
        
        for(int iface=0; iface<6; iface++){
            input_faces[iface]=&input_radii[inputsize*inputsize*iface];
            faces[iface]=&radii[sidesize*sidesize*iface];

            for(int y=0; y< inputsize; y++)
                for(int x=0; x< inputsize; x++){
                    faces[iface][sidesize*(y+2)+x+2]= input_faces[iface][y*inputsize+x];
                }
            // top=0; bottom=1; left=2; right=3
            for(int iside=0; iside<4;iside++)
                sides[iface][iside]=new FLOAT_TYPE[2*inputsize];
            
            for(int x=0; x< inputsize; x++){
                sides[iface][0][x+inputsize]=input_faces[iface][x];
                sides[iface][0][x]=input_faces[iface][x+inputsize];
                sides[iface][1][x]=input_faces[iface][x+(inputsize)*(inputsize-2)];
                sides[iface][1][x+inputsize]=input_faces[iface][x+(inputsize)*(inputsize-1)];
            }
            for(int y=0; y< inputsize; y++){
                sides[iface][2][y+inputsize]=input_faces[iface][y*inputsize];
                sides[iface][2][y]=input_faces[iface][(y)*inputsize+1];
                sides[iface][3][y]=input_faces[iface][y*inputsize+inputsize-2];
                sides[iface][3][y+inputsize]=input_faces[iface][y*inputsize+inputsize-1];

                
            }

            
            
        }
        
        
        FLOAT_TYPE* neighbors_sides[6][4];
        int versus[6][4];
        
        neighbors_sides[0][0]=sides[4][1]; versus[0][0] = 1;
        neighbors_sides[0][1]=sides[5][0]; versus[0][1] = 1;
        neighbors_sides[0][2]=sides[3][3]; versus[0][2] = 1;
        neighbors_sides[0][3]=sides[1][2]; versus[0][3] = 1;

        neighbors_sides[1][0]=sides[4][3]; versus[1][0] = -1;
        neighbors_sides[1][1]=sides[5][3]; versus[1][1] = 1;
        neighbors_sides[1][2]=sides[0][3]; versus[1][2] = 1;
        neighbors_sides[1][3]=sides[2][2]; versus[1][3] = 1;
        
        neighbors_sides[2][0]=sides[4][0]; versus[2][0] = -1;
        neighbors_sides[2][1]=sides[5][1]; versus[2][1] = -1;
        neighbors_sides[2][2]=sides[1][3]; versus[2][2] = 1;
        neighbors_sides[2][3]=sides[3][2]; versus[2][3] = 1;

        neighbors_sides[3][0]=sides[4][2]; versus[3][0] = 1;
        neighbors_sides[3][1]=sides[5][2]; versus[3][1] = -1;
        neighbors_sides[3][2]=sides[2][3]; versus[3][2] = 1;
        neighbors_sides[3][3]=sides[0][2]; versus[3][3] = 1;
    
        neighbors_sides[4][0]=sides[2][0]; versus[4][0] = -1;
        neighbors_sides[4][1]=sides[0][0]; versus[4][1] = 1;
        neighbors_sides[4][2]=sides[3][0]; versus[4][2] = 1;
        neighbors_sides[4][3]=sides[1][0]; versus[4][3] = -1;   

        neighbors_sides[5][0]=sides[0][1]; versus[5][0] = 1;
        neighbors_sides[5][1]=sides[2][1]; versus[5][1] = -1;
        neighbors_sides[5][2]=sides[3][1]; versus[5][2] = -1;
        neighbors_sides[5][3]=sides[1][1]; versus[5][3] = 1;
        
        
        
        for(int iface=0; iface<6;iface++){
            
                                  

            int offset;
            offset=(versus[iface][0]>0 ? 0 : inputsize-1);
            for(int x=0; x< inputsize; x++){
                faces[iface][x+2]=neighbors_sides[iface][0][offset+versus[iface][0]*x];
                faces[iface][x+2+sidesize]=neighbors_sides[iface][0][offset+versus[iface][0]*x+inputsize];
                
            }
            
            offset=(versus[iface][1]>0 ? 0 : inputsize-1);            
            for(int x=0; x< inputsize; x++){
                faces[iface][x+2 + (sidesize)*(sidesize-1)] = neighbors_sides[iface][1][offset+versus[iface][1]*x];
                faces[iface][x+2 + (sidesize)*(sidesize-2)] = neighbors_sides[iface][1][offset+versus[iface][1]*x+inputsize];                
            }

            offset=(versus[iface][2]>0 ? 0 : inputsize-1);
            for(int y=0; y< inputsize; y++){
                faces[iface][(y+2)*sidesize]=neighbors_sides[iface][2][offset+versus[iface][2]*y];
                faces[iface][(y+2)*sidesize+1]=neighbors_sides[iface][2][offset+versus[iface][2]*y+inputsize];                
            }

            offset=(versus[iface][3]>0 ? 0 : inputsize-1);
            for(int y=0; y< inputsize; y++){
                faces[iface][(y+2)*sidesize + (sidesize-1)] = neighbors_sides[iface][3][offset+versus[iface][3]*y];
                faces[iface][(y+2)*sidesize + (sidesize-2)] = neighbors_sides[iface][3][offset+versus[iface][3]*y+inputsize];

                
            }
            
            faces[iface][0]                     =0.5*(faces[iface][2]+ faces[iface][2*sidesize]);
            faces[iface][sidesize-1]            =0.5*(faces[iface][sidesize-3] + faces[iface][3*sidesize-1]);
            faces[iface][sidesize*(sidesize-1)] =0.5*(faces[iface][sidesize*(sidesize-3)] + faces[iface][sidesize*(sidesize-1)+2]);
            faces[iface][sidesize*sidesize-1]   =0.5*(faces[iface][sidesize*(sidesize-2)-1] + faces[iface][sidesize*sidesize-3]);           
            
            faces[iface][sidesize +1]             =0.5*(faces[iface][sidesize+2]+ faces[iface][2*sidesize+1]);
            faces[iface][2*sidesize -2]           =0.5*(faces[iface][2*sidesize-3] + faces[iface][3*sidesize-2]);
            faces[iface][sidesize*(sidesize-2)+1] =0.5*(faces[iface][sidesize*(sidesize-3)+1] + faces[iface][sidesize*(sidesize-2)+2]);
            faces[iface][sidesize*(sidesize-1)-2] =0.5*(faces[iface][sidesize*(sidesize-2)-2] + faces[iface][sidesize*(sidesize-1)-3]);  
            
            
            faces[iface][1]                        =0.5*(faces[iface][0]+ faces[iface][2]);
            faces[iface][sidesize-2]               =0.5*(faces[iface][sidesize-1]+ faces[iface][sidesize-3]);
            faces[iface][sidesize*(sidesize-1) + 1]=0.5*(faces[iface][sidesize*(sidesize-1)]+ faces[iface][sidesize*(sidesize-1) + 2]);
            faces[iface][sidesize*sidesize-2]      =0.5*(faces[iface][sidesize*sidesize-1]+ faces[iface][sidesize*sidesize-3]);

            faces[iface][sidesize]                =0.5*(faces[iface][0]+ faces[iface][2*sidesize]);
            faces[iface][sidesize*(sidesize-2)]   =0.5*(faces[iface][sidesize*(sidesize-1)]+ faces[iface][sidesize*(sidesize-3)]);
            faces[iface][2*sidesize-1]            =0.5*(faces[iface][sidesize-1]+ faces[iface][3*sidesize-1]);
            faces[iface][sidesize*(sidesize-1)-1] =0.5*(faces[iface][sidesize*(sidesize)-1]+ faces[iface][sidesize*(sidesize-3)-1]);
            
            
            
            
        }
        
        for(int iface=0; iface<6; iface++)
            for(int iside=0; iside<4;iside++)
                delete [] sides[iface][iside];
                
    }
    
// // // // // // // // // // // // // // // // // // // // // // 

__host__ __device__  void convert_xyz_to_cube_uv(FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE z, int &index, FLOAT_TYPE &u, FLOAT_TYPE &v){
  FLOAT_TYPE absX = fabs(x);
  FLOAT_TYPE absY = fabs(y);
  FLOAT_TYPE absZ = fabs(z);
  
  int isXPositive = x > 0 ? 1 : 0;
  int isYPositive = y > 0 ? 1 : 0;
  int isZPositive = z > 0 ? 1 : 0;
  
  FLOAT_TYPE maxAxis, uc, vc;
  
  // POSITIVE X
  if (isXPositive && absX >= absY && absX >= absZ) {

    maxAxis = absX;
    uc = y;
    vc = z;
    index = 0;
  }
  // NEGATIVE X
  if (!isXPositive && absX >= absY && absX >= absZ) {

    maxAxis = absX;
    uc = -y;
    vc = z;
    index = 2;
  }
  // POSITIVE Y
  if (isYPositive && absY >= absX && absY >= absZ) {

    maxAxis = absY;
    uc = -x;
    vc = z;
    index = 1;
  }
  // NEGATIVE Y
  if (!isYPositive && absY >= absX && absY >= absZ) {

    maxAxis = absY;
    uc = x;
    vc = z;
    index = 3;
  }
  // POSITIVE Z
  if (isZPositive && absZ >= absX && absZ >= absY) {

    maxAxis = absZ;
    uc = y;
    vc = -x;
    index = 5;
  }
  // NEGATIVE Z
  if (!isZPositive && absZ >= absX && absZ >= absY) {

    maxAxis = absZ;
    uc = y;
    vc = x;
    index = 4;
  }

  if(tan_adjust){
    // Do tangent correction
    u = 0.5f * (std::atan(uc / maxAxis)*4.f/M_PIF+ 1.0f);
    v = 0.5f * (std::atan(vc / maxAxis)*4.f/M_PIF+ 1.0f);
  }
  else{
    // Convert range from -1 to 1 to 0 to 1
    u = 0.5f * (uc / maxAxis + 1.0f);
    v = 0.5f * (vc / maxAxis + 1.0f);
}
}
// // // // // // // // // // // // // // // // // // // 

__host__ __device__ FLOAT_TYPE cubicInterpolate (FLOAT_TYPE p[4], FLOAT_TYPE x) {
	return p[1] + 0.5 * x*(p[2] - p[0] + x*(2.0*p[0] - 5.0*p[1] + 4.0*p[2] - p[3] + x*(3.0*(p[1] - p[2]) + p[3] - p[0])));
}

__host__ __device__ FLOAT_TYPE bicubicInterpolate (FLOAT_TYPE p[4][4], FLOAT_TYPE x, FLOAT_TYPE y) {
	FLOAT_TYPE arr[4];
	arr[0] = cubicInterpolate(p[0], x);
	arr[1] = cubicInterpolate(p[1], x);
	arr[2] = cubicInterpolate(p[2], x);
	arr[3] = cubicInterpolate(p[3], x);
	return cubicInterpolate(arr, y);
}


__host__ __device__ FLOAT_TYPE linearInterpolate (FLOAT_TYPE p[2], FLOAT_TYPE x) {
	return (1.f-x)*p[0] + x*p[1];
}

__host__ __device__ FLOAT_TYPE bilinearInterpolate (FLOAT_TYPE p[2][2], FLOAT_TYPE x, FLOAT_TYPE y) {
	FLOAT_TYPE arr[2];
	arr[0] = linearInterpolate(p[0], x);
	arr[1] = linearInterpolate(p[1], x);
	return linearInterpolate(arr, y);
}

// __host__ __device__ FLOAT_TYPE linearInterpolateNew (FLOAT_TYPE p[2], FLOAT_TYPE x, FLOAT_TYPE cosx) {
// 	FLOAT_TYPE l = (1.f-x)*p[0] + x*p[1];
//     FLOAT_TYPE corr = 1.-(2.*p[0]*p[1]*x*(1.-x)*(1.-cosx))/(l*l);
//     return l*std::sqrt(corr);
// }
// 
// __host__ __device__ FLOAT_TYPE bilinearInterpolateNew (FLOAT_TYPE p[2][2], FLOAT_TYPE x, FLOAT_TYPE y, FLOAT_TYPE cosx[3]) {
// 	FLOAT_TYPE arr[2];
// 	arr[0] = linearInterpolateNew(p[0], x, cosx[0]);
// 	arr[1] = linearInterpolateNew(p[1], x, cosx[1]);
// 	return linearInterpolateNew(arr, y, cosx[2]);
// }

// // // // // // // // // // // // // // // // // // // 
   __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
       
    int iface;
    FLOAT_TYPE u,v;
    

    
    convert_xyz_to_cube_uv(xx,yy,zz, iface,  u, v);
    
    FLOAT_TYPE* face=&radii[sidesize*sidesize*iface];
   
    u=u*FLOAT_TYPE(inputsize)+1.5;
    v=v*FLOAT_TYPE(inputsize)+1.5;
    int ui=int(std::floor(u));
    int vi=int(std::floor(v));
    
    FLOAT_TYPE val=0;
    if(cubic_int==1){
        FLOAT_TYPE points[4][4];
        
        for(int j=0; j<4; j++)
            for(int i=0; i<4; i++){
                points[j][i]=face[(ui-1+i)    +sidesize*(vi-1+j) ];
                
            }

        FLOAT_TYPE udec= u-FLOAT_TYPE(ui);
        FLOAT_TYPE vdec= v-FLOAT_TYPE(vi);
        val = bicubicInterpolate (points, udec, vdec);
    }
    else{
        FLOAT_TYPE points[2][2];
        
        for(int j=0; j<2; j++)
            for(int i=0; i<2; i++){
                points[j][i]=face[(ui+i)    +sidesize*(vi+j) ];
                
            }

        FLOAT_TYPE udec= u-FLOAT_TYPE(ui);
        FLOAT_TYPE vdec= v-FLOAT_TYPE(vi);
        val = bilinearInterpolate (points, udec, vdec);        
        
        
        
    }
    /*else{
        FLOAT_TYPE points[2][2];
        
        for(int j=0; j<2; j++)
            for(int i=0; i<2; i++){
                points[j][i]=face[(ui+i)    +sidesize*(vi+j) ];
                
            }

        
        FLOAT_TYPE norms[6];
        FLOAT_TYPE cosx[3];
        
        FLOAT_TYPE vecs[6][3]{{1.*ui-sidesize/2.+0.5,    1.*vi-sidesize/2.+0.5, inputsize/2.},
                              {1.*ui+1.-sidesize/2.+0.5, 1.*vi-sidesize/2.+0.5, inputsize/2.},
                        {1.*ui-sidesize/2.+0.5,    1.*vi+1.-sidesize/2.+0.5, inputsize/2.},
                        {1.*ui+1.-sidesize/2.+0.5, 1.*vi+1.-sidesize/2.+0.5, inputsize/2.},
                        {u-sidesize/2.+0.5, 1.*vi-sidesize/2.+0.5, inputsize/2.},
                        {u-sidesize/2.+0.5, 1.*vi+1.-sidesize/2.+0.5, inputsize/2.}};        
            
        for(int ivec=0; ivec<6; ivec++){
            norms[ivec]=0;
            for(int i=0; i<3; i++) norms[ivec]+=vecs[ivec][i]*vecs[ivec][i];
            norms[ivec]=std::sqrt(norms[ivec]);
        }
        for(int icos=0; icos<3; icos++){
            cosx[icos]=0;
            for(int i=0; i<3; i++) cosx[icos]+=vecs[2*icos][i]*vecs[2*icos+1][i];
            cosx[icos]/=norms[2*icos]*norms[2*icos+1];
        }                
        
            
        FLOAT_TYPE udec= u-FLOAT_TYPE(ui);
        FLOAT_TYPE vdec= v-FLOAT_TYPE(vi);
        val = bilinearInterpolateNew(points, udec, vdec, cosx);        
        
        
        
    }     */   
        
        
    FLOAT_TYPE r=SQRT(xx*xx+yy*yy+zz*zz*z_scaling*z_scaling);

    if(r<=ABS(val)) return 1;
    else return 0;
    
//     printf("r=%f, grid=%f\n", r, radii[iphi+nphi*itheta]);

    }
//    __host__ __device__ FLOAT_TYPE get_density(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz){
//        
//     int iface;
//     FLOAT_TYPE u,v;
//     
// 
//     
//     convert_xyz_to_cube_uv(xx,yy,zz, iface,  u, v);
//     
//     FLOAT_TYPE* face=&radii[sidesize*sidesize*iface];
//    
//     u=u*FLOAT_TYPE(inputsize)+1.5;
//     v=v*FLOAT_TYPE(inputsize)+1.5;
//     int ui=int(u);
//     int vi=int(v);
//     FLOAT_TYPE udec= u-FLOAT_TYPE(ui);
//     FLOAT_TYPE vdec= v-FLOAT_TYPE(vi);
//     
//     FLOAT_TYPE val = face[(ui)    +sidesize*((vi)  ) ]*(1.-udec)*(1.-vdec) +
//                      face[(ui+1)  +sidesize*((vi)  ) ]*(   udec)*(1.-vdec) +
//                      face[(ui)    +sidesize*((vi+1)) ]*(1.-udec)*(   vdec) +
//                      face[(ui+1)  +sidesize*((vi+1)) ]*(   udec)*(   vdec);
//     
//     FLOAT_TYPE r=SQRT(xx*xx+yy*yy+zz*zz);
// 
//     if(r<=ABS(val)) return 1;
//     else return 0;
//     
// //     printf("r=%f, grid=%f\n", r, radii[iphi+nphi*itheta]);
// 
//     }
    
    
    
    __host__ __device__ void get_refractive_index(FLOAT_TYPE xx, FLOAT_TYPE yy, FLOAT_TYPE zz, FLOAT_TYPE (&value)[2]){
        FLOAT_TYPE dens_val = get_density(xx, yy, zz);
        if(dens_val>0.5){
            value[0] = 1.-get_delta(xx,yy,zz);
            value[1] = get_beta(xx,yy,zz);
           }
        else{
            value[0] = 1.;
            value[1] = 0.;
           }
        }  
    
    virtual size_t mysize(){return sizeof(CubeMap);}
    virtual int mytype(){return shape_type::CubeMap;};
    virtual std::string get_name(){return std::string("CubeMap");};
    
    FLOAT_TYPE a;
    
    FLOAT_TYPE* radii;
    FLOAT_TYPE* input_radii;

    int inputsize;
    int sidesize;
    FLOAT_TYPE z_scaling;
    bool cubic_int;
    bool is_init;
    bool tan_adjust;

};

};

#ifndef WITH_CUDA
#undef __host__ 
#undef __device__ 
#endif

