#pragma once

#include <omp.h>


#include "datatypes.h"
#include "detectors/detector.h"
#include "pattern.h"
#include "shapes/shape.h"


namespace Detectors{
    class Detector; 

    std::vector<Pattern> acquire_dataset_gpu(Detectors::Detector& detector, std::vector<Shapes::Shape*>  dataset);
    std::vector<FLOAT_TYPE> compare_dataset_gpu(Detectors::Detector& detector, std::vector<Shapes::Shape*> dataset, FLOAT_TYPE* experiment, int* errormap, FLOAT_TYPE error_norm, FLOAT_TYPE experiment_sum,FLOAT_TYPE error_exponent, FLOAT_TYPE);    
};
