#pragma once
#include <cufft.h>
#include <cuComplex.h>
#include "datatypes.h"
#include <cstdio>
#include <cmath>
#include "grid.h"
#include <omp.h>

#include "shapes/shape.h"
#include "shapes/ellipsoid.h"
#include "shapes/dumbbell.h"
#include "context.h"
#include <memory>


#define BUFFER_SIZE 1024*sizeof(FLOAT_TYPE)
#define SHAPE_BUFFER_SIZE 128*sizeof(FLOAT_TYPE)

typedef FLOAT_TYPE complex_type[2];

extern Context context;



class MSFT_gpu_context{
public:
    MSFT_gpu_context(int nthreads_, int dev_id_, int block_size_x_, int block_size_y_):
        dev_id(dev_id_),
        nthreads(nthreads_),
        streams(nthreads_),
        plan(nthreads_),
        in_dev(nthreads_),
        sum_dev(nthreads_),
        sum_host(nthreads_),
        in_dev_ft(nthreads_),
        buffer_size(BUFFER_SIZE),
        shape_buffer_size(SHAPE_BUFFER_SIZE),
        blockSize(block_size_x_, block_size_y_),
        gridSize((context.output_npixel+block_size_x_-1)/block_size_x_, (context.output_npixel+block_size_y_-1)/block_size_y_),
        blockSize_total(block_size_x_, block_size_y_),
        gridSize_total((context.npixel+block_size_x_-1)/block_size_x_, (context.npixel+block_size_y_-1)/block_size_y_),
        block_size_x(block_size_x_),
        block_size_y(block_size_y_)        
        {
            
        #pragma omp critical
        {
        cudaSetDevice(dev_id);
            
//         for(auto& ss : streams) cudaStreamCreate(&ss);

        for(auto& ss : streams) cudaStreamCreateWithFlags(&ss, cudaStreamNonBlocking);

        
        for(int ithread=0; ithread<nthreads; ithread++){
            if (cufftPlan2d(&plan[ithread], context.npixel, context.npixel, CUFFT_C2C) != CUFFT_SUCCESS){
                fprintf(stderr, "CUFFT Error: Unable to create plan\n");
            }
            cufftSetStream(plan[ithread], streams[ithread]);
        }
           
        cudaMalloc((void**)&out_dev, context.npixel * context.npixel *sizeof(complex_type));
        
        cudaMalloc((void**)&pattern_raw_dev, context.npixel * context.npixel *sizeof(FLOAT_TYPE));
        
        cudaMalloc((void**)&pattern_dev, context.output_npixel * context.output_npixel *sizeof(FLOAT_TYPE));

        cudaMallocHost((void**)&pattern_host, context.output_npixel * context.output_npixel *sizeof(FLOAT_TYPE));
        
        cudaMalloc((void**)&N_dev, context.npixel * context.npixel *sizeof(CUDA_COMPLEX));
        
        if(context.output_npixel!=context.scale_npixel){
            cudaMalloc((void**)&pattern_scale_dev, context.scale_npixel * context.scale_npixel *sizeof(FLOAT_TYPE));
            cudaMalloc((void**)&weights_scale_dev, context.scale_npixel * context.scale_npixel *sizeof(FLOAT_TYPE));            
        }

        for(int i=0; i<nthreads;i++)
            cudaMalloc((void**)&in_dev[i], context.npixel*context.npixel * sizeof(CUDA_COMPLEX));
        
        for(int i=0; i<nthreads;i++)
            cudaMalloc((void**)&sum_dev[i], sizeof(FLOAT_TYPE));
        
        for(int i=0; i<nthreads;i++)
            cudaMallocHost((void**)&sum_host[i], sizeof(FLOAT_TYPE));

        for(int i=0; i<nthreads;i++)
            cudaMalloc((void**)&in_dev_ft[i], context.npixel*context.npixel * sizeof(CUDA_COMPLEX));
              
        
// // // // // // // // // //         
        
        cudaMalloc(&shape, sizeof(Shapes::Shape*));
        cudaMalloc((void**) &shape_dev_temp, shape_buffer_size);
        cudaMallocHost((void**) &shape_host_temp, shape_buffer_size);

        cudaMalloc((void**) &grid_dev, sizeof(Grid));
        cudaMallocHost((void**) &grid_host, sizeof(Grid));

        cudaMalloc(&data_buf_dev, buffer_size);
        cudaMallocHost(&data_buf_host, buffer_size);
        
        
//         printf("Shapes allocated on device\n");
        }
        
    }
    
    void check_buffer(std::size_t required){
        if(required>buffer_size){
            cudaSetDevice(dev_id);
//             printf("Buffer increased from %d to %d\n",buffer_size, required );
            buffer_size=required;
            #pragma omp critical
            {
            cudaFree(data_buf_dev);
            cudaFreeHost(data_buf_host);
            cudaMalloc(&data_buf_dev, buffer_size);
            cudaMallocHost(&data_buf_host, buffer_size);
            }
        }
        
    }
    
    void check_shape_buffer(std::size_t required){
        if(required>shape_buffer_size){
            cudaSetDevice(dev_id);
//             printf("Shape buffer increased from %d to %d\n",shape_buffer_size, required );
            shape_buffer_size=required;
            #pragma omp critical
            {
            cudaFree(shape_dev_temp);
            cudaFreeHost(shape_host_temp);
            cudaMalloc((void**) &shape_dev_temp, shape_buffer_size);
            cudaMallocHost((void**) &shape_host_temp, shape_buffer_size);
            }
        }
        
    }
    
    
    
    ~MSFT_gpu_context(){

        for(auto& ss : plan)  cufftDestroy(ss);
       
        for(auto& ss : in_dev)  cudaFree(ss);

        for(auto& ss : in_dev_ft)  cudaFree(ss);
        
        for(auto& ss : streams) cudaStreamDestroy(ss);
        
        for(auto& ss : sum_dev)  cudaFree(ss);

        for(auto& ss : sum_host)  cudaFree(ss);

        
        cudaFree(out_dev);
        cudaFreeHost(pattern_host);
        cudaFree(pattern_dev);
        cudaFree(pattern_raw_dev);
        cudaFree(shape);
        cudaFree(shape_dev_temp);
        cudaFreeHost(shape_host_temp);

        
        cudaFree(data_buf_dev);
        cudaFreeHost(data_buf_host);
        
        cudaFree(N_dev);

        cudaFree(grid_dev);
        cudaFree(grid_host);

        
        if(context.output_npixel!=context.scale_npixel){
            cudaFree(pattern_scale_dev);
            cudaFree(weights_scale_dev);
        }            
            
//         delete_shape<<<1,1>>>( (Shapes::Ellipsoid**) shapes[0]);
//         delete_shape<<<1,1>>>( (Shapes::Dumbbell**)  shapes[1]);

        
    }
    
    int block_size_x;
    int block_size_y;
    
    std::vector<cudaStream_t> streams;
    std::vector<cufftHandle> plan;
//     std::vector<Shapes::Shape**> shapes;
    void** shape;
    void* shape_dev_temp;
    void* shape_host_temp;
    
    void* data_buf_dev;
    void* data_buf_host;

    
    dim3 blockSize;
    dim3 gridSize;
    dim3 blockSize_total;
    dim3 gridSize_total;
    
    complex_type *out_dev;
    FLOAT_TYPE* pattern_scale_dev;
    FLOAT_TYPE* weights_scale_dev;
    FLOAT_TYPE* sum;

    FLOAT_TYPE* pattern_host;
    FLOAT_TYPE* pattern_dev;
    FLOAT_TYPE* pattern_raw_dev;
    CUDA_COMPLEX *N_dev;
    std::vector<FLOAT_TYPE*> sum_dev;
    std::vector<FLOAT_TYPE*> sum_host;

    std::vector<CUDA_COMPLEX*> in_dev;
    std::vector<CUDA_COMPLEX*> in_dev_ft;
    
    Grid* grid_dev;
    Grid* grid_host;
    
    
    std::size_t buffer_size;
    std::size_t shape_buffer_size;
    
    
    int dev_id;
    int nthreads;

};



