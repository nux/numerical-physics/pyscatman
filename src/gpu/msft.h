#pragma once
#include <cufft.h>
#include <cuComplex.h>
#include <cstdio>
#include <cmath>
#include <omp.h>
#include <memory>

#include "datatypes.h"
#include "grid.h"
#include "context.h"
#include "shapes/shape.h"
#include "shapes/ellipsoid.h"
#include "shapes/dumbbell.h"
#include "shapes/spherical_harmonics.h"
#include "shapes/shell_sphere.h"
#include "shapes/moon_sphere.h"
#include "shapes/radial_map.h"
#include "shapes/volume_map.h"
#include "shapes/volume_distribution.h"
#include "shapes/cube_map.h"
#include "shapes/cube_mesh.h"

#include "shapes/cube_faces.h"
#include "shapes/faces.h"


#include "gpu/msft_context.h"
#include "pattern.h"

typedef FLOAT_TYPE complex_type[2];

extern Context context;

void msft_gpu(Shapes::Shape& shape, Pattern& pattern,   MSFT_gpu_context* context);

void rescale_pattern_gpu(Pattern& pattern, MSFT_gpu_context* gpu_context);
