#include "gpu/msft.h"


// #include <cuda_runtime.h>


// #define OS 5
#define SIGMA FLOAT_TYPE(0.5)
// #define OS_Z 2



#define INTERP_EPSILON 1e-6


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}



__device__ void set_interpolation_gpu(FLOAT_TYPE* data,FLOAT_TYPE* weights, FLOAT_TYPE x, FLOAT_TYPE y, int npixel, FLOAT_TYPE value){
              int ixt = std::floor(x);
              int iyt = std::floor(y);

              FLOAT_TYPE xtdec = x-FLOAT_TYPE(ixt);
              FLOAT_TYPE ytdec = y-FLOAT_TYPE(iyt);
              
                if(ixt>=0 && iyt>=0 && ixt<npixel && iyt<npixel ){
                    FLOAT_TYPE weight = (1.f-xtdec)*(1.f-ytdec);
                    atomicAdd(&data[ixt + iyt*npixel],value*weight);
                    atomicAdd(&weights[ixt + iyt*npixel], weight);
                }
                if(ixt+1>=0 && iyt>=0 && ixt+1<npixel && iyt<npixel ){
                    FLOAT_TYPE weight = (xtdec)*(1.f-ytdec);
                    atomicAdd(&data[ixt + 1 + iyt*npixel],value*weight);
                    atomicAdd(&weights[ixt + 1 + iyt*npixel], weight);
                }
                if(ixt>=0 && iyt+1>=0 && ixt<npixel && iyt+1<npixel ){
                    FLOAT_TYPE weight = (1.f-xtdec)*(ytdec);
                    atomicAdd(&data[ixt + (iyt+1)*npixel],value*weight);
                    atomicAdd(&weights[ixt + (iyt+1)*npixel], weight);
                }
                if(ixt+1>=0 && iyt+1>=0 && ixt+1<npixel && iyt+1<npixel ){
                    FLOAT_TYPE weight = (xtdec)*(ytdec);
                    atomicAdd(&data[ixt + 1 + (iyt+1)*npixel],value*weight);
                    atomicAdd(&weights[ixt + 1 + (iyt+1)*npixel], weight);
                }
    }


__global__ void  rescale_gpu(FLOAT_TYPE* data, int output_npixel, FLOAT_TYPE* dataout, FLOAT_TYPE* weights,  int scale_npixel){
       
        const int x = blockIdx.x * blockDim.x + threadIdx.x;
        const int y = blockIdx.y * blockDim.y + threadIdx.y;
        
        FLOAT_TYPE conv_factor = FLOAT_TYPE(scale_npixel)/FLOAT_TYPE(output_npixel);
        
        if(x<output_npixel && y<output_npixel){
              unsigned int index= x+y*output_npixel;
              FLOAT_TYPE ix = FLOAT_TYPE(x)*conv_factor;
              FLOAT_TYPE iy = FLOAT_TYPE(y)*conv_factor;
              set_interpolation_gpu(dataout, weights, ix, iy, scale_npixel, data[index]);
        }
                
    }


__global__ void  rescale_weight_gpu(FLOAT_TYPE* dataout, FLOAT_TYPE* weights,  int scale_npixel){
        const int x = blockIdx.x * blockDim.x + threadIdx.x;
        const int y = blockIdx.y * blockDim.y + threadIdx.y;
        
        if(x<scale_npixel && y<scale_npixel){
              unsigned int index= x+y*scale_npixel;
              dataout[index]/=weights[index];
        }
                
    }
    
// void rescale_pattern_gpu(Pattern& pattern, MSFT_gpu_context* gpu_context){
//    
//     
//         cudaMemcpyAsync(gpu_context->pattern_dev, &pattern.data[0], 
//                      context.output_npixel*context.output_npixel*sizeof(FLOAT_TYPE),
//                      cudaMemcpyDeviceToHost, gpu_context->streams[0]);
//     
//         cudaMemsetAsync(gpu_context->pattern_scale_dev, 0,context.scale_npixel * context.scale_npixel *sizeof(FLOAT_TYPE), gpu_context->streams[0]);
//         cudaMemsetAsync(gpu_context->weights_scale_dev, 0,context.scale_npixel * context.scale_npixel *sizeof(FLOAT_TYPE), gpu_context->streams[0]);
//         rescale_gpu<<<gpu_context->gridSize, gpu_context->blockSize, 0, gpu_context->streams[0]>>>(gpu_context->pattern_dev, context.output_npixel, gpu_context->pattern_scale_dev, gpu_context->weights_scale_dev, context.scale_npixel);
//         rescale_weight_gpu<<<gpu_context->gridSize, gpu_context->blockSize, 0, gpu_context->streams[0]>>>(gpu_context->pattern_scale_dev, gpu_context->weights_scale_dev, context.scale_npixel);
// 
//         pattern.resize(context.scale_npixel);
//         
//         cudaStreamSynchronize(gpu_context->streams[0]);
//         
//         cudaMemcpyAsync( &pattern.data[0], gpu_context->pattern_scale_dev, 
//                         context.scale_npixel*context.scale_npixel*sizeof(FLOAT_TYPE),
//                         cudaMemcpyDeviceToHost, gpu_context->streams[0]);
//         
//         
// }


__device__
FLOAT_TYPE get_interpolation(FLOAT_TYPE* data, FLOAT_TYPE x, FLOAT_TYPE y, int npixel){
              int ixt = std::floor(x);
              int iyt = std::floor(y);

              FLOAT_TYPE xtdec = x-FLOAT_TYPE(ixt);
              FLOAT_TYPE ytdec = y-FLOAT_TYPE(iyt);
              
              FLOAT_TYPE totweight=0;
              FLOAT_TYPE val =0;
              
                if(ixt>=0 && iyt>=0 && ixt<npixel && iyt<npixel ){
                    FLOAT_TYPE weight = (1.f-xtdec)*(1.f-ytdec);
                    val+= data[ixt + iyt*npixel]*weight;
                    totweight +=  weight;
                }
                if(ixt+1>=0 && iyt>=0 && ixt+1<npixel && iyt<npixel ){
                    FLOAT_TYPE weight = (xtdec)*(1.f-ytdec);
                    val+= data[ixt + 1 + iyt*npixel]*weight;
                    totweight +=  weight;
                }
                if(ixt>=0 && iyt+1>=0 && ixt<npixel && iyt+1<npixel ){
                    FLOAT_TYPE weight = (1.f-xtdec)*(ytdec);
                    val+= data[ixt + (iyt+1) *npixel]*weight;
                    totweight +=  weight;
                }
                if(ixt+1>=0 && iyt+1>=0 && ixt+1<npixel && iyt+1<npixel ){
                    FLOAT_TYPE weight = (xtdec)*(ytdec);
                    val+= data[ixt +1 + (iyt+1)*npixel]*weight;
                    totweight +=  weight;
                }
                
                if(totweight>INTERP_EPSILON){
                    return val/totweight;
                }
                else{
                    return -1;
                }
    }

    
__global__ void  momentum_coordinates(FLOAT_TYPE* data, FLOAT_TYPE* dataout, FLOAT_TYPE K, FLOAT_TYPE dq, FLOAT_TYPE xshift, FLOAT_TYPE yshift, FLOAT_TYPE pixel_size,  int npixel, int output_npixel){
 
        const int x = blockIdx.x * blockDim.x + threadIdx.x;
        const int y = blockIdx.y * blockDim.y + threadIdx.y;

        FLOAT_TYPE dqtotal = pixel_size;
    
        
        if(x<output_npixel && y<output_npixel){
              unsigned int index= x+y*output_npixel;
              FLOAT_TYPE ix = FLOAT_TYPE(x) - output_npixel/2.f - xshift;
              FLOAT_TYPE iy = FLOAT_TYPE(y) - output_npixel/2.f - yshift;
              FLOAT_TYPE qtotal_dist = std::sqrt(ix*ix+iy*iy)*dqtotal;              
              FLOAT_TYPE theta_dist = 2.f * std::asin(qtotal_dist/(2.f*K));              
              FLOAT_TYPE q_dist=K*std::sin(theta_dist);                            
              FLOAT_TYPE q_pixels = q_dist/dq;              
              FLOAT_TYPE phi = std::atan2(iy,ix);                            
              FLOAT_TYPE xt = q_pixels * std::cos(phi);
              FLOAT_TYPE yt = q_pixels * std::sin(phi);                
              xt += FLOAT_TYPE(npixel)/2.f + xshift;
              yt += FLOAT_TYPE(npixel)/2.f + yshift;
              dataout[index]=get_interpolation(data, xt, yt, npixel);
        }

    }

    
__global__ void  angle_coordinates(FLOAT_TYPE* data, FLOAT_TYPE* dataout,  FLOAT_TYPE K, FLOAT_TYPE dq, FLOAT_TYPE xshift, FLOAT_TYPE yshift, FLOAT_TYPE pixel_size, int npixel,  int output_npixel, int intcorrection=0){
        FLOAT_TYPE dtheta=pixel_size/180.f*M_PIF;
        
        const int x = blockIdx.x * blockDim.x + threadIdx.x;
        const int y = blockIdx.y * blockDim.y + threadIdx.y;
        
        if(x<output_npixel && y<output_npixel){
              unsigned int index= x+y*output_npixel;
              FLOAT_TYPE ix = FLOAT_TYPE(x) - output_npixel/2.f - xshift;
              FLOAT_TYPE iy = FLOAT_TYPE(y) - output_npixel/2.f - yshift;
              FLOAT_TYPE theta_dist = std::sqrt(ix*ix+iy*iy)*dtheta;
              FLOAT_TYPE q_dist=K*std::sin(theta_dist);
              FLOAT_TYPE q_pixels = q_dist/dq;
              FLOAT_TYPE phi = std::atan2(iy,ix);                            
              FLOAT_TYPE xt = q_pixels * std::cos(phi);
              FLOAT_TYPE yt = q_pixels * std::sin(phi);                
              xt += FLOAT_TYPE(npixel)/2.f + xshift;
              yt += FLOAT_TYPE(npixel)/2.f + yshift;
              FLOAT_TYPE intscale=1.f;
              if(intcorrection==1){
                  FLOAT_TYPE costheta=std::cos(theta_dist);
                  intscale=costheta*costheta*costheta;
              }
              dataout[index]=intscale*get_interpolation(data, xt, yt, npixel);
        }
                
    }

    
__global__ void  detector_coordinates(FLOAT_TYPE* data, FLOAT_TYPE* dataout,  FLOAT_TYPE K, FLOAT_TYPE dq, FLOAT_TYPE xshift, FLOAT_TYPE yshift, FLOAT_TYPE pixel_size, int npixel,  int output_npixel){

        const int x = blockIdx.x * blockDim.x + threadIdx.x;
        const int y = blockIdx.y * blockDim.y + threadIdx.y;
        
        if(x<output_npixel && y<output_npixel){
              int index= x+y*output_npixel;
              FLOAT_TYPE ix = FLOAT_TYPE(x) - output_npixel/2.f - xshift;
              FLOAT_TYPE iy = FLOAT_TYPE(y) - output_npixel/2.f - yshift;
              FLOAT_TYPE det_dist = std::sqrt(ix*ix+iy*iy)*pixel_size;
              FLOAT_TYPE theta_dist=std::atan(det_dist);
              FLOAT_TYPE q_dist=K*det_dist/std::sqrt(1.f+det_dist*det_dist);
              FLOAT_TYPE q_pixels = q_dist/dq;
              FLOAT_TYPE phi = std::atan2(iy,ix);                            
              FLOAT_TYPE xt = q_pixels * std::cos(phi);
              FLOAT_TYPE yt = q_pixels * std::sin(phi);                
              xt += FLOAT_TYPE(npixel)/2.f + xshift;
              yt += FLOAT_TYPE(npixel)/2.f + yshift;
              FLOAT_TYPE intscale=std::pow(std::cos(theta_dist), 3);
              dataout[index]=intscale*get_interpolation(data, xt, yt, npixel);
        }
                
    }

__global__ void  projection_coordinates(FLOAT_TYPE* data, FLOAT_TYPE* dataout,int npixel,int output_npixel){
            const int x = blockIdx.x * blockDim.x + threadIdx.x;
        const int y = blockIdx.y * blockDim.y + threadIdx.y;
        
        if(x<output_npixel && y<output_npixel){
              int index= x+y*output_npixel;
              int newindex=(x-output_npixel/2+npixel/2)+(y-output_npixel/2+npixel/2)*npixel;
              dataout[index]=data[newindex];
        }
}
    
__global__ void  projection_coordinates(complex_type* data, complex_type* dataout,int npixel,int output_npixel){
            const int x = blockIdx.x * blockDim.x + threadIdx.x;
        const int y = blockIdx.y * blockDim.y + threadIdx.y;
        
        if(x<output_npixel && y<output_npixel){
              int index= x+y*output_npixel;
              int newindex=(x-output_npixel/2+npixel/2)+(y-output_npixel/2+npixel/2)*npixel;
              dataout[index][0]=data[newindex][0];
              dataout[index][1]=data[newindex][1];
        }
}
    
__device__
inline void mult(complex_type& A, complex_type& B, complex_type& C){
    C[0] = A[0]*B[0]-A[1]*B[1];
    C[1] = A[0]*B[1]+A[1]*B[0];
    
}



__global__ void get_pattern(FLOAT_TYPE* pattern, complex_type* out, int npixel){
    int c = blockIdx.x*blockDim.x + threadIdx.x;
    int r = blockIdx.y*blockDim.y + threadIdx.y;
    
    if(c<npixel && r<npixel){
        int index = r * npixel + c; // 1D flat index
        pattern[index]=out[index][0]*out[index][0]+out[index][1]*out[index][1];
    }
}


// __global__ void get_total_absorption(CUDA_COMPLEX* N_cut, int cut_npixel, int npixel, FLOAT_TYPE* in_sum){
// 
//     const int ix = blockIdx.x * blockDim.x + threadIdx.x;
//     const int iy = blockIdx.y * blockDim.y + threadIdx.y;
//     
//     __shared__ FLOAT_TYPE local_sum;
//     if(threadIdx.x ==0 && threadIdx.y==0)
//         local_sum=0;
//     
//     __syncthreads();
// 
//     if(ix<cut_npixel && iy<cut_npixel){
//         int index=ix+iy*npixel;
//         FLOAT_TYPE val= 1.f-exp(2.f*N_cut[index].y);
//         atomicAdd(&local_sum,val);
//     }
//     __syncthreads();
//     if(threadIdx.x ==0 && threadIdx.y==0)
//         atomicAdd(in_sum,local_sum);
// }
    
    
    
__global__ void get_total_scattering(FLOAT_TYPE* pattern, int npixel, FLOAT_TYPE* in_sum){

    const int ix = blockIdx.x * blockDim.x + threadIdx.x;
    const int iy = blockIdx.y * blockDim.y + threadIdx.y;
    
    __shared__ FLOAT_TYPE local_sum;
    if(threadIdx.x ==0 && threadIdx.y==0)
        local_sum=0;
    
    __syncthreads();

    if(ix<npixel && iy<npixel){
        int index=ix+iy*npixel;
        FLOAT_TYPE val= pattern[index];
        atomicAdd(&local_sum,val);
    }
    __syncthreads();
    if(threadIdx.x ==0 && threadIdx.y==0)
        atomicAdd(in_sum,local_sum);
}
    

__global__
void update_output(complex_type* in, complex_type* out, int npixel, FLOAT_TYPE dq, FLOAT_TYPE dx,FLOAT_TYPE dz, FLOAT_TYPE K, FLOAT_TYPE xshift, FLOAT_TYPE yshift, int islice){
    
    const int ix = blockIdx.x * blockDim.x + threadIdx.x;
    const int iy = blockIdx.y * blockDim.y + threadIdx.y;

    if(ix<npixel && iy<npixel){

            int index = ix + npixel*iy;
              
            FLOAT_TYPE dist_q_square=((ix-npixel/2-xshift)*(ix-npixel/2-xshift)
                                 +(iy-npixel/2-yshift)*(iy-npixel/2-yshift))*dq*dq;//std::pow(dq,2);
          
            if(K*K>=dist_q_square){
                FLOAT_TYPE kz = std::sqrt(K*K-dist_q_square);
                FLOAT_TYPE phase_diff = -kz*dz*FLOAT_TYPE(islice);
            
    //             FLOAT_TYPE qz = K*(1.f-std::sqrt(1-dist_q_square/(K*K)));
    //             FLOAT_TYPE phase_diff = qz*dz*FLOAT_TYPE(islice);
                
                complex_type temp;
                complex_type temp_phase={std::cos(phase_diff), std::sin(phase_diff)};
                mult(in[index], temp_phase, temp);
            
                atomicAdd(&out[index][0],temp[0]);
                atomicAdd(&out[index][1],temp[1]);
            }
                       
    }
    
}



__global__ void render_slice(Shapes::Shape ** __restrict__ shape, 
                            Grid* grid, 
                            CUDA_COMPLEX* in_cut,
                            CUDA_COMPLEX* N_cut,
                            int cut_npixel, int npixel, int islice, FLOAT_TYPE dx, FLOAT_TYPE dz, int os, FLOAT_TYPE K, FLOAT_TYPE* in_sum, FLOAT_TYPE xshift, FLOAT_TYPE yshift){
    const int ix = blockIdx.x * blockDim.x + threadIdx.x;
    const int iy = blockIdx.y * blockDim.y + threadIdx.y;
    
    __shared__ FLOAT_TYPE local_sum;
    if(threadIdx.x ==0 && threadIdx.y==0)
        local_sum=0;
    
    __syncthreads();

    if(ix<cut_npixel && iy<cut_npixel){
        int index=ix+iy*npixel;
        FLOAT_TYPE coords[3];
        grid->get_coord(ix, iy, islice, &coords[0]);
        FLOAT_TYPE min[3]={coords[0]-dx*SIGMA, coords[1]-dx*SIGMA,coords[2]-dz*SIGMA};
        FLOAT_TYPE max[3]={coords[0]+dx*SIGMA, coords[1]+dx*SIGMA,coords[2]+dz*SIGMA};
        
        complex_type r_index;

        (*shape)->get_refractive_index_os(min, max, r_index, os);

        in_cut[index].x=((r_index[0]-1)*std::cos(N_cut[index].x) -  
                          r_index[1]   *std::sin(N_cut[index].x))*exp(N_cut[index].y);
        in_cut[index].y=((r_index[0]-1)*std::sin(N_cut[index].x) +
                          r_index[1]   *std::cos(N_cut[index].x))*exp(N_cut[index].y);
        
        N_cut[index].x +=  r_index[0]  * K * dz ;
        N_cut[index].y -= r_index[1] * K * dz;

        atomicAdd(&local_sum,r_index[1]);   
        
        FLOAT_TYPE phase = 2.*M_PI*((xshift/FLOAT_TYPE(npixel)+0.5)*ix + (yshift/FLOAT_TYPE(npixel)+0.5)*iy ); 
        complex_type shift;
        complex_type prod;
        shift[0]=std::cos(phase);
        shift[1]=std::sin(phase);
        
        prod[0]=in_cut[index].x*shift[0]-in_cut[index].y*shift[1];
        prod[1]=in_cut[index].x*shift[1]+in_cut[index].y*shift[0];
        
        in_cut[index].x=prod[0];
        in_cut[index].y=prod[1];
        
        
        
        
        
//         CUDA_COMPLEX potential;
//         potential.x=r_index[0]*r_index[0]-r_index[1]*r_index[1]-1.f;
//         potential.y=2.f*r_index[0]*r_index[1];
// 
//         in_cut[index].x=(potential.x*std::cos(N_cut[index].x)-potential.y*std::sin(N_cut[index].x))*exp(N_cut[index].y);
//         in_cut[index].y=(potential.x*std::sin(N_cut[index].x)+potential.y*std::cos(N_cut[index].x))*exp(N_cut[index].y);
//                 
//         
//         N_cut[index].x +=  r_index[0]  * K * dz ;
//         N_cut[index].y -= r_index[1] * K * dz;
// 
//         atomicAdd(&local_sum,r_index[1]);        
//         
//         FLOAT_TYPE density=(*shape)->get_density_os(min, max, os);
// //         FLOAT_TYPE delta=(*shape)->get_delta(coords[0],coords[1],coords[2]);
// //         FLOAT_TYPE beta=(*shape)->get_beta(coords[0],coords[1],coords[2]);
//         FLOAT_TYPE delta=(*shape)->get_delta_os(min, max, os);
//         FLOAT_TYPE beta=(*shape)->get_beta_os(min, max, os);
//         
// //         if(density!=0) printf("val = %f, delta=%f, beta=%f\n", density, delta, beta);
// 
//         N_cut[index].x += (1.f-delta* density) * K * dz ;
//         N_cut[index].y -= beta * K * dz * density;
//         in_cut[index].x= density*beta*exp(N_cut[index].y)*std::cos(N_cut[index].x);
//         in_cut[index].y= density*beta*exp(N_cut[index].y)*std::sin(N_cut[index].x);
// 
//         atomicAdd(&local_sum,density);
//          atomicAdd(in_sum,density);
    }
    __syncthreads();
    if(threadIdx.x ==0 && threadIdx.y==0)
        atomicAdd(in_sum,local_sum);
}


template<class ST>  
__global__ void copy_shape(ST ** __restrict__ dest, ST* __restrict__ src,   int size){
    const int ix = blockIdx.x * blockDim.x + threadIdx.x;
    const int iy = blockIdx.y * blockDim.y + threadIdx.y;
    if(ix==0 && iy==0){
//         delete *dest;
        *dest=new ST(*src);
    }
}

template<class ST>  
__global__ void copy_buffer(ST ** __restrict__ dest, void* __restrict__ buffer){
    const int ix = blockIdx.x * blockDim.x + threadIdx.x;
    const int iy = blockIdx.y * blockDim.y + threadIdx.y;
    if(ix==0 && iy==0){
//         delete *dest;
        memcpy((**dest).buffer, buffer, (**dest).buffer_size);
    }
}

template<class ST>  
__global__ void copy_radii(ST ** __restrict__ dest, FLOAT_TYPE* __restrict__ radii_dev,    int nphi , int ntheta){
    const int ix = blockIdx.x * blockDim.x + threadIdx.x;
    const int iy = blockIdx.y * blockDim.y + threadIdx.y;
    if(ix<nphi && iy<ntheta){
        int index=ix+nphi*iy;
//         delete *dest;
        (**dest).radii[index]=radii_dev[index];
    }
}

template<class ST>  
__global__ void copy_data(ST ** __restrict__ dest, COMPLEX_TYPE* __restrict__ data_dev,    std::size_t  xdim , std::size_t  ydim, std::size_t  zdim){
    const std::size_t  ix = blockIdx.x * blockDim.x + threadIdx.x;
    const std::size_t  iy = blockIdx.y * blockDim.y + threadIdx.y;
    const std::size_t  iz = blockIdx.z * blockDim.z + threadIdx.z;

    if(ix<xdim && iy<ydim && iz<zdim){
        std::size_t index=ix+xdim*iy+xdim*ydim*iz;
        (**dest).data[index][0]=data_dev[index][0];
        (**dest).data[index][1]=data_dev[index][1];
    }
}

template<class ST>  
__global__ void copy_data(ST ** __restrict__ dest, bool* __restrict__ data_dev,    std::size_t  xdim , std::size_t  ydim, std::size_t  zdim){
    const std::size_t  ix = blockIdx.x * blockDim.x + threadIdx.x;
    const std::size_t  iy = blockIdx.y * blockDim.y + threadIdx.y;
    const std::size_t  iz = blockIdx.z * blockDim.z + threadIdx.z;

    if(ix<xdim && iy<ydim && iz<zdim){
        std::size_t index=ix+xdim*iy+xdim*ydim*iz;
        (**dest).data[index]=data_dev[index];
    }
}


template<class ST>  __global__  void delete_shape(ST** shape){
    const int ix = blockIdx.x * blockDim.x + threadIdx.x;
    const int iy = blockIdx.y * blockDim.y + threadIdx.y;
    if(ix==0 && iy==0){
        delete (*shape);
    }
}

// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

void msft_gpu(Shapes::Shape& shape_in, Pattern& pattern, MSFT_gpu_context* gpu_context){
    cudaSetDevice(gpu_context->dev_id);
    int shape_size=shape_in.mysize();

    gpu_context->check_shape_buffer(shape_size);
//     printf("%d\n", shape_size);
    
    Shapes::Shape* shape=(Shapes::Shape*) gpu_context->shape_host_temp;    
    memcpy (shape, &shape_in, shape_size);
    
    
    
    

    FLOAT_TYPE xshift=shape->xshift;
    FLOAT_TYPE yshift=shape->yshift;

    int tight_npixel=std::ceil(shape->maxDim/context.dx);
    int cut_npixel=tight_npixel+4;  
    
    if(cut_npixel>context.npixel/2) cut_npixel=context.npixel/2;

    
    FLOAT_TYPE gridsize=FLOAT_TYPE(cut_npixel)*context.dx;
    int nslices = std::ceil(gridsize/context.dz);

    
//     Grid grid({cut_npixel,cut_npixel,nslices}, 
//                     {-gridsize/FLOAT_TYPE(2),-gridsize/FLOAT_TYPE(2),-gridsize/FLOAT_TYPE(2)},
//                     { context.dx, context.dx, context.dz}, shape.latitude, shape.longitude, shape.orientation);
    Grid *grid = new (gpu_context->grid_host) Grid({cut_npixel,cut_npixel,nslices}, 
                    {-gridsize/FLOAT_TYPE(2),-gridsize/FLOAT_TYPE(2),-gridsize/FLOAT_TYPE(2)},
                    { context.dx, context.dx, context.dz}, shape->latitude, shape->longitude, shape->orientation);

// // // // // // // // // // // // // // // // // d
    Shapes::Shape** shape_dev;
    Shapes::Shape *shape_dev_temp;


    
    
//     cudaMalloc((void**) &shape_dev_temp, shape_size);
    shape_dev=(Shapes::Shape**) gpu_context->shape;
    shape_dev_temp=(Shapes::Shape*) gpu_context->shape_dev_temp;

    cudaMemsetAsync(gpu_context->N_dev, 0,context.npixel*context.npixel *sizeof(complex_type), gpu_context->streams[0]);    

    cudaMemcpyAsync( gpu_context->grid_dev, gpu_context->grid_host, sizeof(Grid),
                     cudaMemcpyHostToDevice, gpu_context->streams[0]);
    cudaMemcpyAsync( shape_dev_temp, gpu_context->shape_host_temp, shape_size, 
                     cudaMemcpyHostToDevice, gpu_context->streams[0]);

/*               
    std::size_t size;
    cudaDeviceGetLimit(&size, cudaLimitMallocHeapSize);
    printf("Max size: %f MB\n", FLOAT_TYPE(size)/1000000.);*/
    
    
    for(int ithread=0; ithread<gpu_context->nthreads; ithread++)
        cudaMemsetAsync(gpu_context->in_dev[ithread], 0,context.npixel*context.npixel * sizeof(CUDA_COMPLEX),gpu_context->streams[0]);
 
   
    switch(shape->mytype()){
        case shape_type::Ellipsoid:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::Ellipsoid**) shape_dev, (Shapes::Ellipsoid*) shape_dev_temp, shape_size);    
//             printf("Ellipsoid, size %d\n", shape_size);
            break;
        case shape_type::Dumbbell:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::Dumbbell**) shape_dev, (Shapes::Dumbbell*) shape_dev_temp, shape_size);
            break;
        case shape_type::ShellSphere:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::ShellSphere**) shape_dev, (Shapes::ShellSphere*) shape_dev_temp, shape_size);
            break;
        case shape_type::MoonSphere:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::MoonSphere**) shape_dev, (Shapes::MoonSphere*) shape_dev_temp, shape_size);
            break;
        case shape_type::VolumeMap:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::VolumeMap**) shape_dev, (Shapes::VolumeMap*) shape_dev_temp, shape_size);
            break;
        case shape_type::VolumeDistribution:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::VolumeDistribution**) shape_dev, (Shapes::VolumeDistribution*) shape_dev_temp, shape_size);
            break;    
        case shape_type::CubeFaces:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::CubeFaces**) shape_dev, (Shapes::CubeFaces*) shape_dev_temp, shape_size);
            break; 
        case shape_type::Faces:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::Faces**) shape_dev, (Shapes::Faces*) shape_dev_temp, shape_size);
            break;  
        case shape_type::CubeMap:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::CubeMap**) shape_dev, (Shapes::CubeMap*) shape_dev_temp, shape_size);
            break;            
        case shape_type::CubeMesh:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::CubeMesh**) shape_dev, (Shapes::CubeMesh*) shape_dev_temp, shape_size);
            break;  
        default:
            copy_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::RadialMap**) shape_dev, (Shapes::RadialMap*) shape_dev_temp, shape_size);
            break;
    }
            
    if(shape->buffer_size>0){
        gpu_context->check_buffer(shape->buffer_size);
        memcpy (gpu_context->data_buf_host, shape->buffer, shape->buffer_size);
        cudaMemcpyAsync( gpu_context->data_buf_dev, gpu_context->data_buf_host, shape->buffer_size, 
                    cudaMemcpyHostToDevice, gpu_context->streams[0]);
        copy_buffer<<<1,1,0,gpu_context->streams[0]>>>(shape_dev, gpu_context->data_buf_dev);
    }
        
//     printf("Shape init done\n");

    dim3 blockSize_cut(gpu_context->block_size_x, gpu_context->block_size_y);
    dim3 gridSize_cut((cut_npixel+gpu_context->block_size_x-1)/gpu_context->block_size_x, (cut_npixel+gpu_context->block_size_y-1)/gpu_context->block_size_y);

// // // // // // // // // // // // //   

    cudaMemsetAsync(gpu_context->out_dev, 0,context.npixel * context.npixel *sizeof(complex_type), gpu_context->streams[0]);

    
//     cudaStreamSynchronize(gpu_context->streams[0]);
   
   
// // // // // // // // // // // //     
    
//     
//     int masterid = omp_get_thread_num();        
//     int locid;
//     cudaGetDevice(&locid);
    
//     printf("running on GPU %d from thread %d\n",locid, masterid);


    
    
for(int islice =0; islice<nslices; ++islice){
// #pragma omp task depend(out: in_cut_dev[islice]) depend(inout:N_cut_dev)
//     {
//         printf("-----%d\t\r",islice);
        int tid = 0;        
//         cudaMemsetAsync(gpu_context->in_dev[tid], 0,gpu_context->npixel*gpu_context->npixel * sizeof(CUDA_COMPLEX),gpu_context->streams[tid]);
        cudaMemsetAsync(gpu_context->sum_dev[0], 0,sizeof(FLOAT_TYPE), gpu_context->streams[0]);

        render_slice<<<gridSize_cut, blockSize_cut,0,gpu_context->streams[0]>>>
                    (shape_dev, gpu_context->grid_dev, gpu_context->in_dev[tid], gpu_context->N_dev, cut_npixel, context.npixel,
                    islice,context.dx,context.dz, context.os, context.K ,gpu_context->sum_dev[0], xshift, yshift);

        cudaMemcpyAsync( gpu_context->sum_host[0], gpu_context->sum_dev[0], sizeof(FLOAT_TYPE),
                    cudaMemcpyDeviceToHost, gpu_context->streams[0]);
        
        cudaStreamSynchronize(gpu_context->streams[0]);

        if(*gpu_context->sum_host[0]>0){
            
            if (cufftExecC2C(gpu_context->plan[tid], gpu_context->in_dev[tid], gpu_context->in_dev_ft[tid], CUFFT_FORWARD)!= CUFFT_SUCCESS){
                fprintf(stderr, "CUFFT Error: Unable to execute plan\n");
    //             return;	
            }  
    
            update_output<<<gpu_context->gridSize_total, gpu_context->blockSize_total,0,gpu_context->streams[tid]>>>((complex_type*) gpu_context->in_dev_ft[tid], gpu_context->out_dev, context.npixel,  context.dq, context.dx, context.dz, context.K, xshift, yshift, islice);
        }
//             
    } // parallel for
//     } // end omp task
//     cudaSetDevice(gpu_context->dev_id);

    for(auto& ss : gpu_context->streams) cudaStreamSynchronize(ss);

    
    get_pattern<<<gpu_context->gridSize_total, gpu_context->blockSize_total, 0, gpu_context->streams[0]>>>(gpu_context->pattern_raw_dev, gpu_context->out_dev, context.npixel);
    
            
        ////////Get total get_total_absorption
    
    cudaMemsetAsync(gpu_context->sum_dev[0], 0,sizeof(FLOAT_TYPE), gpu_context->streams[0]);

    get_total_scattering<<<gpu_context->gridSize_total,gpu_context->blockSize_total,0,gpu_context->streams[0]>>>
                (gpu_context->pattern_raw_dev, context.npixel,  gpu_context->sum_dev[0]);

    cudaMemcpyAsync( gpu_context->sum_host[0], gpu_context->sum_dev[0], sizeof(FLOAT_TYPE),
                cudaMemcpyDeviceToHost, gpu_context->streams[0]);
    
    cudaStreamSynchronize(gpu_context->streams[0]);

    FLOAT_TYPE total_scattering=*gpu_context->sum_host[0];
    
    total_scattering*=context.dx*context.dx/(context.npixel*context.npixel);
    FLOAT_TYPE in_sum=context.npixel*context.npixel;
    FLOAT_TYPE total_photons=std::pow(context.dx*context.npixel,2)*context.photon_density;
    pattern.scattered_photons=total_scattering/in_sum*total_photons;
//     printf("In: %e, Scattered: %e, Ratio: %e, In photons: %e, Scatt. photons: %e\n",in_sum, total_scattering, total_scattering/in_sum, total_photons, pattern.scattered_photons);   

    
//     pattern.scattered_photons=total_absorption*context.photon_density*std::exp(-0.5*shape.beam_position*shape.beam_position);
    
    // // // // // // // // //     d
    
    if(pattern.coordinates=="angle"){
//         printf("Changing coordinates to angle\n");
        angle_coordinates<<<gpu_context->gridSize, gpu_context->blockSize, 0, gpu_context->streams[0]>>>(gpu_context->pattern_raw_dev, gpu_context->pattern_dev, context.K, context.dq, xshift, yshift, pattern.pixel_size,context.npixel,context.output_npixel);
    }    
    
    else if(pattern.coordinates=="angle-flat"){
//         printf("Changing coordinates to angle\n");
        angle_coordinates<<<gpu_context->gridSize, gpu_context->blockSize, 0, gpu_context->streams[0]>>>(gpu_context->pattern_raw_dev, gpu_context->pattern_dev, context.K, context.dq, xshift, yshift, pattern.pixel_size,context.npixel,context.output_npixel, 1);
    }
    else if(pattern.coordinates=="momentum"){
//         printf("Changing coordinates to momentum\n");
        momentum_coordinates<<<gpu_context->gridSize, gpu_context->blockSize, 0, gpu_context->streams[0]>>>(gpu_context->pattern_raw_dev, gpu_context->pattern_dev, context.K, context.dq, xshift, yshift, pattern.pixel_size, context.npixel,context.output_npixel);
    }
    else if(pattern.coordinates=="detector"){
//         printf("Changing coordinates to momentum\n");
        detector_coordinates<<<gpu_context->gridSize, gpu_context->blockSize, 0, gpu_context->streams[0]>>>(gpu_context->pattern_raw_dev, gpu_context->pattern_dev, context.K, context.dq, xshift, yshift, pattern.pixel_size, context.npixel,context.output_npixel);
    }   

    else{
        projection_coordinates<<<gpu_context->gridSize, gpu_context->blockSize, 0, gpu_context->streams[0]>>>(gpu_context->pattern_raw_dev, gpu_context->pattern_dev, context.npixel,context.output_npixel);
        if(pattern.save_field==1){
            complex_type *out_cut;
            cudaMalloc((void**)&out_cut, context.output_npixel * context.output_npixel *sizeof(complex_type));
            projection_coordinates<<<gpu_context->gridSize, gpu_context->blockSize, 0, gpu_context->streams[0]>>>(gpu_context->out_dev, out_cut, context.npixel,context.output_npixel);

            cudaMemcpyAsync( &pattern.field[0], out_cut, 
                     context.output_npixel*context.output_npixel*sizeof(complex_type),
                     cudaMemcpyDeviceToHost, gpu_context->streams[0]); 
            cudaStreamSynchronize(gpu_context->streams[0]);

            cudaFree(out_cut);
        }

    }
    
    
    cudaStreamSynchronize(gpu_context->streams[0]);

    
    if(context.output_npixel>context.scale_npixel){
    
        cudaMemsetAsync(gpu_context->pattern_scale_dev, 0,context.scale_npixel * context.scale_npixel *sizeof(FLOAT_TYPE), gpu_context->streams[0]);
        cudaMemsetAsync(gpu_context->weights_scale_dev, 0,context.scale_npixel * context.scale_npixel *sizeof(FLOAT_TYPE), gpu_context->streams[0]);
        rescale_gpu<<<gpu_context->gridSize, gpu_context->blockSize, 0, gpu_context->streams[0]>>>(gpu_context->pattern_dev, context.output_npixel, gpu_context->pattern_scale_dev, gpu_context->weights_scale_dev, context.scale_npixel);
        rescale_weight_gpu<<<gpu_context->gridSize, gpu_context->blockSize, 0, gpu_context->streams[0]>>>(gpu_context->pattern_scale_dev, gpu_context->weights_scale_dev, context.scale_npixel);

        pattern.resize(context.scale_npixel);
        
        cudaStreamSynchronize(gpu_context->streams[0]);
        
        cudaMemcpyAsync( gpu_context->pattern_host, gpu_context->pattern_scale_dev, 
                        context.scale_npixel*context.scale_npixel*sizeof(FLOAT_TYPE),
                        cudaMemcpyDeviceToHost, gpu_context->streams[0]);
        cudaStreamSynchronize(gpu_context->streams[0]);

        memcpy(&pattern.data[0], gpu_context->pattern_host, context.scale_npixel*context.scale_npixel*sizeof(FLOAT_TYPE));

        
        
    }
    else{
    
        cudaMemcpyAsync( gpu_context->pattern_host, gpu_context->pattern_dev, 
                        context.output_npixel*context.output_npixel*sizeof(FLOAT_TYPE),
                        cudaMemcpyDeviceToHost, gpu_context->streams[0]);
        cudaStreamSynchronize(gpu_context->streams[0]);

        memcpy(&pattern.data[0], gpu_context->pattern_host, context.output_npixel*context.output_npixel*sizeof(FLOAT_TYPE));
    }

    
    delete_shape<<<1, 1,0,gpu_context->streams[0]>>>(shape_dev);
    
//     switch(shape->mytype()){
//         case shape_type::Ellipsoid:
//             delete_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::Ellipsoid**) shape_dev);
//             break;
//         case shape_type::Dumbbell:
//             delete_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::Dumbbell**) shape_dev);
//             break;
//         default:
//             delete_shape<<<1, 1,0,gpu_context->streams[0]>>>((Shapes::RadialMap**) shape_dev);
//             break;
//             }
    
//     cudaStreamSynchronize(gpu_context->streams[0]);
    

//     cudaFree(grid_dev);
//     cudaFree(shape_dev);
    

   
}




// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 





