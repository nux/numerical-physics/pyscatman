
#include "gpu/acquire.h"
#include "gpu/msft.h"
#include "context.h"

extern Context context;

namespace Detectors{
    

std::vector<Pattern> acquire_dataset_gpu(Detectors::Detector& detector, std::vector<Shapes::Shape*> dataset, bool save_field=0){    
        
        context.init_gpu();
        int nshapes=dataset.size();
        std::vector<Pattern> pattern_list(nshapes, Pattern(context.output_npixel, detector.coordinates, detector.pixel_size, save_field));
        
        std::vector<int> countlist(context.gpu_list.size(), 0);
        int totcounts=0;
        auto print_counts=[&](){
            for(int idev=0; idev<context.gpu_list.size(); idev++)
                printf("\t\t\t");
            printf("\r");
            printf("%.1f%%\t", float(totcounts)/float(nshapes)*100.);
            for(int idev=0; idev<context.gpu_list.size(); idev++){
                printf("\tGPU %d: %.1f%%", context.gpu_list[idev], float(countlist[idev])/float(totcounts)*100.);
            }
            printf("\r");
        };
        if(context.verbose) printf("\n");
        
        int local_gpu_threads=context.nthreads_gpu;
        int inner_threads=1;
        
        if(nshapes+1<context.nthreads_gpu){
            local_gpu_threads=nshapes+1;
//             inner_threads=context.nthreads;
        }
        
        
        if(context.verbose) fputs("\e[?25l", stdout);
        #pragma omp parallel num_threads(local_gpu_threads)
        {                    
            #pragma omp single
            {
                for(int ishape=0; ishape<nshapes; ishape++){
                    #pragma omp task
                    {
                        int tid = omp_get_thread_num();
//                         printf("Shape %d, Thread %d\n", ishape, tid);
//                         printf("Shape %d with thread %d on gpu %d on stream %d\n", ishape, tid, gpu_context_list[tid]->dev_id, (long) gpu_context_list[tid]->streams[0]);
//                         dataset[ishape]->init_gpu(context.gpu_context_list[tid]);
                        dataset[ishape]->init();

                        bool check_size = (int(std::ceil(dataset[ishape]->maxDim/context.dx))<=int(context.output_npixel-4)); 
                        if(!check_size){
                            FLOAT_TYPE maxsize = FLOAT_TYPE(context.output_npixel-4)*context.dx;
                            printf("WARNING: Shape %d (%s) with size %.2f exceeds maximum allowed size (%.2f) for the given experimental parameters. \n", ishape,dataset[ishape]->get_name().c_str(), dataset[ishape]->maxDim, FLOAT_TYPE(context.output_npixel/2)*context.dx);
                        }
//                         else{
                        
                            
                            msft_gpu(*dataset[ishape],pattern_list[ishape], context.gpu_context_list[tid]);
                            
                            #pragma omp atomic update
                            countlist[tid%context.gpu_list.size()]++;
                            #pragma omp atomic update
                            totcounts++;
                            
                            if(context.verbose){
                                #pragma omp critical(print_c)
                                print_counts();
                            }

                            detector.apply_features(pattern_list[ishape]);         
                            if(detector.mask_radius>0) detector.apply_mask(pattern_list[ishape]);
                            
//                             if(context.output_npixel!=context.scale_npixel)
//                                 rescale_pattern_gpu(pattern_list[ishape], context.gpu_context_list[tid]);
                            
                                
//                         }
                    }
                }
            }

            #pragma omp taskwait
        }
        
        /*
#pragma omp parallel for schedule(dynamic) num_threads(local_gpu_threads)
                for(int ishape=0; ishape<nshapes; ishape++){
                        int tid = omp_get_thread_num();

//                         printf("Shape %d, Thread %d\n, GPU %d\n", ishape, tid, context.gpu_context_list[tid]->dev_id);
//                         printf("Shape %d with thread %d on gpu %d on stream %d\n", ishape, tid, gpu_context_list[tid]->dev_id, (long) gpu_context_list[tid]->streams[0]);
//                         dataset[ishape]->init_gpu(context.gpu_context_list[tid]);
                        dataset[ishape]->init();

                        bool check_size = (int(std::ceil(dataset[ishape]->maxDim/context.dx))<=int(context.output_npixel-4)); 
                        if(!check_size){
                            printf("WARNING: Shape %d (%s) with size %.2f exceeds maximum allowed size (%.2f) for the given experimental parameters. Diffraction pattern set to -1\n", ishape,dataset[ishape]->get_name().c_str(), dataset[ishape]->maxDim, FLOAT_TYPE(context.output_npixel/2)*context.dx);
                        }
                        else{
                        
                            
                            msft_gpu(*dataset[ishape],pattern_list[ishape], context.gpu_context_list[tid]);
                            
                            #pragma omp atomic update
                            countlist[tid%context.gpu_list.size()]++;
                            #pragma omp atomic update
                            totcounts++;
                            
                            if(context.verbose){
                                #pragma omp critical(print_c)
                                print_counts();
                            }

                            detector.apply_features(pattern_list[ishape]);         
                            if(detector.mask_radius>0) detector.apply_mask(pattern_list[ishape]);
                            
//                             if(context.output_npixel!=context.scale_npixel)
//                                 rescale_pattern_gpu(pattern_list[ishape], context.gpu_context_list[tid]);
                            
                                
                        }
                }
            */

        if(context.verbose) fputs("\e[?25h", stdout); 
        if(context.verbose) printf("\n");      
        return std::move(pattern_list);
}


// // // // // // // // // // // // // // // // // // // // // 



std::vector<FLOAT_TYPE> compare_dataset_gpu(Detectors::Detector& detector, std::vector<Shapes::Shape*> dataset, FLOAT_TYPE* error_pattern, int* errormap,  FLOAT_TYPE error_norm,FLOAT_TYPE experiment_sum, FLOAT_TYPE error_exponent, FLOAT_TYPE error_metric){    
    
        context.init_gpu();
        int nshapes=dataset.size();
//         std::vector<Pattern> pattern_list(nshapes, Pattern(context.output_npixel, detector.coordinates, detector.pixel_size));
        std::vector<FLOAT_TYPE> errors(nshapes);
        
        int local_gpu_threads=context.nthreads_gpu;
        int inner_threads=1;
        
        if(nshapes+1<context.nthreads_gpu){
//             local_gpu_threads=nshapes+1;
//             inner_threads=context.nthreads;
            local_gpu_threads=nshapes;

            
        }
        
        
        #pragma omp parallel for schedule(dynamic) num_threads(local_gpu_threads)
        for(int ishape=0; ishape<nshapes; ishape++){
                        Pattern temppattern(context.output_npixel, detector.coordinates, detector.pixel_size);
                        int tid = omp_get_thread_num();
                        dataset[ishape]->init();

                        bool check_size = (int(std::ceil(dataset[ishape]->maxDim/context.dx))<=int(context.output_npixel-4)); 
                        if(!check_size){
                            FLOAT_TYPE maxsize = FLOAT_TYPE(context.output_npixel-4)*context.dx;
                            printf("WARNING: Shape %d (%s) with size %.2f exceeds maximum allowed size (%.2f) for the given experimental parameters. \n", ishape,dataset[ishape]->get_name().c_str(), dataset[ishape]->maxDim, FLOAT_TYPE(context.output_npixel/2)*context.dx);
                        }
                        
                        msft_gpu(*dataset[ishape],temppattern, context.gpu_context_list[tid]);
//                             
                        errors[ishape] = temppattern.get_error(error_pattern, errormap, error_norm, experiment_sum, (*dataset[ishape]).scaling, (*dataset[ishape]).offset, error_exponent, error_metric);

        }
        
        
/*
        #pragma omp parallel num_threads(local_gpu_threads)
        {                    
            #pragma omp single
            {
                for(int ishape=0; ishape<nshapes; ishape++){
                    #pragma omp task
                    {
                        int tid = omp_get_thread_num();
                        dataset[ishape]->init();

                        bool check_size = (int(std::ceil(dataset[ishape]->maxDim/context.dx))<=int(context.output_npixel-4)); 
                        if(!check_size){
                            FLOAT_TYPE maxsize = FLOAT_TYPE(context.output_npixel-4)*context.dx;
                            printf("WARNING: Shape %d (%s) with size %.2f exceeds maximum allowed size (%.2f) for the given experimental parameters. \n", ishape,dataset[ishape]->get_name().c_str(), dataset[ishape]->maxDim, FLOAT_TYPE(context.output_npixel/2)*context.dx);
                        }
//                         else{

                            msft_gpu(*dataset[ishape],pattern_list[ishape], context.gpu_context_list[tid]);
//                             
                            errors[ishape] = pattern_list[ishape].get_error(experiment, errormap, (*dataset[ishape]).scaling, (*dataset[ishape]).offset, error_exponent);

//                         }
                    }
                }
            }

            #pragma omp taskwait
        }*/
             
        return std::move(errors);
}
    
};



