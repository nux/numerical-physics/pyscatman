#include "cpu/msft.h"

// #define OS 5
#define SIGMA FLOAT_TYPE(0.5) //0.5*sqrt(2.)
// #define OS_Z 2

#define INTERP_EPSILON 1e-6

FLOAT_TYPE get_interpolation(std::vector<FLOAT_TYPE>& data, FLOAT_TYPE x, FLOAT_TYPE y, int npixel){
              int ixt = std::floor(x);
              int iyt = std::floor(y);

              FLOAT_TYPE xtdec = x-FLOAT_TYPE(ixt);
              FLOAT_TYPE ytdec = y-FLOAT_TYPE(iyt);
              
              FLOAT_TYPE totweight=0;
              FLOAT_TYPE val =0;
              
                if(ixt>=0 && iyt>=0 && ixt<npixel && iyt<npixel ){
                    FLOAT_TYPE weight = (1.-xtdec)*(1.-ytdec);
                    val+= data[ixt + iyt*npixel]*weight;
                    totweight +=  weight;
                }
                if(ixt+1>=0 && iyt>=0 && ixt+1<npixel && iyt<npixel ){
                    FLOAT_TYPE weight = (xtdec)*(1.-ytdec);
                    val+= data[ixt + 1 + iyt*npixel]*weight;
                    totweight +=  weight;
                }
                if(ixt>=0 && iyt+1>=0 && ixt<npixel && iyt+1<npixel ){
                    FLOAT_TYPE weight = (1.-xtdec)*(ytdec);
                    val+= data[ixt + (iyt+1) *npixel]*weight;
                    totweight +=  weight;
                }
                if(ixt+1>=0 && iyt+1>=0 && ixt+1<npixel && iyt+1<npixel ){
                    FLOAT_TYPE weight = (xtdec)*(ytdec);
                    val+= data[ixt +1 + (iyt+1)*npixel]*weight;
                    totweight +=  weight;
                }
                
                if(totweight>INTERP_EPSILON){
                    return val/totweight;
                }
                else{
                    return -1;
                }
    }


    
    std::vector<FLOAT_TYPE>  momentum_coordinates(std::vector<FLOAT_TYPE> data, FLOAT_TYPE K, FLOAT_TYPE dq, FLOAT_TYPE xshift, FLOAT_TYPE yshift, FLOAT_TYPE pixel_size, int npixel, int output_npixel, int theadspershape=1){
        std::vector<FLOAT_TYPE> tempdata(output_npixel*output_npixel,0);

        FLOAT_TYPE dqtotal=pixel_size;
        
        #pragma omp parallel for schedule(dynamic) num_threads(theadspershape)
        for(int y = 0; y<output_npixel; y++)
          for( int x = 0; x<output_npixel; x++){
              int index= x+y*output_npixel;
              FLOAT_TYPE ix = FLOAT_TYPE(x) - output_npixel/2. -xshift;
              FLOAT_TYPE iy = FLOAT_TYPE(y) - output_npixel/2. -yshift;
              FLOAT_TYPE qtotal_dist = std::sqrt(ix*ix+iy*iy)*dqtotal;              
              FLOAT_TYPE theta_dist = 2. * std::asin(qtotal_dist/(2.*K));              
              FLOAT_TYPE q_dist=K*std::sin(theta_dist);                            
              FLOAT_TYPE q_pixels = q_dist/dq;              
              FLOAT_TYPE phi = std::atan2(iy,ix);                            
              FLOAT_TYPE xt = q_pixels * std::cos(phi);
              FLOAT_TYPE yt = q_pixels * std::sin(phi);                
              xt += FLOAT_TYPE(npixel)/2. + xshift;
              yt += FLOAT_TYPE(npixel)/2. + yshift;
              tempdata[index]=get_interpolation(data, xt, yt, npixel);
        }
        
        return tempdata;       
        
    }


    std::vector<FLOAT_TYPE>  detector_coordinates(std::vector<FLOAT_TYPE> data, FLOAT_TYPE K, FLOAT_TYPE dq, FLOAT_TYPE xshift, FLOAT_TYPE yshift, FLOAT_TYPE pixel_size, int npixel,int output_npixel, int theadspershape=1){
        std::vector<FLOAT_TYPE> tempdata(output_npixel*output_npixel,0);
       

        #pragma omp parallel for schedule(dynamic) num_threads(theadspershape)
        for(int y = 0; y<output_npixel; y++)
          for( int x = 0; x<output_npixel; x++){
              int index= x+y*output_npixel;
              FLOAT_TYPE ix = FLOAT_TYPE(x) - output_npixel/2.- xshift;
              FLOAT_TYPE iy = FLOAT_TYPE(y) - output_npixel/2. - yshift;
              FLOAT_TYPE det_dist = std::sqrt(ix*ix+iy*iy)*pixel_size;
              FLOAT_TYPE theta_dist=std::atan(det_dist);
              FLOAT_TYPE q_dist=K*det_dist/std::sqrt(1.f+det_dist*det_dist);
              FLOAT_TYPE q_pixels = q_dist/dq;
              FLOAT_TYPE phi = std::atan2(iy,ix);                            
              FLOAT_TYPE xt = q_pixels * std::cos(phi);
              FLOAT_TYPE yt = q_pixels * std::sin(phi);                
              xt += FLOAT_TYPE(npixel)/2.+xshift;
              yt += FLOAT_TYPE(npixel)/2.+yshift;
              FLOAT_TYPE intscale=std::pow(std::cos(theta_dist), 3);
              tempdata[index]=intscale*get_interpolation(data, xt, yt, npixel);
        }
        
        return tempdata;       
        
    }
    
    std::vector<FLOAT_TYPE>  angle_coordinates(std::vector<FLOAT_TYPE> data, FLOAT_TYPE K, FLOAT_TYPE dq, FLOAT_TYPE xshift, FLOAT_TYPE yshift,FLOAT_TYPE pixel_size, int npixel,int output_npixel, int theadspershape=1, int intcorrection=0){
        std::vector<FLOAT_TYPE> tempdata(output_npixel*output_npixel,0);

        FLOAT_TYPE dtheta=pixel_size/180.*M_PI;

        #pragma omp parallel for schedule(dynamic) num_threads(theadspershape)
        for(int y = 0; y<output_npixel; y++)
          for( int x = 0; x<output_npixel; x++){
              int index= x+y*output_npixel;
              FLOAT_TYPE ix = FLOAT_TYPE(x) - output_npixel/2.-xshift;
              FLOAT_TYPE iy = FLOAT_TYPE(y) - output_npixel/2.-yshift;
              FLOAT_TYPE theta_dist = std::sqrt(ix*ix+iy*iy)*dtheta;
              FLOAT_TYPE q_dist=K*std::sin(theta_dist);
              FLOAT_TYPE q_pixels = q_dist/dq;
              FLOAT_TYPE phi = std::atan2(iy,ix);                            
              FLOAT_TYPE xt = q_pixels * std::cos(phi);
              FLOAT_TYPE yt = q_pixels * std::sin(phi);                
              xt += FLOAT_TYPE(npixel)/2.+xshift;
              yt += FLOAT_TYPE(npixel)/2.+yshift;
              FLOAT_TYPE intscale=1.;
              if(intcorrection==1) intscale=std::pow(std::cos(theta_dist), 3);
              tempdata[index]=intscale*get_interpolation(data, xt, yt, npixel);
        }
        
        return tempdata;       
        
    }

    
    std::vector<FLOAT_TYPE>  projection_coordinates(std::vector<FLOAT_TYPE> data, int npixel,int output_npixel, int theadspershape=1){
        std::vector<FLOAT_TYPE> tempdata(output_npixel*output_npixel,0);

        #pragma omp parallel for schedule(dynamic) num_threads(theadspershape)
        for(int y = 0; y<output_npixel; y++)
          for( int x = 0; x<output_npixel; x++){
              int index= x+y*output_npixel;
              int newindex=(x-output_npixel/2+npixel/2)+(y-output_npixel/2+npixel/2)*npixel;
              tempdata[index]=data[newindex];
        }
        
        return tempdata;       
        
    }    
    std::vector<std::complex<FLOAT_TYPE>> projection_coordinates(FFTW_COMPLEX*  data, int npixel,int output_npixel, int theadspershape=1){
        std::vector<std::complex<FLOAT_TYPE>> tempdata(output_npixel*output_npixel,{0,0});

        #pragma omp parallel for schedule(dynamic) num_threads(theadspershape)
        for(int y = 0; y<output_npixel; y++)
          for( int x = 0; x<output_npixel; x++){
              int index= x+y*output_npixel;
              int newindex=(x-output_npixel/2+npixel/2)+(y-output_npixel/2+npixel/2)*npixel;
              tempdata[index].real(data[newindex][0]);
              tempdata[index].imag(data[newindex][1]);

        }
        
        return tempdata;       
        
    } 

inline void mult(FFTW_COMPLEX& A, FFTW_COMPLEX& B, FFTW_COMPLEX& C){
    FFTW_COMPLEX temp={ A[0]*B[0]-A[1]*B[1], A[0]*B[1]+A[1]*B[0]};
    C[0] = temp[0];
    C[1] = temp[1];
}



FLOAT_TYPE get_total_absorption(FLOAT_TYPE* total_attenuation, int cut_npixel){
    
    FLOAT_TYPE sum=0;
    for(int index=0; index<cut_npixel*cut_npixel; index++)
            sum+=1.-exp(2.*total_attenuation[index]);
    return sum;
    
}


FLOAT_TYPE get_total_intensity(FLOAT_TYPE* total_attenuation, FLOAT_TYPE* total_phase,int cut_npixel){
    
    FFTW_COMPLEX sum={0,0};
    for(int index=0; index<cut_npixel*cut_npixel; index++){
            sum[0]+=exp(total_attenuation[index])*std::cos(total_phase[index]);
            sum[1]+=exp(total_attenuation[index])*std::sin(total_phase[index]);
    }
    return sum[0]*sum[0]+sum[1]*sum[1];
    
}


FLOAT_TYPE get_scattered_intensity(FLOAT_TYPE* total_attenuation, FLOAT_TYPE* total_phase,int cut_npixel){
    
    FFTW_COMPLEX sum={0,0};
    for(int index=0; index<cut_npixel*cut_npixel; index++){
            sum[0]+=(1-exp(total_attenuation[index]))*std::cos(total_phase[index]);
            sum[1]+=(1-exp(total_attenuation[index]))*std::sin(total_phase[index]);
    }
    return sum[0]*sum[0]+sum[1]*sum[1];
    
}

// 
void msft(Shapes::Shape& shape, Pattern& pattern, int nthreads){
    FLOAT_TYPE xshift=shape.xshift;
    FLOAT_TYPE yshift=shape.yshift;
        
    
    
    
    int tight_npixel=std::ceil(shape.maxDim/context.dx);
    int cut_npixel=tight_npixel+4;  
    
    if(cut_npixel>context.npixel/2) cut_npixel=context.npixel/2;
    
    
    
    FLOAT_TYPE gridsize=FLOAT_TYPE(cut_npixel)*context.dx;
    int nslices = std::ceil(gridsize/context.dz);

    
    Grid grid({cut_npixel,cut_npixel,nslices}, 
                    {-gridsize/FLOAT_TYPE(2),-gridsize/FLOAT_TYPE(2),-gridsize/FLOAT_TYPE(2)},
                    { context.dx, context.dx, context.dz}, shape.latitude, shape.longitude, shape.orientation);
    
    
    unsigned int N = context.npixel*context.npixel;

    FLOAT_TYPE* total_phase=new FLOAT_TYPE[cut_npixel*cut_npixel];
    FLOAT_TYPE* total_attenuation=new FLOAT_TYPE[cut_npixel*cut_npixel];
    
    
    FFTW_COMPLEX** in_cut= new FFTW_COMPLEX*[nslices];
    FFTW_COMPLEX *total_field;
    std::vector<FLOAT_TYPE> total_pattern(context.npixel*context.npixel, 0);
    
    for(int islice=0; islice<nslices; islice++){
        in_cut[islice]= new FFTW_COMPLEX[cut_npixel*cut_npixel];
        for(unsigned int i=0; i<cut_npixel*cut_npixel; ++i){
            in_cut[islice][i][0]=0;
            in_cut[islice][i][1]=0;
        }
    }
    
    total_field = (FFTW_COMPLEX*) FFTW_MALLOC(sizeof(FFTW_COMPLEX)*context.npixel*context.npixel);
   
    
    for(unsigned int i=0; i<context.npixel*context.npixel; ++i){
        total_field[i][0]=0;
        total_field[i][1]=0;

    }
    for(unsigned int i=0; i<cut_npixel*cut_npixel; ++i){
        total_phase[i]=0;
        total_attenuation[i]=0;
    }
    

    
    
    


for(int islice =0; islice<nslices; ++islice){

#ifdef _WIN32
    #pragma omp parallel for schedule(dynamic) num_threads(nthreads)    
#else
    #pragma omp parallel for schedule(dynamic) collapse(2) num_threads(nthreads)
#endif
        for(int y_d=0; y_d<cut_npixel; y_d++)
            for(int x_d=0; x_d<cut_npixel; x_d++){
                int index=x_d+y_d*cut_npixel;
                std::vector<FLOAT_TYPE> coords(3);
                grid.get_coord(x_d, y_d, islice, &coords[0]);
                
                FLOAT_TYPE min[3]={coords[0]-context.dx*SIGMA, coords[1]-context.dx*SIGMA,coords[2]-context.dz*SIGMA};
                FLOAT_TYPE max[3]={coords[0]+context.dx*SIGMA, coords[1]+context.dx*SIGMA,coords[2]+context.dz*SIGMA};
                
                FLOAT_TYPE r_index[2];
                
                shape.get_refractive_index_os(min, max, r_index, context.os);

                in_cut[islice][index][0]=((r_index[0]-1)*std::cos(total_phase[index]) -  
                                           r_index[1]   *std::sin(total_phase[index]))*exp(total_attenuation[index]);
                in_cut[islice][index][1]=((r_index[0]-1)*std::sin(total_phase[index]) +
                                           r_index[1]   *std::cos(total_phase[index]))*exp(total_attenuation[index]);
                
                
                total_phase[index] += r_index[0]* context.K * context.dz ;
                total_attenuation[index]-= r_index[1]* context.K * context.dz;

    
            }
}

/*

for(int islice =0; islice<nslices; ++islice){

#ifdef _WIN32
    #pragma omp parallel for schedule(dynamic) num_threads(nthreads)    
#else
    #pragma omp parallel for schedule(dynamic) collapse(2) num_threads(nthreads)
#endif
        for(int y_d=0; y_d<cut_npixel; y_d++)
            for(int x_d=0; x_d<cut_npixel; x_d++){
                int index=x_d+y_d*cut_npixel;
                std::vector<FLOAT_TYPE> coords(3);
                grid.get_coord(x_d, y_d, islice, &coords[0]);
                
                FLOAT_TYPE min[3]={coords[0]-context.dx*SIGMA, coords[1]-context.dx*SIGMA,coords[2]-context.dz*SIGMA};
                FLOAT_TYPE max[3]={coords[0]+context.dx*SIGMA, coords[1]+context.dx*SIGMA,coords[2]+context.dz*SIGMA};
                
                FLOAT_TYPE r_index[2];
                
                shape.get_refractive_index_os(min, max, r_index, context.os);
                FFTW_COMPLEX potential={r_index[0]*r_index[0]-r_index[1]*r_index[1]-1,
                                        2.*r_index[0]*r_index[1]};
                in_cut[islice][index][0]=(potential[0]*std::cos(total_phase[index])-potential[1]*std::sin(total_phase[index]))*exp(total_attenuation[index]);
                in_cut[islice][index][1]=(potential[0]*std::sin(total_phase[index])+potential[1]*std::cos(total_phase[index]))*exp(total_attenuation[index]);
                
//                 total_phase[index] += r_index[0] * context.K * context.dz ;
//                 total_attenuation[index]-= r_index[1] * context.K * context.dz;
                FLOAT_TYPE delta=1-r_index[0];
                FLOAT_TYPE beta=r_index[1];
                
//                 total_phase[index] += (1-delta+0.5*delta*delta-0.5*beta*beta) * context.K * context.dz ;
//                 total_attenuation[index]-= (beta-beta*delta) * context.K * context.dz;
                total_phase[index] += (1-delta) * context.K * context.dz ;
                total_attenuation[index]-= (beta) * context.K * context.dz;
                
//                 FLOAT_TYPE density=shape.get_density_os(min, max, context.os);
// 
//                 FLOAT_TYPE delta=shape.get_delta_os(min, max, context.os);
//                 FLOAT_TYPE beta=shape.get_beta_os(min, max, context.os);
// //                 FLOAT_TYPE delta=shape.get_delta(coords[0],coords[1],coords[2]);
// //                 FLOAT_TYPE beta=shape.get_beta(coords[0],coords[1],coords[2]);
// 
//                 in_cut[islice][index][0]+=density*beta*exp(total_attenuation[index])*std::cos(total_phase[index]);
//                 in_cut[islice][index][1]+=density*beta*exp(total_attenuation[index])*std::sin(total_phase[index]);
//                 
//                 total_phase[index] += (1.-delta* density) * context.K * context.dz ;
//                 total_attenuation[index]-= beta * context.K * context.dz * density;

    
            }
}*/


// // // // // // 

FFTW_COMPLEX** in=new FFTW_COMPLEX*[nthreads];
FFTW_PLAN* p=new FFTW_PLAN[nthreads];



for(int ithread=0; ithread<nthreads; ithread++){
    in[ithread] = (FFTW_COMPLEX*) FFTW_MALLOC(sizeof(FFTW_COMPLEX)*N);
    
    #pragma omp critical (planning)
    p[ithread]=FFTW_PLAN_DFT_2D(context.npixel, context.npixel, in[ithread], in[ithread], FFTW_FORWARD, FFTW_ESTIMATE);
}


#pragma omp parallel for schedule(dynamic) num_threads(nthreads)
    for(int islice =0; islice<nslices; ++islice){
        int ithread=omp_get_thread_num();
 
        FLOAT_TYPE* tempin= (FLOAT_TYPE*) in[ithread];
//         #pragma omp simd aligned(tempin)       
        for(unsigned int i=0; i<2*N; ++i){
            tempin[i]=0;
        }
        

        
        for(int y=0; y<cut_npixel; ++y)
            for(int x=0; x<cut_npixel; ++x){
                int index_cut = x + y*cut_npixel;
                int index = (x+context.npixel/2-cut_npixel/2) + (y+context.npixel/2-cut_npixel/2)*context.npixel;
                FLOAT_TYPE phase = 2.*M_PI*((xshift/FLOAT_TYPE(context.npixel)+0.5)*x + (yshift/FLOAT_TYPE(context.npixel)+0.5)*y ); 
                FFTW_COMPLEX shift={std::cos(phase), std::sin(phase)};
/*                in[ithread][index][0] = in_cut[islice][index_cut][0];
                in[ithread][index][1] = in_cut[islice][index_cut][1];   */     
                in[ithread][index][0] = in_cut[islice][index_cut][0]*shift[0]-in_cut[islice][index_cut][1]*shift[1];
                in[ithread][index][1] = in_cut[islice][index_cut][0]*shift[1]+in_cut[islice][index_cut][1]*shift[0];  
        }
//         
        FFTW_EXECUTE(p[ithread]); 
//     
//       
        for(int iy=0; iy<context.npixel; ++iy)
          for(int ix=0; ix<context.npixel; ++ix){

            int index = (ix) + (iy)*context.npixel;
              
            FLOAT_TYPE dist_q_square=((ix-context.npixel/2-xshift)*(ix-context.npixel/2-xshift)
                                 +(iy-context.npixel/2-yshift)*(iy-context.npixel/2-yshift))*std::pow(context.dq,2);
          
//             FLOAT_TYPE qz = context.K*(1.-std::sqrt(1-dist_q_square/(std::pow(context.K,2))));
            if(context.K*context.K>=dist_q_square){
                FLOAT_TYPE kz = std::sqrt(std::pow(context.K,2)-dist_q_square);
                FLOAT_TYPE phase_diff = -kz*context.dz*FLOAT_TYPE(islice);


                FLOAT_TYPE mod=std::sqrt(in[ithread][index][0]*in[ithread][index][0]+in[ithread][index][1]*in[ithread][index][1]);
                FLOAT_TYPE phase=std::atan2(in[ithread][index][1],in[ithread][index][0]) + phase_diff;
                
                
                FLOAT_TYPE realp=mod*std::cos(phase);
                FLOAT_TYPE imagp=mod*std::sin(phase);
                
                #pragma omp atomic 
                total_field[index][0]+=realp;
                #pragma omp atomic 
                total_field[index][1]+=imagp;
            }

        }        
//         
    }

    for(int ithread=0; ithread<nthreads; ithread++){
            FFTW_DESTROY_PLAN(p[ithread]);
            FFTW_FREE(in[ithread]); 
            
    }

    delete [] p;
    delete [] in;


    for(int index=0; index<context.npixel*context.npixel; ++index){
            total_pattern[index]=total_field[index][0]*total_field[index][0]+total_field[index][1]*total_field[index][1];
        } 

    
    
    FLOAT_TYPE scatsum=0;
    for(int index=0; index<context.npixel*context.npixel; ++index){
            scatsum+=total_pattern[index];
        }

//     scatsum*=context.dx*context.dx*context.dx*context.dx;
/*    scatsum*=context.dx*context.dx*context.dx*context.dx/(context.npixel*context.npixel);
    FLOAT_TYPE in_sum=std::pow(context.dx*cut_npixel,2);
    FLOAT_TYPE total_photons=std::pow(context.dx*cut_npixel,2)*context.photon_density;
    pattern.scattered_photons=scatsum/in_sum*total_photons;
    printf("In: %e, Scattered: %e, Ratio: %e, In photons: %e, dx: %e, npix: %d\n",in_sum, scatsum, scatsum/in_sum, total_photons, context.dx, context.npixel);   */      
     
    scatsum*=context.dx*context.dx/(context.npixel*context.npixel);
    FLOAT_TYPE in_sum=context.npixel*context.npixel;
    FLOAT_TYPE total_photons=std::pow(context.dx*context.npixel,2)*context.photon_density;
    pattern.scattered_photons=scatsum/in_sum*total_photons;
//     printf("In: %e, Scattered: %e, Ratio: %e, In photons: %e, Scatt. photons: %e\n",in_sum, scatsum, scatsum/in_sum, total_photons, pattern.scattered_photons);   

//     FLOAT_TYPE scatt_power=get_total_absorption(total_attenuation, cut_npixel);
//     FLOAT_TYPE input_power=std::pow(cut_npixel,2);
//     FLOAT_TYPE scatt_power_ratio=scatt_power/input_power;
//     FLOAT_TYPE total_photons=std::pow(context.dx*cut_npixel,2)*context.photon_density;
//     pattern.scattered_photons=scatt_power_ratio*total_photons;
//     printf("Ratio: %e, Scatt. photons: %e\n",scatt_power_ratio, pattern.scattered_photons);
//     
    

    if(pattern.coordinates=="angle"){
//         printf("Changing coordinates to angle\n");
        pattern.data = angle_coordinates(total_pattern, context.K, context.dq, xshift, yshift, pattern.pixel_size, context.npixel,context.output_npixel,nthreads); 
    }
    else if(pattern.coordinates=="angle-flat"){
//         printf("Changing coordinates to angle\n");
        pattern.data = angle_coordinates(total_pattern, context.K, context.dq, xshift, yshift, pattern.pixel_size, context.npixel,context.output_npixel,nthreads,1); 
    }
    else if(pattern.coordinates=="momentum"){
//         printf("Changing coordinates to momentum\n");
        pattern.data = momentum_coordinates(total_pattern, context.K, context.dq, xshift, yshift, pattern.pixel_size, context.npixel,context.output_npixel,nthreads);                    
    }
    else if(pattern.coordinates=="detector"){
//         printf("Changing coordinates to momentum\n");
        pattern.data = detector_coordinates(total_pattern, context.K, context.dq, xshift, yshift, pattern.pixel_size, context.npixel,context.output_npixel,nthreads);                    
    }    
    else{
        pattern.data = projection_coordinates(total_pattern, context.npixel,context.output_npixel,nthreads);                    
        if(pattern.save_field==1)
            pattern.field = projection_coordinates(total_field, context.npixel,context.output_npixel,nthreads);                    
    }
        
//     if(pattern.save_field==1){
// 
//         for(int index=0; index<context.output_npixel*context.output_npixel; ++index){
//             pattern.field[index]={total_field[index][0],total_field[index][1]};
//         }
//     }
    
    
//     FLOAT_TYPE total_absorption= get_total_absorption(total_attenuation, cut_npixel);
//     pattern.scattered_photons=total_absorption*context.photon_density*std::exp(-0.5*shape.beam_position*shape.beam_position);
    /*
    FLOAT_TYPE sum=0;
    for(int index=0; index<context.output_npixel*context.output_npixel; ++index){
            sum+=pattern.data[index];
        }
    total_absorption= 
    pattern.scattered_photons=total_absorption*context.photon_density*std::exp(-0.5*shape.beam_position*shape.beam_position);
    */
    
    

    FFTW_FREE(total_field);
    for(int islice=0; islice<nslices; islice++) delete [] in_cut[islice];

    
    delete [] in_cut;
    delete [] total_phase;
    delete [] total_attenuation;
    
}





// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 

void set_interpolation(FLOAT_TYPE* data,FLOAT_TYPE* weights, FLOAT_TYPE x, FLOAT_TYPE y, int npixel, FLOAT_TYPE value){
              int ixt = std::floor(x);
              int iyt = std::floor(y);

              FLOAT_TYPE xtdec = x-FLOAT_TYPE(ixt);
              FLOAT_TYPE ytdec = y-FLOAT_TYPE(iyt);
              
                if(ixt>=0 && iyt>=0 && ixt<npixel && iyt<npixel ){
                    FLOAT_TYPE weight = (1.-xtdec)*(1.-ytdec);
                    data[ixt + iyt*npixel]+=value*weight;
                    weights[ixt + iyt*npixel]+= weight;
                }
                if(ixt+1>=0 && iyt>=0 && ixt+1<npixel && iyt<npixel ){
                    FLOAT_TYPE weight = (xtdec)*(1.-ytdec);
                    data[ixt + 1 + iyt*npixel]+=value*weight;
                    weights[ixt + 1 + iyt*npixel]+= weight;
                }
                if(ixt>=0 && iyt+1>=0 && ixt<npixel && iyt+1<npixel ){
                    FLOAT_TYPE weight = (1.-xtdec)*(ytdec);
                    data[ixt + (iyt+1)*npixel]+=value*weight;
                    weights[ixt + (iyt+1)*npixel]+= weight;
                }
                if(ixt+1>=0 && iyt+1>=0 && ixt+1<npixel && iyt+1<npixel ){
                    FLOAT_TYPE weight = (xtdec)*(ytdec);
                    data[ixt + 1 + (iyt+1)*npixel]+=value*weight;
                    weights[ixt + 1 + (iyt+1)*npixel]+= weight;
                }
    }


    
void rescale_pattern(Pattern& pattern){


        FLOAT_TYPE* temp_pattern=new FLOAT_TYPE[context.output_npixel*context.output_npixel];
    
        for(unsigned int i=0; i<context.output_npixel*context.output_npixel; i++)
            temp_pattern[i]=pattern.data[i];
    
        pattern.resize(context.scale_npixel);   
        
        FLOAT_TYPE* weights=new FLOAT_TYPE[context.scale_npixel*context.scale_npixel];

        
        for(unsigned int i=0; i<context.scale_npixel*context.scale_npixel; i++){
            weights[i]=0;
            pattern.data[i]=0;
        }
        
        FLOAT_TYPE conv_factor = FLOAT_TYPE(context.scale_npixel)/FLOAT_TYPE(context.output_npixel);
        for(int y=0; y<context.output_npixel; y++)
            for(int x=0; x<context.output_npixel; x++){
              unsigned int index= x+y*context.output_npixel;
              FLOAT_TYPE ix = FLOAT_TYPE(x)*conv_factor;
              FLOAT_TYPE iy = FLOAT_TYPE(y)*conv_factor;
              set_interpolation(&pattern.data[0], weights, ix, iy,context.scale_npixel, temp_pattern[index]);
        }
                  
        for(unsigned int i=0; i<context.scale_npixel*context.scale_npixel; i++){
            pattern.data[i]/=weights[i];
        }
        
        delete [] temp_pattern;
        delete [] weights;
}


















