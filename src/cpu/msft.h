#pragma once

#include "datatypes.h"

#include <cstdio>
#include <cmath>
#include <omp.h>

#include "fftw3.h"

#include "grid.h"
#include "shapes/shape.h"
#include "context.h"
#include "pattern.h"

extern Context context;

void msft(Shapes::Shape& shape, Pattern& pattern, int nthreads);

void rescale_pattern(Pattern& pattern);
