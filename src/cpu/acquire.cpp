
#include "cpu/acquire.h"
#include "cpu/msft.h"
#include "context.h"
#include <string>

extern Context context;

namespace Detectors{
// // // // // // // // // // // // // // // // // // // // // // // // // //     
   
    
std::vector<Pattern> acquire_dataset(Detectors::Detector& detector, std::vector<Shapes::Shape*> dataset, bool save_field=0){    
         context.init_cpu();

         int nshapes=dataset.size();
         std::vector<Pattern> pattern_list(nshapes, Pattern(context.output_npixel, detector.coordinates, detector.pixel_size, save_field));


         int maxthreads=context.nthreads;
         int threadspershape=std::ceil(FLOAT_TYPE(maxthreads)/FLOAT_TYPE(nshapes));
         int nthreads=std::ceil(FLOAT_TYPE(maxthreads)/FLOAT_TYPE(threadspershape));
         
         FLOAT_TYPE totcounts=0;
        auto print_counts=[&](){
            printf("\t\t\t");
            printf("\r");
            printf("%.1f%%\t", float(totcounts)/float(nshapes)*100.);
            printf("\r");
        };
        if(context.verbose) printf("\n");
         
        if(context.verbose) fputs("\e[?25l", stdout);

#pragma omp parallel for schedule(dynamic) num_threads(nthreads)
         for(int ishape=0; ishape<nshapes; ishape++){
            dataset[ishape]->init();
             
            bool check_size = (int(std::ceil(dataset[ishape]->maxDim/context.dx))<=int(context.output_npixel-4)); 
            if(!check_size){
                FLOAT_TYPE maxsize = FLOAT_TYPE(context.output_npixel-4)*context.dx;
                printf("WARNING: Shape %d (%s) with size %.2f exceeds maximum allowed size (%.2f) for the given experimental parameters. \n", ishape,dataset[ishape]->get_name().c_str(), dataset[ishape]->maxDim, FLOAT_TYPE(context.output_npixel/2)*context.dx);
            }
//         else{
            
            

            msft(*dataset[ishape], pattern_list[ishape],  threadspershape);
            
            #pragma omp atomic
            totcounts++;
            
            if(context.verbose){
                #pragma omp critical(print_c)
                print_counts();
            }
            
            if(context.output_npixel!=context.scale_npixel)
                rescale_pattern(pattern_list[ishape]);

            
            detector.apply_features(pattern_list[ishape]);         
            
            
            if(detector.mask_radius>0) detector.apply_mask(pattern_list[ishape]);
            
            
//         }
         }
        if(context.verbose) fputs("\e[?25h", stdout); 
        if(context.verbose) printf("\n");  
        return std::move(pattern_list);
}




// // // // // // // // // // // // // // // // // // // // // // // // // //     
   
    
std::vector<FLOAT_TYPE> compare_dataset(Detectors::Detector& detector, std::vector<Shapes::Shape*> dataset, FLOAT_TYPE* error_pattern, int* errormap, FLOAT_TYPE error_norm, FLOAT_TYPE experiment_sum, FLOAT_TYPE error_exponent, FLOAT_TYPE error_metric){    
         context.init_cpu();

         int nshapes=dataset.size();
         std::vector<Pattern> pattern_list(nshapes, Pattern(context.output_npixel, detector.coordinates, detector.pixel_size));
         std::vector<FLOAT_TYPE> errors(nshapes);


         int maxthreads=context.nthreads;
         int threadspershape=std::ceil(FLOAT_TYPE(maxthreads)/FLOAT_TYPE(nshapes));
         int nthreads=std::ceil(FLOAT_TYPE(maxthreads)/FLOAT_TYPE(threadspershape));
         
#pragma omp parallel for schedule(dynamic) num_threads(nthreads)
         for(int ishape=0; ishape<nshapes; ishape++){
            dataset[ishape]->init();
             
            bool check_size = (int(std::ceil(dataset[ishape]->maxDim/context.dx))<=int(context.output_npixel-4)); 
            if(!check_size){
                FLOAT_TYPE maxsize = FLOAT_TYPE(context.output_npixel-4)*context.dx;
                printf("WARNING: Shape %d (%s) with size %.2f exceeds maximum allowed size (%.2f) for the given experimental parameters. \n", ishape,dataset[ishape]->get_name().c_str(), dataset[ishape]->maxDim, FLOAT_TYPE(context.output_npixel/2)*context.dx);
            }
//         else{
            
            msft(*dataset[ishape], pattern_list[ishape],  threadspershape);
            errors[ishape] = pattern_list[ishape].get_error(error_pattern, errormap,error_norm,  experiment_sum, (*dataset[ishape]).scaling, (*dataset[ishape]).offset, error_exponent, error_metric);
         }
        return std::move(errors);
}

};

