#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>
#include <complex>

#define INTERP_EPSILON 1e-6

class Pattern{
public:

    Pattern(unsigned int npixel_, std::string coordinates_, FLOAT_TYPE pixel_size_, bool save_field_=0, FLOAT_TYPE scattered_photons_=-1): npixel(npixel_), data(npixel_*npixel_, -1), coordinates(coordinates_), pixel_size(pixel_size_),scattered_photons(scattered_photons_), save_field(save_field_){
        if(save_field){
            field.resize(npixel*npixel);
        }
                
    }
    Pattern(const Pattern& in): npixel(in.npixel), data(in.data), coordinates(in.coordinates), pixel_size(in.pixel_size), scattered_photons(in.scattered_photons), save_field(in.save_field), field(in.field) {}    
    
    
    Pattern(Pattern&& other): data(std::move(other.data)), field(std::move(other.field)){
        npixel=other.npixel;
        coordinates= other.coordinates;
        pixel_size=other.pixel_size;
        scattered_photons=other.scattered_photons;
        save_field=other.save_field;
    }
//     Pattern(Pattern&& in): npixel(in.npixel), data(std::move(in.data)){}

//     void save_h5(std::string filename, std::vector<std::string> path, std::string dataset){
//         write_h5(filename, path, dataset, {npixel,npixel}, &data[0]);
//         
//     }
    
    void resize(unsigned int npixel_){
        npixel=npixel_;
        data.resize(npixel*npixel);

    }
    
    
    
    std::vector<FLOAT_TYPE> get(){return data;}
    void set(std::vector<FLOAT_TYPE> data_in){data=data_in;} 
    
    

    
    FLOAT_TYPE get(unsigned int index){return data[index];}
    FLOAT_TYPE get(unsigned int x, unsigned int y){return data[x + y*npixel];}
    
    
    
    
    FLOAT_TYPE get_error(FLOAT_TYPE* error_pattern, int* errormap,  FLOAT_TYPE error_norm, FLOAT_TYPE experiment_sum,  FLOAT_TYPE scaling, FLOAT_TYPE offset, FLOAT_TYPE exponent, FLOAT_TYPE metric=1.){
        FLOAT_TYPE datasum=0;

        #pragma omp simd reduction(+:datasum)
        for(int index=0; index<data.size(); index++)
            if(errormap[index]==1){
                datasum+=data[index];
            }

            
        FLOAT_TYPE error=0;
        FLOAT_TYPE tempval;
        
        #pragma omp simd private(tempval)  reduction(+:error)
        for(int index=0; index<data.size(); index++)
            if(errormap[index]==1){
                tempval = data[index]*scaling/datasum*experiment_sum+offset;
//                 printf("val: %f\n", tempval);
                error+=std::pow(std::fabs(error_pattern[index] - std::pow(tempval, exponent)), metric);
            }
        return std::pow(error, 1./metric)/error_norm;
        
        
    }
    
    
    
//     FLOAT_TYPE get_pixel_size(){return pixel_size;}
//     std::string get_coordinates(){return coordinates;}
    
    
    
    
    void set(FLOAT_TYPE val, unsigned int index){data[index]=val;}
    void set(FLOAT_TYPE val, unsigned int x, unsigned int y){data[x + y*npixel]=val;}    

    unsigned int npixel;
    bool save_field;
    std::string coordinates;
    FLOAT_TYPE pixel_size;
    FLOAT_TYPE scattered_photons;
    std::vector<FLOAT_TYPE> data;
    std::vector<std::complex<FLOAT_TYPE>> field;

    
};
