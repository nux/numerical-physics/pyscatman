#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>
#include <iostream>
#include <omp.h>
#include <string>
#include <iostream>
#include <chrono>

#include "cpu/acquire.h"
#ifdef WITH_CUDA
    #include "gpu/acquire.h"
#endif
#include "pattern.h"
#include "shapes/shape.h"
#include "util/tuning.h"
#include "context.h"
#include "check.h"
#define EPSILON 1e-6


// class Context;

extern Context context;
// #include "h5_utilities.h"
namespace Detectors{
    class Detector;

    std::vector<Pattern> acquire_dataset(Detectors::Detector& , std::vector<Shapes::Shape*>, bool);
#ifdef WITH_CUDA
    std::vector<Pattern> acquire_dataset_gpu(Detectors::Detector& , std::vector<Shapes::Shape*>, bool);
#endif    

class Detector{
public:

    Detector(unsigned int mask_radius_=0, std::string coordinates_="projection"): mask_radius(mask_radius_), coordinates(coordinates_){
        CheckOptions(coordinates, {"projection", "momentum", "angle", "angle-flat", "detector"}, "coordinates");
        
        if(coordinates=="angle" || coordinates=="angle-flat"){
            pixel_size=context.output_theta/FLOAT_TYPE(context.output_npixel/2.);
        }
        else if(coordinates=="momentum"){
            pixel_size= 2.*context.K*std::sin(context.output_thetarad/2.) / FLOAT_TYPE(context.output_npixel/2.);
        }
        else if(coordinates=="projection"){
            pixel_size=context.dq;
        }
        else if(coordinates=="detector"){
            pixel_size=std::tan(context.output_thetarad) / FLOAT_TYPE(context.output_npixel/2.);
        }
        
        
    }

        
        
    std::vector<Pattern> acquire(std::vector<Shapes::Shape*> dataset, bool save_field=0){
        std::vector<Pattern> dataset_pattern;
        auto t1 = std::chrono::high_resolution_clock::now();
#ifdef WITH_CUDA
        if(context.on_gpu)
            dataset_pattern=std::move(Detectors::acquire_dataset_gpu(*this,dataset, save_field));
        else
            dataset_pattern=std::move(Detectors::acquire_dataset(*this,dataset, save_field));   
#else
        dataset_pattern=std::move(Detectors::acquire_dataset(*this,dataset, save_field));   
#endif
        auto t2 = std::chrono::high_resolution_clock::now();
        if(context.verbose){
            std::chrono::duration<float, std::milli> fp_ms = t2 - t1;
            float milliseconds=fp_ms.count();
            float seconds=milliseconds/1000.;
            float milli_per_pattern=milliseconds/float(dataset.size());
            printf("Total time:       %.3f s\n", seconds);
            printf("Time per pattern: %.1f ms\n", milli_per_pattern);
        }
        
        return std::move(dataset_pattern);
    }
    
    Pattern acquire(Shapes::Shape* shape, bool save_field=0){
       std::vector<Shapes::Shape*> shape_list;
       shape_list.push_back(shape);
       return std::move(acquire(shape_list, save_field)[0]);
    }

    
    void apply_mask(Pattern& pattern){
//         printf("Setting mask with radius %d\n", mask_radius);
        for(unsigned int y=0; y<context.output_npixel; ++y)
            for(unsigned int x=0; x<context.output_npixel; ++x){
                FLOAT_TYPE xs=x-context.output_npixel/2.;
                FLOAT_TYPE ys=y-context.output_npixel/2.;
                if(xs*xs+ys*ys<mask_radius*mask_radius) {
                        pattern.set(0, x,y);
                }
                
            }
        
    }
 
    virtual int apply_features(Pattern& pattern) = 0;



    
    int poisson_noise(Pattern& pattern){
        
        auto& data = pattern.data;
        
        FLOAT_TYPE sum = 0;
        for(auto& value : data) sum+=value;
        if(sum==0) return 1;

        
        for(auto& value : data) value=value/sum*pattern.scattered_photons;
        

        std::default_random_engine generator;
        std::poisson_distribution<long int> d(1);  
        
//         printf("%f\n", average);
        
        for(auto& value : data){
            d.param(std::poisson_distribution<long int>::param_type{value});
            value=d(generator); //d(generator);   /*response(d(generator));   */     
        }
        
        return 0;
    }
 
    
    
    unsigned int mask_radius;
    FLOAT_TYPE pixel_size;
    std::string coordinates;
   
        
// // // // // // // //     
    

    
//     std::vector<FLOAT_TYPE> data;
};
};
