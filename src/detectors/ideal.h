#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>
#include <random>

#include "pattern.h"
#include "detectors/detector.h"

// #include "h5_utilities.h"
namespace Detectors{

class Ideal: public Detector{
public:
    Ideal(unsigned int mask_radius_=0, std::string coordinates_="projection"): Detector(mask_radius_, coordinates_){}
    
    
    int apply_features(Pattern& pattern){
        
        if(context.photon_density>0) return poisson_noise(pattern);
        return 0;        
    };
    
};
};
