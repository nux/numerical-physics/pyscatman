#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>
#include <random>

#include "pattern.h"
#include "detectors/detector.h"

// #include "h5_utilities.h"
namespace Detectors{

class MSFT: public Detector{
public:
    MSFT(std::string coordinates_="projection"): Detector(0, coordinates_){}
    
    int apply_features(Pattern& pattern){
        return 0;        
    };
    
    
    std::vector<FLOAT_TYPE> compare(std::vector<Shapes::Shape*> dataset, FLOAT_TYPE* experiment, int* errormap, FLOAT_TYPE error_norm,FLOAT_TYPE experiment_sum, FLOAT_TYPE error_exponent, FLOAT_TYPE metric){
#ifdef WITH_CUDA
        if(context.on_gpu)
            return std::move(Detectors::compare_dataset_gpu(*this,dataset, experiment, errormap, error_norm, experiment_sum, error_exponent, metric));
        else
            return std::move(Detectors::compare_dataset(*this,dataset, experiment, errormap, error_norm, experiment_sum, error_exponent, metric ));   
#else
        return std::move(Detectors::compare_dataset(*this,dataset, experiment, errormap, error_norm, experiment_sum, error_exponent, metric ));   
#endif
    }
    
    
};
};
