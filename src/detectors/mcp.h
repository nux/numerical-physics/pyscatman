#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>
#include <random>
#include "pattern.h"
#include "detectors/detector.h"

// #include "h5_utilities.h"
namespace Detectors{

class MCP: public Detector{
public:
    MCP(unsigned int mask_radius_, FLOAT_TYPE gain_, FLOAT_TYPE saturation_ , FLOAT_TYPE linearity_ , FLOAT_TYPE exponent_, FLOAT_TYPE offset_ , std::string coordinates_="projection"): 
        Detector(mask_radius_, coordinates_), 
        gain(gain_), 
        saturation(saturation_), 
        linearity(linearity_),
        offset(offset_),
        exponent(exponent_){}
    
    int apply_features(Pattern& pattern){
        
        
        if(context.photon_density>0) poisson_noise(pattern);

        
        auto& data = pattern.data;
        
        
        auto response = [this](long int input)->FLOAT_TYPE{

            FLOAT_TYPE val=gain*std::pow(input,exponent);
            if(val <= linearity*saturation)
                return val;
            else
                return saturation*(1-linearity)*
                        (1.-std::exp(1./(linearity-1)*(FLOAT_TYPE(val)/saturation - linearity))) + linearity*saturation;
        };
        
        

        std::default_random_engine generator;

        
//         printf("%f\n", average);
        
        for(auto& value : data){
                value=response(value); //d(generator);   /*response(d(generator));   */     
        }
        
        return 0;
    }
    


    FLOAT_TYPE gain;
    FLOAT_TYPE saturation;
    FLOAT_TYPE linearity;
    FLOAT_TYPE exponent;
    FLOAT_TYPE offset;
    
};
};
