#pragma once

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <stdio.h>
#include <random>
#include "pattern.h"
#include "detectors/detector.h"

// #include "h5_utilities.h"
namespace Detectors{

class MCP: public Detector{
public:
    MCP(uint mask_radius_, FLOAT_TYPE photons_per_channel_, uint channels_per_pixel_, FLOAT_TYPE gain_, FLOAT_TYPE saturation_ , FLOAT_TYPE linearity_ , FLOAT_TYPE offset_, FLOAT_TYPE exponent_, FLOAT_TYPE channel_angle_, FLOAT_TYPE mcp_rotation_, FLOAT_TYPE maxangle_, FLOAT_TYPE maxgain_, bool angle_correction_flag_=0): 
        Detector(mask_radius_, angle_correction_flag_), 
        photons_per_channel(photons_per_channel_),
        channels_per_pixel(channels_per_pixel_),
        gain(gain_), 
        saturation(saturation_), 
        linearity(linearity_),
        offset(offset_),
        exponent(exponent_),
        channel_angle(channel_angle_),
        mcp_rotation(mcp_rotation_),
        maxangle(maxangle_),
        maxgain(maxgain_){}
    
    int apply_features(Pattern& pattern){
        auto& data = pattern.data;
        
        FLOAT_TYPE average = 0;
        for(auto& value : data) average+=value;
        average/=(1.*FLOAT_TYPE(context.output_npixel*context.output_npixel));
        
        if(average==0) return 1;
        
        efficiency_correction(pattern);
        
        auto response = [this](long int input){
//             FLOAT_TYPE val=0;
//             if(input <= linearity*saturation/gain)
//                 val=gain*FLOAT_TYPE(input);
//             else
//                 val = saturation*(1-linearity)*
//                         (1.-std::exp(1./(linearity-1)*(FLOAT_TYPE(input)*gain/saturation - linearity))) + linearity*saturation;
//             return std::pow(val, exponent)+offset;
            FLOAT_TYPE val=0;
            if(input <= linearity*saturation)
                val=FLOAT_TYPE(input);
            else
                val =saturation*(1-linearity)*
                        (1.-std::exp(1./(linearity-1)*(FLOAT_TYPE(input)/saturation - linearity))) + linearity*saturation;
            return std::pow(val, exponent);
        };
        
        

        std::default_random_engine generator;

        
//         printf("%f\n", average);
        
        for(auto& value : data){
            FLOAT_TYPE input = value/(average*FLOAT_TYPE(channels_per_pixel))*FLOAT_TYPE(photons_per_channel);
            std::poisson_distribution<long int> d(input+offset);  
            
            value=0;
            
            for(uint i=0; i<channels_per_pixel; i++)
                value+=response(d(generator))*gain; //d(generator);   /*response(d(generator));   */     
        }
        
        return 0;
    }
    
    void efficiency_correction(Pattern& pattern){
        
        FLOAT_TYPE c_theta = -channel_angle/180.*M_PI;
        FLOAT_TYPE c_alpha = mcp_rotation/180.*M_PI;
        FLOAT_TYPE maxangle_rad = M_PI*maxangle/180.;
//          x<xm ? cos(pi*(x-xm)/xm)/2+0.5 : sin(xm)/sin(x)*(1-sin(x))/(1-sin(xm))
        auto getgain=[this, maxangle_rad](FLOAT_TYPE angle)->FLOAT_TYPE{
//             FLOAT_TYPE A = 0.8;
//             if(angle<maxangle_rad*(1.-A))
//                 return 0.;
//             else 
            if(angle<=maxangle_rad/* and angle>maxangle_rad*(1.-A)*/)
                return maxgain*std::pow((cos(M_PI*(maxangle_rad-angle)/(maxangle_rad))+1.)/2.,2);
            else
                return std::sin(maxangle_rad)/std::sin(angle)*(1.-std::sin(angle))/(1.-std::sin(maxangle_rad));
        };

#pragma omp parallel for schedule(dynamic) collapse(2)
        for(uint iy =0; iy<context.output_npixel; iy++)
            for(uint ix =0; ix<context.output_npixel; ix++){
                FLOAT_TYPE xt=FLOAT_TYPE(ix)-context.output_npixel/2.;
                FLOAT_TYPE yt=FLOAT_TYPE(iy)-context.output_npixel/2.;
                
                FLOAT_TYPE p_alpha=std::atan2(yt,xt);
//                 FLOAT_TYPE p_theta=std::atan(2.*std::sqrt(xt*xt+yt*yt)*std::tan(context.thetarad/2.)/FLOAT_TYPE(context.output_npixel));
                FLOAT_TYPE p_theta=std::sqrt(xt*xt+yt*yt)*context.dtheta;
                
                if(p_theta<=context.output_thetarad){
                    FLOAT_TYPE d_angle = std::acos(std::cos(c_theta)*std::cos(p_theta) + std::sin(c_theta)*std::sin(p_theta)*std::cos(c_alpha-p_alpha));
                       
//                 printf("%f,%f\t%f,%f\t%f\n", c_alpha/M_PI*180., c_theta/M_PI*180., p_alpha/M_PI*180., p_theta/M_PI*180., d_angle/M_PI*180.);
                    pattern.data[ix+context.output_npixel*iy]*=getgain(d_angle);
                }
                else 
                    pattern.data[ix+context.output_npixel*iy]=0;
                
                }
        
    }

    FLOAT_TYPE gain;
    FLOAT_TYPE saturation;
    FLOAT_TYPE linearity;
    FLOAT_TYPE exponent;
    FLOAT_TYPE offset;
    FLOAT_TYPE photons_per_channel;
    uint channels_per_pixel;
    FLOAT_TYPE channel_angle;
    FLOAT_TYPE mcp_rotation;
    FLOAT_TYPE maxangle;
    FLOAT_TYPE maxgain;
};
};
