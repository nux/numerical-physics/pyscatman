import scatman
import numpy as np


scatman.info()

scatman.set_verbose(1)

scatman.set_experiment(wavelength=45., angle=30, resolution=512);

mydetector = scatman.Detectors.MSFT()

dx = 10.
xdim = 128
ydim = 128
zdim = 128

    
data=np.ones([zdim,ydim,xdim], dtype=complex)

              

for z in range(zdim):
    for y in range(ydim):
        for x in range(xdim):
            rad = (x-xdim/2)**2+ (y-ydim/2)**2+ (z-zdim/2)**2
            if rad <(xdim/2)**2:
                data[z,y,x]=0.99 + 1j*0.02
 
    
print(data.shape)

#myshape = scatman.Shapes.VolumeMap(4, data, latitude=60, longitude=40, rotation=0)
myshape = scatman.Shapes.VolumeDistribution(dx, data, latitude=90, longitude=0, rotation=0)


rendering, dx = myshape.get(80)

print(dx)
#rendering
print(rendering.shape)

scatman.init()

mypattern = mydetector.acquire(myshape)

import matplotlib.pyplot as plt
import numpy as np

plt.imshow(np.log(mypattern))
plt.show()

