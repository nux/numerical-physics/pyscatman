import scatman

scatman.set_experiment(wavelength=20, angle=30, resolution=512, photon_density=1.e6);

mydetector = scatman.Detectors.Ideal()

myshape = scatman.Shapes.Ellipsoid(200,200,200, latitude=60, longitude=40, rotation=0)

mypattern = mydetector.acquire(myshape)

import matplotlib.pyplot as plt
import numpy as np

plt.imshow(np.log(mypattern))
plt.show()

